% FLENG compiler - AArch64 backend

target_architecture(a64).

sym_prefix('').

relocation(Name, Name, Lo) :-
    format_to_atom(Lo, ':lo12:~a', [Name]).

relocation(Name, I, Hi, Lo) :-
    format_to_atom(Hi, '~a~d', [Name, I]),
    format_to_atom(Lo, ':lo12:~a~d', [Name, I]).

b_target(_, _).

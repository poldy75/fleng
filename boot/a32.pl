% FLENG compiler - arm32 backend
%
%   A: r0
%   T: r8 (callee save)
%   C: r0
%   G: r10 (callee save)
%   N: r11 (callee save)
%   E<n>: stack, F points to E0, ...
%   F: r7
%   P0-P3: r1-r3 + top of stack, r4 is used temporarily (tcb is passed in r0)
%   SP: sp(r13), on entry set to rbp
%   X0, X1: r5, r6

:- dynamic([floats/2]).

target_architecture(a32).

float_label(N, L) :- floats(N, L), !.
float_label(N, L) :-
    g_read(flabel, L1),
    !,
    L is L1 + 1,
    g_assign(flabel, L),
    assertz(floats(N, L)).
float_label(N, 1) :-
    assertz(flabel(1)),
    assertz(floats(N, 1)).

d_setup(Out) :- 
    g_assign(flabel, 1),
    emit('  .arm\n', Out).

d_finalize(Out) :-
    emit('  .section .data\n  .balign 8\n', Out),
    floats(N, L),
    emit('f~d: .double ~f\n', [L, N], Out),
    fail.
d_finalize(_).

emit_external(_, _).

d_global(Name, Out) :- emit('  .global ~a\n', [Name], Out).
d_section(Name, Out) :- emit('  .section ~a\n', [Name], Out).
d_comment(Str, Out) :- emit('@ ~a\n', [Str], Out).
d_wreserve(N, Out) :- emit('  .space ~d*4\n', [N], Out).

d_function_entry(Out) :- emit('  push {r5, r6, r7, lr}\n', Out).
d_function_exit(Out) :- emit('  pop {r5, r6, r7, lr}\n', Out).
d_endfunction(Out) :- emit('  .pool\n', Out).

% push r5 as dummy to keep 8-byte stack alignment
c_prepare_foreign_call(N, Out) :- N > 3, !, emit('  push {r4, r5}\n', Out).
c_prepare_foreign_call(_, _).

c_finalize_foreign_call(N, Out) :- N > 3, !, emit('  add sp, sp, #8\n', Out).
c_finalize_foreign_call(_, _).

d_align(code, Out) :- emit('  .balign 8\n', Out).
d_align(even, Out) :- emit('  .balign 2\n', Out).
d_align(halfword, Out) :- emit('  .balign 4\n', Out).
d_align(word, Out) :- emit('  .balign 4\n', Out).
d_align(float, Out) :- emit('  .balign 8\n  .word 0\n', Out).

d_equ(S, X, Out) :- emit('  .equ ~a, ~d\n', [S, X], Out).
d_equ(S, Index, X, Out) :- emit('  .equ ~a~d, ~d\n', [S, Index, X], Out).

d_bdata([B], Out) :- !, emit('  .byte ~w\n', [B], Out).
d_bdata([B|L], Out) :- 
    emit('  .byte ~w', [B], Out),
    d_data1(L, Out).

d_wdata([B], Out) :- !, emit('  .word ~w\n', [B], Out).
d_wdata([B|L], Out) :- 
    emit('  .word ~w', [B], Out),
    d_data1(L, Out).

d_fdata([N], Out) :- !, emit('  .double ~f\n', [N], Out).
d_fdata([N|L], Out) :- 
    emit('  .double ~f, ', [N], Out),
    d_data1(L, Out).

d_data1([], Out) :- emit('\n', Out).
d_data1([B|L], Out) :- 
    emit(', ~w', [B], Out),
    d_data1(L, Out).

d_label(Str, Out) :- emit('~a:\n', [Str], Out).
d_label(Prefix, Index, Out) :- emit('~a~d:\n', [Prefix, Index], Out).

dg_label(Str, Out) :- d_label(Str, Out).

a_call(Name, Out) :- emit('  bl ~a\n', [Name], Out).

r_reg(a, r0).
r_reg(c, r0).
r_reg(t, r8).
r_reg(g, r10).
r_reg(n, r11).
r_reg(fp, r7).
r_reg(sp, sp).
r_reg(e(N), _) :- error('bad register: e(~d)', [N]).
r_reg(p(N), R) :- p_reg(N, R).
r_reg(x(N), R) :- x_reg(N, R).

p_reg(0, r1).
p_reg(1, r2).
p_reg(2, r3).
p_reg(3, r4).

x_reg(0, r5).
x_reg(1, r6).

r_move_r(NR, R, Out) :-
    r_reg(R, RR),
    emit('  ldr ~a, ~a\n', [RR, NR], Out).

r_move(e(N), Dest, Out) :- 
    !, 
    r_load(fp, N, Dest, Out).
r_move(Src, e(N), Out) :- 
    !,
    r_store(Src, fp, N, Out).
r_move(Src, Dest, Out) :-
    r_reg(Src, SR),
    r_reg(Dest, DR),
    emit('  mov ~a, ~a\n', [DR, SR], Out).

r_load(RI, I, RD, Out) :-
    I * 4 < 4069,
    !,
    r_reg(RI, RIR),
    r_reg(RD, RDR),
    emit('  ldr ~a, [~a, #~d * 4]\n', [RDR, RIR, I], Out).
r_load(RI, I, RD, Out) :-
    r_reg(RI, RIR),
    r_reg(RD, RDR),
    emit('  ldr r9, =~d * 4\n  add r9, ~a, r9\n  ldr ~a, [r9]\n', 
        [I, RIR, RDR], Out).

a_load(A, I, RD, Out) :-
    I * 4 < 4069,
    !,
    r_reg(RD, RDR),
    emit('  ldr ~a, =~a\n  ldr ~a, [~a, #~d * 4]\n', [RDR, A, RDR, RDR, I], Out).
a_load(A, I, RD, Out) :-
    r_reg(RD, RDR),
    emit('  ldr ~a, =~a\n  ldr r9, =~d * 4\n  add r9, ~a, r9\n  ldr ~a, [r9]\n', 
        [RDR, A, I, RDR, RDR], Out).

r_store(RS, RI, I, Out) :-
    I * 4 < 4069,
    !,
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    emit('  str ~a, [~a, #~d * 4]\n', [RSR, RIR, I], Out).
r_store(RS, RI, I, Out) :-
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    emit('  ldr r9, =~d * 4\n  add r9, ~a, r9\n  str ~a, [r9]\n', 
        [I, RIR, RSR], Out).

la_load(L, RD, Out) :-
    r_reg(RD, RDR),
    emit('  ldr ~a, =~a\n', [RDR, L], Out).
la_load(L, Index, RD, Out) :-
    r_reg(RD, RDR),
    emit('  ldr ~a, =~a~d\n', [RDR, L, Index], Out).

la_loadm(L, Index, RD, Out) :-
    r_reg(RD, RDR),
    emit('  ldr ~a, =~a~d\n  orr ~a, #1\n', [RDR, L, Index, RDR], Out).

lag_load(L, RD, Out) :- la_load(L, RD, Out).

i_load(I, RD, Out) :-
    I >= 0, I < 0x100,
    !,
    r_reg(RD, RDR),
    emit('  mov ~a, #~w\n', [RDR, I], Out).
i_load(I, RD, Out) :-
    r_reg(RD, RDR),
    emit('  ldr ~a, =~w\n', [RDR, I], Out).

f_load(F, Out) :-
    float_label(F, L),
    emit('  ldr r5, =f~d\n  vldr.f64 d0, [r5]\n', [L], Out).

r_push(R, Out) :-
    r_reg(R, RR),
    emit('  push {~a}\n', [RR], Out).

r_pop(R, Out) :-
    r_reg(R, RR),
    emit('  pop {~a}\n', [RR], Out).

g_return(Out) :- emit('  mov pc, lr\n', Out).

ra_inc(R, Out) :-
    r_reg(R, RR),
    emit('  ldr r5, [~a]\n  add r5, r5, #1\n  str r5, [~a]\n', [RR, RR], Out).

la_inc(L, Out) :- 
    emit('  ldr r5, =~a\n  ldr r6, [r5]\n  add r6, r6, #1\n  str r6, [r5]\n', [L], Out).

sp_align(_).

ra_add(RS, N, RD, Out) :-
    r_reg(RS, RSR),
    r_reg(RD, RDR),
    emit('  add ~a, ~a, #~d * 4\n', [RDR, RSR, N], Out).

r_r_op(OP, RA, RB, RD, Out) :- 
    r_reg(RA, RAR),
    r_reg(RB, RBR),
    r_reg(RD, RDR),
    emit('  ~a ~a, ~a, ~a\n', [OP, RDR, RAR, RBR], Out).

r_and(RA, RB, RD, Out) :- r_r_op(and, RA, RB, RD, Out).
r_or(RA, RB, RD, Out) :- r_r_op(orr, RA, RB, RD, Out).
r_xor(RA, RB, RD, Out) :- 
    % special case
    r_reg(RA, RAR),
    r_reg(RB, RBR),
    r_reg(RD, RDR),
    emit('  eor ~a, ~a, ~a\n  add ~a, ~a, #1\n', [RAR, RAR, RBR, RDR, RAR], Out).

i_mask(R, M, Out) :-
    r_reg(R, RR),
    emit('  and ~a, ~a, #~w\n', [RR, RR, M], Out).

i_or(M, R, Out) :-
    r_reg(R, RR),
    emit('  orr ~a, ~a, #~w\n', [RR, RR, M], Out).

j_if_carry(L, Out) :- emit('  bcs ~a\n', [L], Out).
j_if_carry(L, Index, Out) :- emit('  bcs ~a~d\n', [L, Index], Out).

j_compare_if_equal(RS, RI, I, L, Index, Out) :- 
    r_load(RI, I, a, Out),
    r_reg(RS, RSR),
    r_reg(a, RI2),
    emit('  cmp ~a, ~a\n  beq ~a~d\n', [RI2, RSR, L, Index], Out).

j_r_compare_if_not_equal(RS, RI, L, Index, Out) :- 
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    emit('  cmp ~a, ~a\n  bne ~a~d\n', [RSR, RIR, L, Index], Out).

j_a_compare_if_not_equal(RS, A, L, I, Out) :- 
    r_reg(RS, RSR),
    emit('  ldr r5, =~a\n  ldr r5, [r5]\n  cmp ~a, r5\n  bne ~a~d\n', 
        [A, RSR, L, I], Out).

j_compare_if_not_equal(RS, RI, I, L, Index, Out) :- 
    r_load(RI, I, a, Out),
    r_reg(RS, RSR),
    r_reg(a, RI2),
    emit('  cmp ~a, ~a\n  bne ~a~d\n', [RI2, RSR, L, Index], Out).

j_if_test(R, I, L, Index, Out) :-
    r_reg(R, RR),
    emit('  tst ~a, #~d\n  beq ~a~d\n', [RR, I, L, Index], Out).
j_if_test(R, I, L, Out) :-
    r_reg(R, RR),
    emit('  tst ~a, #~d\n  beq ~a\n', [RR, I, L], Out).

j_if_not_test(R, I, L, Index, Out) :-
    r_reg(R, RR),
    emit('  tst ~a, #~d\n  bne ~a~d\n', [RR, I, L, Index], Out).
j_if_not_test(R, I, L,  Out) :-
    r_reg(R, RR),
    emit('  tst ~a, #~d\n  bne ~a\n', [RR, I, L], Out).

j_always(L, Out) :- emit('  b ~a\n', [L], Out).
j_always(P, Index, Out) :- emit('  b ~a~d\n', [P, Index], Out).

sp_reset(_).

b_target(_, _).

% FLENG compiler - aarch64 backend - Mac ("m") flavor

target_architecture(a64m).

sym_prefix('_').

relocation(Name, Hi, Lo) :-
    format_to_atom(Hi, '~a@PAGE', [Name]),
    format_to_atom(Lo, '~a@PAGEOFF', [Name]).

relocation(Name, I, Hi, Lo) :-
    format_to_atom(Hi, '~a~d@PAGE', [Name, I]),
    format_to_atom(Lo, '~a~d@PAGEOFF', [Name, I]).

b_target(_, _).

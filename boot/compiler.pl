% FLENG compiler - compiler core

main(['-I', Dir|Args]) :-
    asserta(include_path(Dir)),
    main(Args).
main(['-w'|Args]) :-
    assertz(no_warnings),
    main(Args).
main(['-n'|Args]) :-
    assertz(print_progress),
    main(Args).
main(['-d'|Args]) :-
    assertz(debug_info),
    main(Args).
main(['-link', Name|Args]) :-
    load_entry_points(Name),
    main(Args).
main(['-e'|Args]) :-
    assertz(generate_entry_points),
    main(Args).
main(['-m', Module|Args]) :-
    assertz(module_name(Module)),
    main(Args).
main([Infile, Outfile]) :-
    op(200, fx, !),
    op(700, xfx, '\\=:='),
    op(500, yfx, '><'),
    op(400, yfx, '\\\\'),
    op(700, yfx, '@'),
    op(900, xfy, '&'),
    op(880, xfx, ':='),
    op(880, xfx, '<='),
    op(880, xfx, '+='),
    op(880, xfx, '-='),
    op(880, xfx, '*='),
    op(880, xfx, '/='),
    op(880, xfx, '<=='),
    op(880, xfx, '=>'),
    op(0, xfx, '^'),
    g_assign(atom_counter, 0),
    g_assign(literal_counter, 0),
    g_assign(symloc_counter, 0),
    assertz(include_path('.')),
    see(Infile),
    read_program(Prg),
    seen,
    collect_groups(Prg, 0, Groups),
    check_exports,
    open(Outfile, write, Out),
    generate_files(Infile, Groups, Out).
main(_) :-
    target_architecture(Arch),
    format(user_error, 'usage: fl2~a (boot) [-d] [-w] [-n] [-e] [-link FILENAME] [-m MODULE] INFILE OUTFILE\n',
        [Arch]),
    halt(1).

generate_files(Infile, _, Out) :-
    generate_entry_points,
    !,
    dump_entry_points(Infile, Out),
    close(Out),
    halt(0).
generate_files(Infile, Groups, Out) :-
    init_asm(Infile, Out),
    length(Groups, N),
    get_module_name(Module),
    register_atom(Module, SMod),
    asm(module(N, SMod, Module), Out),
    compile_program(Groups, 0, _, Out),
    compile_drop_sequences(Out),
    emit_init(Out),
    atom_count(NA),
    asm(atoms(NA), Out),
    dump_atoms(0, Out),
    dump_data(Out),
    dump_literals(Out),
    dump_resolved_atoms(Out),
    asm(end, Out),
    close(Out),
    halt(0).

% utilities

get_module_name(Module) :- module_name(Module), !.
get_module_name('').

read_program(PRG) :-
    read_term(TERM, [variable_names(VS), singletons(SL)]),
    report_singletons(SL, TERM),
    numbervars(TERM),
    read_program(TERM, VS, PRG).

read_program(end_of_file, _, []).
read_program(-DECL, _, PRG) :-
    process_declaration(DECL, PRG).
read_program(TERM, VS, [t(TERM, VS, SVs)|PRG]) :-
    collect_singletons(TERM, SVs),
    read_program(PRG).

report_singletons([], _).
report_singletons(_, _) :- no_warnings.
report_singletons(SL, _) :-
    format(user_error, 'Warning: singleton variables ', []),
    member(Name = _, SL),
    format(user_error, '~a ', [Name]),
    fail.
report_singletons(_, Term) :-
    format(user_error, 'in term:\n\n  ~q\n', [Term]).

collect_singletons(T, Vs) :-
    collect_vars(T, [], Vs1),
    remove_dups(Vs1, [], Vs).

collect_vars(A, Vs, Vs) :- atomic(A).
collect_vars('$VAR'(I), Vs, [I|Vs]).
collect_vars([A|B], Vs1, Vs) :-
    collect_vars(A, Vs1, Vs2),
    collect_vars(B, Vs2, Vs).
collect_vars(T, Vs1, Vs) :-
    callable(T),
    T =.. [_|Args],
    collect_vars(Args, Vs1, Vs).
collect_vars(T, _, _) :- error('invalid term: ~q\n', [T]).

remove_dups([], _, []).
remove_dups([I|L1], Drop, L) :-
    member(I, L1),
    remove_dups(L1, [I|Drop], L).
remove_dups([I|L1], Drop, L) :-
    member(I, Drop),
    remove_dups(L1, Drop, L).
remove_dups([I|L1], Drop, [I|L]) :-
    remove_dups(L1, Drop, L).

process_declaration(initialization(NAME), Prg) :-
    assertz(module_init(NAME)),
    read_program(Prg).
process_declaration(entry_point(N/A, NAME), Prg) :-
    get_module_name(MOD),
    asserta(entry_point(MOD:N/A, NAME)),
    read_program(Prg).
process_declaration(uses(MOD), Prg) :-
    atom(MOD), MOD \= [],
    process_declaration(uses([MOD]), Prg).
process_declaration(uses(MODs), Prg) :-
    add_uses(MODs),
    read_program(Prg).
process_declaration(module(Mod), Prg) :-
    atom(Mod),
    assertz(module_name(Mod)),
    read_program(Prg).
process_declaration(module(Mod), _) :-
    error('invalid module name: ~q\n', [Mod]).
process_declaration(synthesized(N1/A1, N2/A2), Prg) :-
    atom(N1), atom(N2),
    integer(A1), integer(A2),
    assertz(synthesized_name(N1/A1, N2/A2)),
    read_program(Prg).
process_declaration(include(Fname), Prg) :-
    atom(Fname),
    process_declaration(include([Fname]), Prg).
process_declaration(include(Fnames), Prg) :-
    include_files(Fnames, Prg).
process_declaration(exports(Exports), Prg) :-
    register_exports(Exports),
    read_program(Prg).
process_declaration(arguments(Str), Prg) :-
    register_cmdline(Str),
    read_program(Prg).
process_declaration(DECL, Prg) :-
    format(user_error, 'Warning: invalid declaration: ~q (ignored)\n', [DECL]),
    read_program(Prg).

register_cmdline(Str) :-
    atom(Str),
    atom_codes(Str, C),
    assertz(cmdline_init(C)).
register_cmdline(Str) :- assertz(cmdline_init(Str)).

register_exports(Exports) :-
    member(Exp, Exports),
    register_export(Exp),
    fail.
register_exports(_).

register_export(N/A) :-
    atom(N), integer(A),
    assertz(exported(N/A)),
    !.
register_export(X) :-
    error('invalid export specifier: ~q\n', [X]).

add_uses([]).
add_uses([Mod|Mods]) :-
    assertz(uses(Mod)),
    add_uses(Mods).

include_files([], Prg) :- read_program(Prg).
include_files([Fname|Fnames], Prg) :-
    current_input(In),
    find_include_file(Fname, Fname2),
    see(Fname2),
    read_program(Included),
    seen,
    set_input(In),
    append(Included, Next, Prg),
    include_files(Fnames, Next).
include_files(X, _) :-
    error('invalid include file specification: ~w\n', [X]).

find_include_file(Fname1, Fname) :-
    include_path(Dir),
    check_file(Dir, Fname1, Fname),
    !.
find_include_file(Fname, _) :-
    error('included file does not exist: ~a\n', [Fname]).

check_file(Dir, Fname1, Fname) :-
    format_to_atom(Fname, '~a/~a', [Dir, Fname1]),
    file_exists(Fname),
    !.
check_file(Dir, Fname1, Fname) :-
    format_to_atom(Fname, '~a/~a.fl', [Dir, Fname1]),
    file_exists(Fname).

error(Fstr, Args) :-
    format(user_error, Fstr, Args),
    halt(1).

check_exports :-
    exported(N/A),
    \+pdef(N/A, _),
    format(user_error, 'Warning: undefined export ~a/~d\n', [N, A]),
    fail.
check_exports.

collect_groups([], _, []).
collect_groups([T|PRG], I, [pdef(NA, I, EL, [T|G])|GROUPS]) :-
    T = t(TERM, VS, _),
    length(VS, EL),
    term_head(TERM, NA, _, _),
    filter_group(PRG, NA, G, NG),
    assertz(pdef(NA, I)),
    I2 is I + 1,
    collect_groups(NG, I2, GROUPS).

filter_group([], _, [], []).
filter_group([T|PRG], NA, [T|G], NG) :-
    T = t(TERM, _, _),
    term_head(TERM, NA, _, _),
    filter_group(PRG, NA, G, NG).
filter_group([T|PRG], NA, G, [T|NG]) :-
    filter_group(PRG, NA, G, NG).

term_head((H :- _), NA, EA, ARGS) :-
    !,
    term_head(H, NA, EA, ARGS).
term_head(H, N/A, EA, Args) :-
    H =.. [N|Args1],
    compress_args(Args1, Args),
    length(Args1, A),
    length(Args, EA).

emit(Str, Out) :- write(Out, Str).
emit(Fstr, Args, Out) :- format(Out, Fstr, Args).

emit_init(Out) :-
    module_init(Name),
    emit_init(Out, Name).
emit_init(Out) :-
    asm(init, Out).

emit_init(Out, Name) :-
    pdef(Name/0, I),
    (\+exported(_); exported(Name/0)),
    asm(init(I), Out).
emit_init(_, Name) :-
    error('initialization goal ~a/0 is not defined\n', [Name]).

compress_args([A1, A2, A3, A4, A5|As], [A1, A2, A3, [A4, A5|As]]).
compress_args(As, As).

dbg(X) :- format('~q\n', [X]).
dbg(F, X) :- format(F, [X]).

label(Lc1, Lc, Lc1) :- Lc is Lc1 + 1.

register_data(Lc1, Lc, Lc1) :-
    Lc is Lc1 + 1,
    assertz(r_data(Lc1)).

resolve_cache_data(Identification, Lc, Lc, Ld) :-
    resolve_cache(Identification, Ld),
    !.
resolve_cache_data(Identification, Lc1, Lc, Ld) :-
    register_data(Lc1, Lc, Ld),
    assertz(resolve_cache(Identification, Ld)).

dump_data(Out) :-
    r_data(Ld),
    asm(data(Ld), Out),
    fail.
dump_data(_).

dump_literals(Out) :-
    literal(X, L),
    dump_literal(X, L, Out),
    fail.
dump_literals(_).

% atoms

register_atom(A, I) :- r_atom(A, I), !.
register_atom(A, I) :-
    g_read(atom_counter, I),
    I2 is I + 1,
    g_assign(atom_counter, I2),
    assertz(r_atom(A, I)).

dump_atoms(I, Out) :-
    r_atom(A, I),
    asm(defatom(I, A), Out),
    I2 is I + 1,
    dump_atoms(I2, Out).
dump_atoms(_, _).

atom_count(I) :- g_read(atom_counter, I).

% compilation entry point

compile_program([], Lc, Lc, _).
compile_program([pdef(NA, I, EL, G)|GROUPS], Lc1, Lc, Out) :-
    compile_group(NA, I, EL, G, Lc1, Lc2, Out),
    compile_program(GROUPS, Lc2, Lc, Out).

compile_group(N/A, I, EL, G, Lc1, Lc, Out) :-
    print_progress(N, A),
    register_atom(N, S),
    asm([define(S, A, I, EL, N), reset], Out),
    compile_clauses(G, none, yes, Lc1, Lc2, Out),
    label(Lc2, Lc, Lf),
    asm([not_suspending(Lf), suspend, yield, label(Lf)], Out),
    asm([fail(A), end_def], Out).

compile_clauses([], Fail, _, Lc, Lc, Out) :-
    emit_fail_label(Fail, Out).
compile_clauses([t(Term, _, Ss)|G], Fail, ForceArg1, Lc1, Lc, Out) :-
    emit_fail_label(Fail, Out),
    label(Lc1, Lc2, Fail2),
    compile_clause(Term, Ss, ForceArg1, Fail2, Lc2, Lc3, Out),
    compile_clauses(G, Fail2, yes, Lc3, Lc, Out).

print_progress(N, A) :-
    print_progress,
    format('~a/~d\n', [N, A]).
print_progress(_, _).

emit_fail_label(none, _).
emit_fail_label(Fail, Out) :-
    number(Fail),
    asm(try_else(Fail), Out).

% clause

compile_clause((H :- B), Ss, ForceArg1, Fail, Lc1, Lc, Out) :-
    !,
    term_head(H, N/A, EA, Args),
    H2 =.. [N|Args],
    compile_head(H2, A, Ss, Fail, ForceArg1, Lc1, Lc2, Out, Bound, Forced),
    analyze(B, Bound, _, [], AEnv),
    compile_body(B, EA, Ss, Fail, Bound, Forced, AEnv, Lc2, Lc, Out).
compile_clause(C, _, _, _, _, _, _) :-
    error('invalid clause: ~q\n', [C]).

compile_head(H, A, Ss, Fail, ForceArg1, Lc1, Lc, Out, Bound, Forced) :-
    H =.. [_|Args],
    compile_matches(Args, 0, A, Ss, Fail, ForceArg1, Lc1, Lc, Out, [],
        Bound, [], Forced).

% matching

compile_matches([], _, _, _, _, _, Lc, Lc, _, S, S, F, F).
compile_matches([Args], 3, A, Ss, Fail, _, Lc1, Lc, Out, S1, S, F1, F) :-
    A > 4,
    !,
    asm(getarg(3), Out),
    drop_sequence(S1, [], jump(Fail), Lc1, Lc2, Ld),
    asm(force(Ld), Out),
    compile_match_list(Args, Ss, Fail, Lc2, Lc, Out, S1, S, F1, F).
compile_matches(['$VAR'(V)|Args], I, A, Ss, Fail, _, Lc1, Lc, Out, S1, S,
    F1, F) :-
    memberchk(V, Ss),
    !,
    I2 is I + 1,
    compile_matches(Args, I2, A, Ss, Fail, yes, Lc1, Lc, Out, S1, S, F1, F).
compile_matches([X|Args], I, A, Ss, Fail, FA1, Lc1, Lc, Out, S1, S, F1, F) :-
    asm(getarg(I), Out),
    compile_match(X, Ss, Fail, FA1, Lc1, Lc2, Out, S1, S2, F1, F2),
    I2 is I + 1,
    compile_matches(Args, I2, A, Ss, Fail, yes, Lc2, Lc, Out, S2, S, F2, F).

compile_match_list([], _, _, Lc, Lc, _, S, S, F, F).
compile_match_list(['$VAR'(V)|Args], Ss, Fail, Lc1, Lc, Out, S1, S, F1, F) :-
    memberchk(V, Ss),
    !,
    asm(cdr, Out),
    compile_match_list(Args, Ss, Fail, Lc1, Lc, Out, S1, S, F1, F).
compile_match_list([X|Args], Ss, Fail, Lc1, Lc, Out, S1, S, F1, F) :-
    asm(decons, Out),
    compile_match(X, Ss, Fail, yes, Lc1, Lc2, Out, S1, S2, F1, F2),
    asm(pop, Out),
    compile_match_list(Args, Ss, Fail, Lc2, Lc, Out, S2, S, F2, F).

compile_match('!'('$VAR'(VI)), _, _, _, _, _, _, Seen, _, _, _) :-
    member(VI, Seen),
    error('multiple references of variable in head: ~d\n', [VI]).
compile_match('!'('$VAR'(VI)), Ss, Fail, FA, Lc1, Lc, Out, S, S, F, F) :-
    member(VI, Ss),
    drop_sequence(S, [], jump(Fail), Lc1, Lc, Ld),
    emit_force(FA, Ld, Out).
compile_match('!'('$VAR'(VI)), _, Fail, FA, Lc1, Lc, Out, S, [VI|S], F, [VI|F]) :-
    drop_sequence(S, [], jump(Fail), Lc1, Lc, Ld),
    emit_force(FA, Ld, Out),
    asm(putenv(VI), Out).
compile_match('!'(X), _, _, _, _, _, _, _, _, _, _) :-
    error('misplaced "!" operator: ~q\n', '_!_'(X)).
compile_match(N, _, Fail, FA, Lc1, Lc, Out, S, S, F, F) :-
    integer(N),
    drop_sequence(S, [], jump(Fail), Lc1, Lc, Ld),
    emit_force(FA, Ld, Out),
    asm(isint(N, Fail, Ld), Out).
compile_match(N, _, Fail, FA, Lc1, Lc, Out, S, S, F, F) :-
    float(N),
    drop_sequence(S, [], jump(Fail), Lc1, Lc, Ld),
    emit_force(FA, Ld, Out),
    asm(isfloat(N, Fail, Ld), Out).
compile_match([], _, Fail, FA, Lc1, Lc, Out, S, S, F, F) :-
    % swipl doesn't treat "[]" as atom...
    register_atom('[]', AI),
    drop_sequence(S, [], jump(Fail), Lc1, Lc, Ld),
    emit_force(FA, Ld, Out),
    asm(isatom(AI, Fail, Ld, '[]'), Out).
compile_match(A, _, Fail, FA, Lc1, Lc, Out, S, S, F, F) :-
    atom(A),
    register_atom(A, AI),
    drop_sequence(S, [], jump(Fail), Lc1, Lc, Ld),
    emit_force(FA, Ld, Out),
    asm(isatom(AI, Fail, Ld, A), Out).
compile_match([X|Y], Ss, Fail, FA, Lc1, Lc, Out, S1, S, F1, F) :-
    drop_sequence(S1, [], jump(Fail), Lc1, Lc2, Ld),
    emit_force(FA, Ld, Out),
    asm(islist(Fail, Ld), Out),
    compile_match(X, Ss, Fail, yes, Lc2, Lc3, Out, S1, S2, F1, F2),
    asm(pop, Out),
    compile_match(Y, Ss, Fail, yes, Lc3, Lc, Out, S2, S, F2, F).
compile_match('$VAR'(VI), _, _, _, _, _, _, Seen, _, _, _) :-
    member(VI, Seen),
    error('multiple references of variable in head: ~d\n', [VI]).
compile_match('$VAR'(VI), Ss, _, _, Lc, Lc, _, S, S, F, F) :-
    member(VI, Ss).
compile_match('$VAR'(VI), _, _, _, Lc, Lc, Out, S, [VI|S], F, F) :-
    asm(putenv(VI), Out).
compile_match('{}'(Xs), Ss, Fail, FA, Lc1, Lc, Out, S1, S, F1, F) :-
    tuple_args(Xs, [Arg1|Args]),
    length(Args, N),
    drop_sequence(S1, [], jump(Fail), Lc1, Lc2, Ld),
    emit_force(FA, Ld, Out),
    asm(istuple(N, Fail, Ld), Out),
    compile_match(Arg1, Ss, Fail, yes, Lc2, Lc3, Out, S1, S2, F1, F2),
    asm(pop, Out),
    compile_match(Args, Ss, Fail, yes, Lc3, Lc, Out, S2, S, F2, F).
compile_match(T, Ss, Fail, FA, Lc1, Lc, Out, S1, S, F1, F) :-
    callable(T),
    T =.. [Head|Args],
    length(Args, N),
    drop_sequence(S1, [], jump(Fail), Lc1, Lc2, Ld),
    emit_force(FA, Ld, Out),
    asm(istuple(N, Fail, Ld), Out),
    compile_match(Head, Ss, Fail, yes, Lc2, Lc3, Out, S1, S2, F1, F2),
    asm(pop, Out),
    compile_match(Args, Ss, Fail, yes, Lc3, Lc, Out, S2, S, F2, F).
compile_match(Arg, _, _, _, _, _, _, _, _, _, _) :-
    error('invalid match: ~q\n', [Arg]).

emit_force(no, _, Out) :- asm(deref, Out).
emit_force(yes, Fail, Out) :- asm(force(Fail), Out).

synthesized(NA1, NA2) :-
    synthesized_name(NA1, NA3),
    !,
    synthesized(NA3, NA2).
synthesized(NA1, NA1).

tuple_args((X, Y), [X|Args]) :- !, tuple_args(Y, Args).
tuple_args(X, [X]).

% bodies

compile_body(B, Arity, Ss, Fail, Bound, Forced, AEnv, Lc1, Lc, Out) :-
    compile_primitives(B, Out, AEnv, Ss, Fail, Forced, F2, Bound, B3, Lc1,
        Lc2, [], User),
    reverse(User, User1),
    reorder_goals(User1, User2),
    compile_user_goals(User2, Arity, B3, AEnv, Ss, F2, Lc2, Lc, Out).

reorder_goals([], []).
reorder_goals([G], [G]).
reorder_goals([G0|Gs], GR) :-
    append(Gs, [G0], GR).

compile_primitives((X, Y), Out, AEnv, Ss, Fl, Fd1, Fd, Env1, Env, Lc1, Lc, U1, U) :-
    !,
    compile_primitives(X, Out, AEnv, Ss, Fl, Fd1, Fd2, Env1, Env2, Lc1, Lc2, U1, U2),
    compile_primitives(Y, Out, AEnv, Ss, Fl, Fd2, Fd, Env2, Env, Lc2, Lc, U2, U).
compile_primitives(true, _, _, _, _, F, F, E, E, Lc, Lc, U, U).
compile_primitives(X = Y, Out, AEnv, Ss, Fl, F1, F, E1, E, Lc1, Lc, U1, U) :-
    compile_primitives('$unify'(X, Y, '$VAR'('_')), Out, AEnv, Ss, Fl, F1, F, E1, E,
        Lc1, Lc, U1, U).
compile_primitives(call(T), _, _, _, _, F, F, E, E, Lc, Lc, U, [T|U]) :-
    callable(T), T \= '$VAR'(_), T \= _:_.
compile_primitives(unify(X, Y, R), Out, AEnv, Ss, Fl, F1, F, E1, E, Lc1, Lc, U1, U) :-
    compile_primitives('$unify_safe'(X, Y, R), Out, AEnv, Ss, Fl, F1, F, E1, E, Lc1, Lc,
    U1, U).
compile_primitives('$suspend', Out, AEnv, _, _, F, F, E, E, Lc1, Lc, U, U) :-
    drop_sequence(E, AEnv, suspend, Lc1, Lc, Ld),
    asm(suspending(Ld), Out).
compile_primitives('$reify', Out, _, _, _, F, F, E, E, Lc, Lc, U, U) :-
    asm([getarg(0), reify], Out).
compile_primitives('$label'(NA, R), Out, AEnv, Ss, _, Fd1, Fd, E1, E, Lc, Lc, U, U) :-
    resolve_label(NA, L),
    asm(code_label(NA, L), Out),
    compile_compute_result(R, AEnv, Ss, Fd1, Fd, E1, E, Out).
compile_primitives('$guard'('$VAR'(V), true), Out, AEnv, _, Fail, F, F, E, E, Lc1,
    Lc, U, U) :-
    member(temp(V), AEnv),
    member(type(V, a), AEnv),
    drop_sequence(E, AEnv, jump(Fail), Lc1, Lc, Ld),
    asm([getenv(V), if_true(Ld)], Out).
compile_primitives('$guard'('$VAR'(V), false), Out, AEnv, _, Fail, F, F, E, E, Lc1,
    Lc, U, U) :-
    member(temp(V), AEnv),
    member(type(V, a), AEnv),
    drop_sequence(E, AEnv, jump(Fail), Lc1, Lc, Ld),
    asm([getenv(V), if_false(Ld)], Out).
compile_primitives('$guard'(X, Y), Out, AEnv, Ss, Fail, F, F, E1, E, Lc1, Lc, U, U) :-
    drop_sequence(E1, AEnv, jump(Fail), Lc1, Lc, Ld),
    compile_arguments([X, Y], any, putparam, Ss, F, AEnv, E1, E, Out, Build),
    asm(guard(Ld), Out),
    unref_arguments(Build, Out).
compile_primitives('$idle'(R), Out, AEnv, Ss, _, Fd1, Fd, E1, E, Lc1, Lc, U, U) :-
    register_data(Lc1, Lc, Ld),
    asm(idle(Ld), Out),
    compile_compute_result(R, AEnv, Ss, Fd1, Fd, E1, E, Out).
compile_primitives('$new_task'(V), Out, AEnv, Ss, _, Fd, Fd, E1, E, Lc, Lc, U, U) :-
    compile_value(V, x, Ss, Fd, AEnv, E1, E, Out),
    asm(new_task, Out).
compile_primitives(foreign_call(T), Out, AEnv, Ss, _, F, F, E1, E, Lc, Lc, U, U) :-
    compile_foreign_call(T, Out, AEnv, Ss, F, E1, E).
compile_primitives(compute(Op, A, B, R),_,  _, _, _, _, _, _, _, _, _, _, _) :-
    R \= '$VAR'(_),
    error('computation result must be a variable: ~q\n', [compute(Op, A, B, R)]).
compile_primitives(compute(Op, A, R),_,  _, _, _, _, _, _, _, _, _, _, _) :-
    R \= '$VAR'(_),
    error('computation result must be a variable: ~q\n', [compute(Op, A, R)]).
compile_primitives(compute('>', A, B, R), Out, AEnv, Ss, F, Fd1, Fd, E1, E, Lc1,
        Lc, U1, U) :-
    compile_primitives(compute('<', B, A, R), Out, AEnv, Ss, F, Fd1, Fd, E1, E, Lc1,
            Lc, U1, U).
compile_primitives(compute(Op, A, B, R), Out, AEnv, Ss, _, Fd1, Fd, E1, E, Lc,
        Lc, U, U) :-
    atom(Op), Op \= ('>'),
    check_computation(Op, Types),
    length(Types, 3),
    compile_arguments([A, B], Types, putparam, Ss, Fd1, AEnv, E1, E2, Out,
        Build),
    asm(compute(Op), Out),
    unref_arguments(Build, Out),
    compile_compute_result(R, AEnv, Ss, Fd1, Fd, E2, E, Out).
compile_primitives(compute(Op, A, R), Out, AEnv, Ss, _, Fd1, Fd, E1, E, Lc,
        Lc, U, U) :-
    atom(Op),
    check_computation(Op, Types),
    length(Types, 2),
    compile_arguments([A], Types, putparam, Ss, Fd1, AEnv, E1, E2, Out, Build),
    asm(compute(Op), Out),
    unref_arguments(Build, Out),
    compile_compute_result(R, AEnv, Ss, Fd1, Fd, E2, E, Out).
compile_primitives(T, Out, AEnv, Ss, _, Fd1, Fd, Env1, Env, Lc, Lc, U, U) :-
    T =.. [N|Args1],
    length(Args1, A),
    primitive(N/A, Op, result, Types),
    split_args(Args1, Args, R),
    compile_arguments(Args, Types, putparam, Ss, Fd1, AEnv, Env1, Env2, Out,
        Build),
    asm(Op, Out),
    unref_arguments(Build, Out),
    compile_compute_result(R, AEnv, Ss, Fd1, Fd, Env2, Env, Out).
compile_primitives(T, Out, AEnv, Ss, _, Fd, Fd, Env1, Env, Lc, Lc, U, U) :-
    T =.. [N|Args],
    length(Args, A),
    primitive(N/A, Op, _, Types),
    compile_arguments(Args, Types, putparam, Ss, Fd, AEnv, Env1, Env, Out,
        Build),
    asm(Op, Out),
    unref_arguments(Build, Out).
compile_primitives(U, _, _, _, _, Fd, Fd, Env, Env, Lc, Lc, User, [U|User]).

resolve_label(N/A, I) :- pdef(N/A, I).
resolve_label(NA, _) :-
    error('attempt to take address of undefined user goal: ~w\n', [NA]).

check_computation(Op, Types) :- computation(Op, Types).
check_computation(Op, _) :-
    error('invalid computation: ~w\n', [Op]).

compile_compute_result('$VAR'('_'), _, _, Fd, Fd, Env, Env, _).
compile_compute_result('$VAR'(R), AEnv, _, Fd, [R|Fd], Env, [R|Env], Out) :-
    member(temp(R), AEnv),
    asm(putenvu(R), Out).
compile_compute_result('$VAR'(R), _, Ss, Fd, Fd, Env, Env, _) :-
    member(R, Ss).
compile_compute_result(R, _, Ss, Fd, Fd, Env1, Env, Out) :-
    asm(putparam(0), Out),
    compile_value(R, x, Ss, [], Env1, Env, Out),
    asm([putparam(1), assign], Out).

compile_foreign_call(T, Out, AEnv, Ss, F, E1, E) :-
    callable(T),
    T =.. [Name|Args],
    length(Args, N),
    N < 5,
    compile_arguments(Args, any, putparam, Ss, F, AEnv, E1, E, Out, Build),
    asm(align_stack, Out),
    save_registers(Build, Out),
    asm(foreign_call(Name, N), Out),
    restore_registers(Build, Out),
    unref_arguments(Build, Out).
compile_foreign_call(T, _, _, _, _, _, _, _, _, _, _) :-
    error('invalid foreign call: ~q\n', [T]).

compile_user_goals([], Arity, Env, AEnv, _, _, Lc1, Lc, Out) :-
    drop_sequence(Env, AEnv, done(Arity), Lc1, Lc, Ld),
    asm(jump(Ld), Out).
compile_user_goals(['$invoke'(PDef, Args)], A, Env, AEnv, Ss, Fd, Lc1, Lc, Out) :-
    compile_drop_args(A, Out),
    compile_value(PDef, p, Ss, Fd, AEnv, Env, Env2, Out),
    asm(invoke_goal, Out),
    compile_value(Args, x, Ss, Fd, AEnv, Env2, Env, Out),
    compile_invoke_arguments(Lc1, Lc2, Out),
    drop_sequence(Env2, AEnv, invoke, Lc2, Lc, Ld),
    asm(jump(Ld), Out).
compile_user_goals(['$invoke'(A, B)|_], _, _, _, _, _, _, _, _) :-
    error('use of $invoke in non-tail context: ~q\n', ['$invoke'(A, B)]).
compile_user_goals([call(M:T)], Arity, Env, AEnv, Ss, _, Lc1, Lc, Out) :-
    callable(T),
    called_user_goal(T, N/A, _, Args),
    entry_point(M:N/A, EP),
    asm(tailgoal(EP, N, A), Out),
    compile_drop_args(Arity, Out),
    compile_arguments(Args, any, putarg, Ss, noderef, AEnv, Env, Env2, Out, _),
    drop_sequence(Env2, AEnv, jump, Lc1, Lc, Ld),
    asm(jump(Ld), Out).
compile_user_goals([call(M:T)], Arity, Env, AEnv, Ss, _, Lc1, Lc, Out) :-
    atom(M),
    called_user_goal(T, N/A, _, Args),
    asm(tailgoal('fl_ccall', N, A), Out),
    register_atom(N, NI),
    register_atom(M, MI),
    resolve_cache_data(M:N/A, Lc1, Lc2, Ld1),
    compile_drop_args(Arity, Out),
    compile_arguments(Args, any, putarg, Ss, noderef, AEnv, Env, Env2, Out, _),
    drop_sequence(Env2, AEnv, jump, Lc2, Lc, Ld2),
    asm([resolve(Ld1, MI, NI, A), jump(Ld2)], Out).
compile_user_goals([G], Arity, Env, AEnv, Ss, _, Lc1, Lc, Out) :-
    user_goal(G, N/A, _, I, Args),
    asm(tailgoal(I, N, A), Out),
    compile_drop_args(Arity, Out),
    compile_arguments(Args, any, putarg, Ss, noderef, AEnv, Env, Env2, Out, _),
    drop_sequence(Env2, AEnv, jump, Lc1, Lc, Ld),
    asm(jump(Ld), Out).
compile_user_goals([call(M:T)|Gs], Arity, Env, AEnv, Ss, Fd, Lc1, Lc, Out) :-
    callable(T),
    called_user_goal(T, N/A, _, Args),
    entry_point(M:N/A, EP),
    asm(open_goal(EP, N, A), Out),
    compile_arguments(Args, any, putarg, Ss, noderef, AEnv, Env, Env2, Out, _),
    asm(close, Out),
    compile_user_goals(Gs, Arity, Env2, AEnv, Ss, Fd, Lc1, Lc, Out).
compile_user_goals([call(M:T)|Gs], Arity, Env, AEnv, Ss, Fd, Lc1, Lc, Out) :-
    atom(M),
    called_user_goal(T, N/A, _, Args),
    asm(open_goal('fl_ccall', N, A), Out),
    register_atom(N, NI),
    register_atom(M, MI),
    compile_arguments(Args, any, putarg, Ss, noderef, AEnv, Env, Env2, Out, _),
    resolve_cache_data(M:N/A, Lc1, Lc2, Ld),
    asm([resolve(Ld, MI, NI, A), close], Out),
    compile_user_goals(Gs, Arity, Env2, AEnv, Ss, Fd, Lc2, Lc, Out).
compile_user_goals([G|Gs], Arity, Env, AEnv, Ss, Fd, Lc1, Lc, Out) :-
    user_goal(G, N/A, _, I, Args),
    asm(open_goal(I, N, A), Out),
    compile_arguments(Args, any, putarg, Ss, noderef, AEnv, Env, Env2, Out, _),
    asm(close, Out),
    compile_user_goals(Gs, Arity, Env2, AEnv, Ss, Fd, Lc1, Lc, Out).

user_goal(compute(Op, A, B, R), rtcompute/4, 4, 'fl_compute', [Op, A, B, R]).
user_goal(compute(Op, A, R), rtcompute/3, 3, 'fl_compute2', [Op, A, R]).
user_goal(call(T), rt_call/2, 2, 'fl_call', [T, '$VAR'('__MODULE__')]).
user_goal(call(T, S), rt_call_task/3, 3, 'fl_call_task', [T, '$VAR'('__MODULE__'), S]).
user_goal(call(T, E, S), rt_call_task_with_env/4, 4, 'fl_call_task_with_env',
    [T, '$VAR'('__MODULE__'), E, S]).
user_goal('$call'(T, Mod), rt_call/2, 2, 'fl_call', [T, Mod]).
user_goal('$call_with_remote_task'(Task, T, M), rt_call_with_remote_task/3, 3,
    'fl_call_with_remote_task', [Task, T, M]).
user_goal('$rcall'(T, ID), rt_call_thread/3, 3, 'fl_call_thread',
    [T, ID, '$VAR'('__MODULE__')]).
user_goal('$rcall'(T, ID, Mod), rt_call_thread/3, 3, 'fl_call_thread', [T, ID, Mod]).
user_goal(run(M, T), rt_run/2, 2, 'fl_run', [M, T]).
user_goal('$jump'(L, Args), '$jump'/2, 2, 'fl_jump', [L, Args]).
user_goal(current_module(R), rt_current_module/2, 2, 'fl_current_module',
    ['$VAR'('__MODULE__'), R]).
user_goal(G, NA, EA, I, Args) :-
    called_user_goal(G, NA, EA, Args),
    pdef(NA, I).
user_goal(G, _, _, _, _) :-
    error('call to undefined goal: ~q\n', [G]).

called_user_goal(G, N/A, EA, Args) :-
    callable(G),
    G =.. [N|Args1],
    compress_args(Args1, Args),
    length(Args1, A),
    length(Args, EA).

compile_value(T, Type, Ss, AEnv, Env1, Env, Out) :-
    compile_value(T, Type, Ss, noderef, AEnv, Env1, Env, Out).

compile_value(N, T, _, _, _, Env, Env, Out) :-
    integer(N),
    asm(mkint(N), Out),
    compile_check(T, i, Out).
compile_value([], T, _, _, _, Env, Env, Out) :-
    % SWI-prolog doesn't treat "[]" as an atom
    register_atom('[]', I),
    asm(atom(I, '[]'), Out),
    compile_check(T, a, Out).
compile_value(S, T, _, _, _, Env, Env, Out) :-
    atom(S),
    register_atom(S, I),
    asm(atom(I, S), Out),
    compile_check(T, a, Out).
compile_value(X, _, _, _, _, Env, Env, Out) :-
    ground_value(X),
    !,
    register_literal(X, Lit),
    asm(literal(Lit), Out).
compile_value('$VAR'('__MODULE__'), T, _, _, _, Env, Env, Out) :-
    asm(getmodule, Out),
    compile_check(T, m, Out).
compile_value('$VAR'('_'), T, _, _, _, Env, Env, Out) :-
    asm(mkvar, Out),
    compile_check(T, v, Out).
compile_value('$VAR'(I), T, Ss, _, _, Env, Env, Out) :-
    member(I, Ss),
    asm(mkvar, Out),
    compile_check(T, v, Out).
compile_value('$VAR'(I), T, _, Fd, AEnv, Env, Env, Out) :-
    member(I, Env),
    asm(getenv(I), Out),
    compile_deref(I, Fd, Out),
    compile_check_var(T, I, AEnv, Out).
compile_value('$VAR'(I), T, _, _, _, Env, [I|Env], Out) :-
    asm([mkvar, bind(I)], Out),
    compile_check(T, v, Out).
compile_value([X|Y], T, Ss, Fd, AEnv, Env1, Env, Out) :-
    compile_value(X, x, Ss, Fd, AEnv, Env1, Env2, Out),
    asm(push, Out),
    compile_value(Y, x, Ss, Fd, AEnv, Env2, Env, Out),
    asm(mklist, Out),
    compile_check(T, l, Out).
compile_value('{}'(X), T, Ss, Fd, _, Env1, Env, Out) :-
    tuple_args(X, Args),
    compile_value(Args, x, Ss, Fd, [], Env1, Env, Out),
    length(Args, Arity),
    A2 is Arity - 1,
    asm(mktuple(A2), Out),
    compile_check(T, t, Out).
compile_value(X, T, Ss, Fd, _, Env1, Env, Out) :-
    callable(X),
    X =.. [Name|Args],
    register_atom(Name, I),
    compile_value(Args, x, Ss, Fd, [], Env1, Env, Out),
    length(Args, Arity),
    asm(mktuple(I, Arity, Name), Out),
    compile_check(T, t, Out).
compile_value(X, _, _, _, _, _, _, _) :-
    error('invalid value: ~q\n', [X]).

ground_value(X) :- atomic(X), !.
ground_value([X|Y]) :- !, ground_value(X), ground_value(Y).
ground_value('{}'(X)) :- !, ground_value(X).
ground_value('$VAR'(_)) :- !, fail.
ground_value(T) :-
    T =.. [_|Lst],
    ground_value(Lst).

compile_deref(_, noderef, _).
compile_deref(I, Fd, _) :-
    member(I, Fd).
compile_deref(_, _, Out) :-
    asm(deref, Out).

compile_check(T, T, _).
compile_check(n, i, _).
compile_check(n, f, _).
compile_check(c, _, Out) :- asm(check_cell, Out).
compile_check(i, _, Out) :- asm(check_integer, Out).
compile_check(n, _, Out) :- asm(check_number, Out).
compile_check(m, _, Out) :- asm(check_module, Out).
compile_check(a, _, Out) :- asm(check_atom, Out).
compile_check(t, _, Out) :- asm(check_tuple, Out).
compile_check(l, _, Out) :- asm(check_list, Out).
compile_check(_, _, _).

compile_check_var(T, V, AEnv, Out) :-
    member(type(V, T2), AEnv),
    compile_check(T, T2, Out).
compile_check_var(T, _, _, Out) :- compile_check(T, x, Out).

compile_arguments(Args, Types, Op, Ss, Fd, AEnv, Env1, Env, Out, Build) :-
    compile_arguments(Args, Types, 0, Op, Ss, Fd, AEnv, Env1, Env, Out, [], Build).

compile_arguments([], _, _, _, _, _, _, Env, Env, _, B, B).
compile_arguments([X|Args], any, I, Op, Ss, Fd, AEnv, Env1, Env, Out, B1, B) :-
    compile_value(X, x, Ss, Fd, AEnv, Env1, Env2, Out),
    Instr =.. [Op, I],
    asm(Instr, Out),
    I2 is I + 1,
    build_param(X, Op, I, Ss, Out, B1, B2),
    compile_arguments(Args, any, I2, Op, Ss, Fd, AEnv, Env2, Env, Out, B2, B).
compile_arguments([X|Args], [T|Types], I, Op, Ss, Fd, AEnv, Env1, Env, Out, B1, B) :-
    compile_value(X, T, Ss, Fd, AEnv, Env1, Env2, Out),
    Instr =.. [Op, I],
    asm(Instr, Out),
    I2 is I + 1,
    build_param(X, Op, I, Ss, Out, B1, B2),
    compile_arguments(Args, Types, I2, Op, Ss, Fd, AEnv, Env2, Env, Out, B2, B).

% remember arguments to primitives that are created on the fly and need to be
% unref'd after use:
build_param('$VAR'('_'), putparam, I, _, Out, B, [I|B]) :-
    asm(addref, Out).
build_param('$VAR'(V), putparam, I, Ss, Out, B, [I|B]) :-
    member(V, Ss),
    asm(addref, Out).
build_param('$VAR'(_), putparam, _, _, _, B, B).
build_param('$VAR'(_), putarg, _, _, Out, B, B) :-
    asm(addrefx, Out).
build_param(X, putparam, I, _, Out, B, [I|B]) :-
    compound(X),
    \+ ground_value(X),
    asm(addref, Out).
build_param(X, putarg, _, _, Out, B, B) :-
    \+ ground_value(X),
    asm(addref, Out).
build_param(_, _, _, _, _, B, B).

compile_invoke_arguments(Lc1, Lc, Out) :-
    label(Lc1, Lc, Lf),
    compile_invoke_arguments2(0, Lf, Out),
    asm([islist_t(Lf), puttailarg, label(Lf)], Out).

compile_invoke_arguments2(3, _, _).
compile_invoke_arguments2(I, Lf, Out) :-
    asm([islist_n(Lf), putarg(I), addrefx, pop], Out),
    I2 is I + 1,
    compile_invoke_arguments2(I2, Lf, Out).

drop_sequence(Env, Type, Lc1, Lc, Ld) :-
    drop_sequence(Env, [], Type, Lc1, Lc, Ld).

drop_sequence(Env, AEnv, Type, Lc1, Lc, Ld) :-
    findall(V, member(temp(V), AEnv), MEnv),
    subtract(Env, MEnv, Env1),
    sort(Env1, Env2),
    reverse(Env2, SEnv),
    drop_sequence2(SEnv, Type, Lc1, Lc, Ld).

drop_sequence2([], jump(L), Lc, Lc, L) :- !.
drop_sequence2(SEnv, Type, Lc, Lc, Ld) :-
    drop_sequence(SEnv, Type, Ld),
    !.
drop_sequence2(SEnv, Type, Lc1, Lc, Ld) :-
    label(Lc1, Lc, Ld),
    assertz(drop_sequence(SEnv, Type, Ld)).

compile_drop_sequences(Out) :-
    drop_sequence(Env, Type, Ld),
    compile_drop_sequence(Env, Type, Ld, Out),
    fail.
compile_drop_sequences(_).

compile_drop_sequence([V|Vars], Type, Ld, Out) :-
    asm([label(Ld), drop(V)], Out),
    drop_sequence(Vars, Type, Ld2),
    asm(jump(Ld2), Out),
    !.
compile_drop_sequence([_|Vars], _, _, Out) :-
    member(V, Vars),
    asm(drop(V), Out),
    fail.
compile_drop_sequence([], _, Ld, Out) :-
    asm(label(Ld), Out),
    fail.
compile_drop_sequence(_, done(A), _, Out) :-
    compile_drop_args(A, Out),
    asm([dropgoal, yield], Out),
    !.
compile_drop_sequence(_, yield, _, Out) :-
    asm(yield, Out),
    !.
compile_drop_sequence(_, jump, _, Out) :-
    asm([tailjump, dropgoal, yield], Out),
    !.
compile_drop_sequence(_, invoke, _, Out) :-
    asm(invoke_jump, Out),
    !.
compile_drop_sequence(_, suspend, _, Out) :-
    asm([suspend, yield], Out),
    !.
compile_drop_sequence(_, jump(L), _, Out) :-
    asm(jump(L), Out),
    !.
compile_drop_sequence(_, Type, _, _) :-
    error('bad drop sequence type: ~q\n', [Type]).

compile_drop_args(A, Out) :-
    compile_drop_args(A, 0, Out).

compile_drop_args(I, I, _).
compile_drop_args(_, 4, _).
compile_drop_args(I, J, Out) :-
    I > J,
    asm(droparg(J), Out),
    J2 is J + 1,
    compile_drop_args(I, J2, Out).

unref_arguments(Build, Out) :-
    member(P, Build),
    asm(unref(P), Out),
    fail.
unref_arguments(_, _).

save_registers(Build, Out) :-
    % if Build has odd number of args, push dummy to align stack
    push_dummy(Build, Out),
    member(PR, [0, 1, 2, 3]),
    member(PR, Build),
    asm(save(PR), Out),
    fail.
save_registers(_, _).

push_dummy(B, Out) :-
    length(B, L),
    L mod 2 =\= 0,
    !,
    asm(save(0), Out).
push_dummy(_, _).

restore_registers(Build, Out) :-
    member(PR, [3, 2, 1, 0]),
    member(PR, Build),
    asm(restore(PR), Out),
    fail.
restore_registers(Build, Out) :-
    length(Build, L),
    L mod 2 =\= 0,
    !,
    asm(pop, Out).
restore_registers(_, _).

register_literal(X, Lit) :- literal(X, Lit), !.
register_literal(N, Lit) :-
    float(N),
    !,
    add_literal(N, Lit).
register_literal([X|Y], Lit) :-
    !,
    sub_literal(X, XL),
    sub_literal(Y, YL),
    add_literal([XL|YL], Lit).
register_literal('{}'((X, Y)), Lit) :-
    !,
    tuple_args((X, Y), [Arg1|Args]),
    length(Args, A),
    sub_literal(Arg1, A1L),
    register_literal(Args, AL),
    maybe_add_literal(tuple(A, A1L, AL), Lit).
register_literal('{}'(X), Lit) :-
    % singleton tuple
    !,
    sub_literal(X, A1L),
    register_atom('[]', _), % in case [] is not yet registered
    maybe_add_literal(tuple0(A1L), Lit).
register_literal(X, Lit) :-
    X =.. [N|Args],
    length(Args, A),
    register_atom(N, NI),
    register_literal(Args, AL),
    maybe_add_literal(functor(NI, A, AL), Lit).
register_literal(X, _) :- error('internal error - bad literal: ~q\n', [X]).

maybe_add_literal(L, Lit) :- literal(L, Lit), !.
maybe_add_literal(L, Lit) :- add_literal(L, Lit).

sub_literal([], '[]') :- !, register_atom('[]', _).  % for swipl
sub_literal(X, X) :- atom(X), !, register_atom(X, _).
sub_literal(X, X) :- atomic(X), \+ float(X), !.
sub_literal(X, label(Lit)) :- register_literal(X, Lit).

add_literal(X, Lit) :-
    g_read(literal_counter, Lit),
    Lit2 is Lit + 1,
    g_assign(literal_counter, Lit2),
    assertz(literal(X, Lit)).

string_length([], 2) :- !.  % SWI-Prolog sillyness
string_length(S, Len) :- atom_length(S, Len).

register_symbol_loc(A, SL) :-
    g_read(symloc_counter, SL),
    SL2 is SL + 1,
    g_assign(symloc_counter, SL2),
    assertz(symbol_loc(SL, A)).

% read entry points from file

load_entry_points(Name) :-
    see(Name),
    read_entry_points,
    seen.

read_entry_points :-
    read_term(EP, []),
    add_entry_point(EP).

add_entry_point(end_of_file).
add_entry_point(entry_point(E, L)) :-
    asserta(entry_point(E, L)),
    read_entry_points.
add_entry_point(X) :-
    error('syntax error in link file: ~w\n', [X]).

% entry point file generation

dump_entry_points(_, Out) :-
    get_module_name(Mod),
    dump_entry_points2(Mod, Out).

dump_entry_points2(Mod, Out) :-
    pdef(NA, _),
    dump_entry_point(NA, Mod, Out),
    fail.
dump_entry_points2(_, _).

dump_entry_point(N/A, M, Out) :-
    visible(N/A),
    format_to_atom(X, '~a_~a_~d', [M, N, A]),
    mangle_name(X, Label),
    format(Out, 'entry_point(\'~a\':\'~a\'/~d, \'fl_e_~a\').\n', [M, N, A, Label]),
    !.
dump_entry_point(_, _, _).

visible(NA) :- exported(NA), !.
visible(_) :- exported(_), !, fail.
visible(NA) :- synthesized_name(NA, _), !, fail.
visible(_).

mangle_name(A, N) :-
    atom_codes(A, L),
    mangle_name2(L, L2),
    atom_codes(N, L2).

mangle_name2([], []).
mangle_name2([C|L], [C|L2]) :-
    (C >= 0'a, C =< 0'z ; C >= 0'A, C =< 0'Z; C >= 0'0, C =< 0'9; C = 0'_),
    !,
    mangle_name2(L, L2).
mangle_name2([C|L], L2) :-
    format_to_atom(X, '_~d', [C]),
    atom_codes(X, XL),
    append(XL, L3, L2),
    mangle_name2(L, L3).

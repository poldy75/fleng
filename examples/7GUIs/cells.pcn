/* Cells
   https://eugenkiss.github.io/7guis/tasks/

   This example is incomplete: the actual spreadsheet functionality
   (formula evaluation and recomputation) is currently missing.

    TODO:
    - formula parsing/dependency tracking
    - recomputation
    - readjust columns (shrink, according to widest column)
*/

#define CHAR_WIDTH      8

#define SCROLL_WIDTH    15

#define LEGEND_HEIGHT   15
#define LEGEND_WIDTH    50

#define CELL_HEIGHT     30
#define MIN_CELL_WIDTH  70

#define MAX_ROWS        100
#define MAX_COLUMNS     26

main() {||
    ezd:init([], ezd, ok),
    open_port(p, ps),
    bb:std_events(p, ezd, ezd2),
    ezd2 = [{"drawing", "cells"},
            {"drawing", "legend"},
            {"drawing", "ezd"},
            {"when", "*", "resize", ``fwd_event(p)``}|ezd3],
    list:make(MAX_COLUMNS, MIN_CELL_WIDTH, cols, []),
    //follow(ezd),
    if(data(ok))
        redraw_all(ps, {false, [], {0, 0}, cols, {0, 0.1}, {0, 0.1}}, p, ezd3)
}

follow(s) s ?= [x|s2] -> {writeln(x), follow(s2)}

// STATE = {FOCUS, DB, TOP, COLS, RNGV, RNGH}
// FOCUS = false | {"value", ID, TEXT} | {"formula", ID, TEXT}
// COLS = [WIDTH1, ...]
// DB: CELLID -> {FORMULA, VALUE, DEPENDENTS}

#define get_focus(state)    state[ 0 ]
#define get_db(state)       state[ 1 ]
#define get_top(state)      state[ 2 ]
#define get_cols(state)     state[ 3 ]
#define get_rv(state)       state[ 4 ]
#define get_rh(state)       state[ 5 ]

function total_width(cols) {
    app:foldl(`(x, y, z) -> z = x + y, cols, 0, w),
    return(w)
}

fwd_event(ev, event) { send(ev, event) }

drawing(d, ezd1, ezd) { ezd1 = [{"drawing", d}|ezd] }

redraw_all(s, state, ev, ezd) {||
    cols = get_cols(state),
    w = total_width(cols),
    drawing("legend", ezd, ezd1),
    draw_scrollbars(state, w, ev, state2, ezd1, ezd2),
    redraw_cells(s, state2, ev, ezd2)
}

redraw_cells(s, state, ev, ezd) {||
    cols = get_cols(state),
    w = total_width(cols),
    drawing("ezd", ezd, ezd1),
    bw = w + LEGEND_WIDTH,
    bh = CELL_HEIGHT * MAX_ROWS + LEGEND_HEIGHT,
    ezd1 = [{"object", "bg", [{"rectangle", LEGEND_WIDTH, LEGEND_HEIGHT,
                bw, bh, "clear"}]},
           {"when", "bg", {"buttondown", 1}, ``new_cell(ev)``}|ezd2],
    drawing("cells", ezd2, ezd3),
    draw_grid(w, cols, ezd3, ezd4),
    db = get_db(state),
    focus = get_focus(state),
    map:map_to_list(db, cells),
    draw_cells(cells, cols, focus, ev, ezd4, ezd5),
    redraw(s, state, ev, ezd5)
}

redraw(s, state, ev, ezd) {||
    drawing("legend", ezd, ezd5),
    draw_legend(state, ezd5, ezd6),
    drawing("cells", ezd6, ezd7),
    loop(s, false, state, ev, ezd7)
}

draw_grid(w, cols, ezd1, ezd) {||
    draw_hlines(0, w + LEGEND_WIDTH, ezd1, ezd2),
    draw_vlines(0, LEGEND_WIDTH, cols, ezd2, ezd)
}

draw_vlines(c, x, cols, ezd1, ezd) {||
    y2 = CELL_HEIGHT * MAX_ROWS,
    if(cols ?= [w|cols2]) {
        ezd1 = [{"object", {"vline", c}, [{"line", x, 0, x, y2, "gray"}]}|ezd2],
        draw_vlines(c + 1, x + w, cols2, ezd2, ezd)
    } else
        ezd1 = [{"object", {"vline", c}, [{"line", x, 0, x, y2, "gray"}]}|ezd]
}

draw_hlines(r, w, ezd1, ezd) {||
    y = r * CELL_HEIGHT + LEGEND_HEIGHT,
    if(r >= MAX_ROWS)
        ezd1 = [{"object", {"hline", r}, [{"line", 0, y, w, y, "gray"}]}|ezd]
    else  {||
        ezd1 = [{"object", {"hline", r}, [{"line", 0, y, w, y, "gray"}]}|ezd2],
        draw_hlines(r + 1, w, ezd2, ezd)
    }
}

draw_legend(state, ezd1, ezd) {||
    let state ?= {_, _, {tx, ty}, cols, _, _};
    sdl:window_size(ww, wh),
    draw_clegend(cols, 0, 0, tx, ww, ezd1, ezd3),
    draw_rlegend(0, 0, ty, wh, ezd3, ezd)
}

draw_clegend(cols, col, x, tx, ww, ezd1, ezd) {?
    cols ?= [w|cols2] -> {||
        xm = x - tx,
        {?
            xm >= ww -> ezd1 = [{"object", {"clegend", col}, []}|ezd2],
            x >= tx, xm < ww -> {||
                x1 = xm + LEGEND_WIDTH, x2 = x1 + w,
                xt = x1 + w / 2, yt = LEGEND_HEIGHT / 2,
                colname = 'A' + col,
                ezd1 = [{"object", {"clegend", col},
                            [{"box", x1, 0, x2, LEGEND_HEIGHT, "white"},
                                {"text", xt, yt, [colname]}]}|ezd2]
            },
            default -> ezd1 = [{"object", {"clegend", col}, []}|ezd2]
        },
        draw_clegend(cols2, col + 1, x + w, tx, ww, ezd2, ezd)
    },
    default -> ezd1 = ezd
}

draw_rlegend(row, y, ty, wh, ezd1, ezd) {?
    row >= MAX_ROWS -> ezd1 = ezd,
    default -> {||
        ym = y - ty,
        {?
            ym >= wh -> ezd1 = [{"object", {"rlegend", row}, []}|ezd2],
            y >= ty, ym < wh -> {||
                number_to_list(row, rowname, []),
                y1 = ym + LEGEND_HEIGHT, y2 = y1 + LEGEND_HEIGHT,
                xt = LEGEND_WIDTH / 2, yt = y1 + CELL_HEIGHT / 2,
                ezd1 = [{"object", {"rlegend", row}, [{"box", 0, y1, LEGEND_WIDTH, y2,
                            "white"}, {"text", xt, yt, rowname}]}|ezd2]
            },
            default -> ezd1 = [{"object", {"rlegend", row}, []}|ezd2]
        },
        draw_rlegend(row + 1, y + CELL_HEIGHT, ty, wh, ezd2, ezd)
    }
}

draw_scrollbars(state1, aw, ev, state, ezd1, ezd) {||
    org = get_top(state1),
    sdl:window_size(ww, wh),
    let org ?= {tx, ty};
    ah = CELL_HEIGHT * MAX_ROWS,
    rh1 = (ww - LEGEND_WIDTH - SCROLL_WIDTH) / real(aw),
    rv1 = (wh - LEGEND_HEIGHT - SCROLL_WIDTH) / real(ah),
    rhf = real(tx) / aw,
    rht = rhf + rh1,
    rvf = real(ty) / ah,
    rvt = rvf + rv1,
    rv = {rvf, rvt}, rh = {rhf, rht},
    state_update_ranges(state1, rv, rh, state),
    x1 = ww - SCROLL_WIDTH,
    h1 = wh - SCROLL_WIDTH,
    h2 = h1 - LEGEND_HEIGHT,
    w2 = ww - SCROLL_WIDTH - LEGEND_WIDTH,
    bb:scrollbar("vscroll", {x1, LEGEND_HEIGHT}, {SCROLL_WIDTH, h2}, "v", rv, ev,
        ezd1, ezd3),
    bb:scrollbar("hscroll", {LEGEND_WIDTH, h1}, {w2, SCROLL_WIDTH}, "h", rh, ev,
        ezd3, ezd)
}

draw_cells(cells, cols, focus, ev, ezd1, ezd) {?
    cells ?= [{_, id, {_, v, _}}|cells2] -> {||
        if(focus ?= {_, id2, text}, id == id2)
            draw_cell(id, text, cols, id, ev, ezd1, ezd2)
        else draw_cell(id, v, cols, false, ev, ezd1, ezd2),
        draw_cells(cells2, cols, focus, ev, ezd2, ezd)
    },
    default -> ezd1 = ezd
}

draw_cell(id, text, cols, focusid, ev, ezd1, ezd) {||
    box = id_to_box(id, cols),
    let box ?= {pos, size};
    bb:entry(id, pos, size, text, focusid, ev, ezd1, ezd)
}

loop(s, click, state, ev, ezd) {?
    s ?= [{"resize", _, _}|s2] -> redraw_all(s2, state, ev, ezd),
    s ?= [{"scroll", "vscroll", info}|s2] -> {||
        rh = get_rh(state),
        drawing("legend", ezd, ezd2),
        bb:scrollbar_move("vscroll", info, rv2, ezd2, ezd3),
        drawing("cells", ezd3, ezd4),
        update_origin(state, rv2, rh, state2, ezd4, ezd5),
        redraw(s2, state2, ev, ezd5)
    },
    s ?= [{"scroll", "hscroll", info}|s2] -> {||
        rv = get_rv(state),
        drawing("legend", ezd, ezd2),
        bb:scrollbar_move("hscroll", info, rh2, ezd2, ezd3),
        drawing("cells", ezd3, ezd4),
        update_origin(state, rv, rh2, state2, ezd4, ezd5),
        redraw(s2, state2, ev, ezd5)
    },
    s ?= [{"new", x, y}|s2] -> {
        cols = get_cols(state),
        top = get_top(state),
        let top ?= {tx, ty};
        id = pos_to_id(x + tx - LEGEND_WIDTH, y + ty - LEGEND_HEIGHT, 0, cols),
        add_cell(id, ev, state, state2, ezd, ezd2),
        loop(s2, false, state2, ev, ezd2)
    },
    s ?= [{"focus", id}|s2] -> {||
        refocus(id, ev, state, state2, ezd, ezd2),
        loop(s2, false, state2, ev, ezd2)
    },
    s ?= [{"textinput", c}|s2] -> user_input(c, s2, state, ev, ezd),
    s ?= [{"keydown", 8}|s2] -> user_input(c, s2, state, ev, ezd),
    default -> { let s ?= [_|s2]; loop(s2, click, state, ev, ezd) }
}

user_input(c, s, state, ev, ezd) {||
    focus = get_focus(state),
    if(focus ?= {m, id, text}) {||
        bb:entry_update(text, c, text2),
        update_focus({m, id, text2}, state, state2),
        draw_and_adjust_column(s, id, text2, ev, state2, ezd)
    } else loop(s, false, state, ev, ezd)
}

refocus(id, ev, state1, state, ezd1, ezd) {||
    cols = get_cols(state1),
    old = get_focus(state1),
    if(old ?= {m, oldid, oldtext}) {
        oldcell = get_cell(oldid, state1),
        let oldcell ?= {f, v, dep};
        if(m == "value") {
            f2 = f,
            v2 = oldtext
        } else {
            f2 = oldtext,
            v2 = v
        },
        set_cell(oldid, {f2, v2, dep}, state1, state2),
        draw_cell(oldid, v2, cols, id, ev, ezd1, ezd2),
        cell = get_cell(id, state1),
        let cell ?= {_, text, _};
        draw_cell(id, text, cols, id, ev, ezd2, ezd3),
        update_focus({"value", id, text}, state2, state3),
        recompute(dep, state3, state, ezd3, ezd)
    } else {
        cell = get_cell(id, state1),
        if(cell ?= {_, oldtext, _}) text = oldtext
        else text = [],
        update_focus({"value", id, text}, state1, state),
        draw_cell(id, text, cols, id, ev, ezd1, ezd)
    }
}

function pos_to_id(x, y, c, cols) {?
    cols ?= [w|cols2] -> {
        r = y / CELL_HEIGHT,
        if(x < w) return({r, c})
        else return(pos_to_id(x - w, y, c + 1, cols2))
    },
    default -> return(false)
}

function id_to_box(id, cols) {||
    let id ?= {r, c};
    id_to_box2(r, c, 0, cols, box),
    return(box)
}

id_to_box2(r, c, x, cols, box) {?
    cols ?= [w|_], c == 0 -> {
        x1 = LEGEND_WIDTH + x,
        y1 = r * CELL_HEIGHT + LEGEND_HEIGHT,
        box = {{x1, y1}, {w, CELL_HEIGHT}}
    },
    cols ?= [w|cols2], c > 0 -> id_to_box2(r, c - 1, x + w, cols2, box),
    default -> box = false
}

update_origin(state1, rv, rh, state, ezd1, ezd) {||
    let state1 ?= {focus, db, _, cols, _, _},
    let rv ?= {rvf, _},
    let rh ?= {rhf, _},
    w = total_width(cols),
    tx = integer(w * rhf), ty = integer(CELL_HEIGHT * MAX_ROWS * rvf),
    ox = -tx, oy = -ty,
    ezd1 = [{"origin", "cells", ox, oy}|ezd],
    state = {focus, db, {tx, ty}, cols, rv, rh}
}

state_update_ranges(state1, rv, rh, state) {||
    let state1 ?= {focus, db, top, cols, _, _},
    state = {focus, db, top, cols, rv, rh}
}

function get_cell(id, state) {
    map:lookup(id, get_db(state), x),
    return(x)
}

set_cell(id, val, state1, state) {||
    let state1 ?= {focus, db, top, cols, rv, rh};
    map:insert(id, val, db, db2),
    state = {focus, db2, top, cols, rv, rh}
}

new_cell(ev, event) {||
    let event ?= {_, _, x, y};
    send(ev, {"new", x, y})
}

add_cell(id, ev, state1, state, ezd1, ezd) {||
    let state1 ?= {focus, db, top, cols, rv, rh};
    map:insert(id, {[], [], []}, db, db2),
    state2 = {focus, db2, top, cols, rv, rh},
    refocus(id, ev, state2, state, ezd1, ezd)
}

update_focus(focus, state1, state) {||
    let state1 ?= {_, db, top, cols, rv, rh};
    state = {focus, db, top, cols, rv, rh}
}

focus_text(id, state, text) {
    focus = get_focus(state),
    if(focus ?= {_, id, text1}) text = text1
    else text = []
}

draw_and_adjust_column(s, id, text, ev, state, ezd) {||
    let id ?= {_, c};
    n = (length(text) + 2) * CHAR_WIDTH,
    cols = get_cols(state),
    clen = cols[ c ],
    {?
        n > clen -> {
            let state ?= {focus, db, top, _, rv, rh};
            widen(c, cols, n, cols2),
            redraw_cells(s, {focus, db, top, cols2, rv, rh}, ev, ezd)
        },
        default -> {||
            draw_cell(id, text, cols, id, ev, ezd, ezd2),
            loop(s, false, state, ev, ezd2)
        }
    }
}

widen(c, cols, w, cols2) {?
    cols ?= [_|cols1], c == 0 -> cols2 = [w|cols1],
    cols ?= [w1|cols1], c > 0 -> {
        cols2 = [w1|cols3],
        widen(c - 1, cols1, w, cols3)
    },
    default -> cols2 = []
}

recompute(dep, state1, state, ezd1, ezd) {
    //XXX
    state = state1,
    ezd1 = ezd,
    writeln({"recompute", dep})
}

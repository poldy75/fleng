%= "fmt" module: Formatted output
%
% Support for formatted output and string construction. These
% operations take a string describing a format and a list of
% arguments to be displayed, depending on the formatting instructions
% encoded in the string.
%
% Character sequences in the format string are interpreted in the
% following manner:
%
%         ~~  Output the "~" (tilde) character.
%
%         ~a  Output the unquoted characters of the next argument
%             which must be a string.
%
%         ~s  Output the string, character list, char or int array given in the
%             next argument.
%
%         ~d  Output the next argument, which must be a number, in
%             decimal notation.
%
%         ~x  Output the next argument, which must be a number, in
%             hexadecimal notation.
%
%         ~w  Output the next argument term in FGHC term syntax.
%
%         ~q  Output the next argument term in FGHC term syntax and with
%             strings quoted, if necessary.
%
%         ~c  Output the next argument which must be an integer as
%             a single character.
%
%         ~n  Output a newline character.
%
%         ~?  Take a format-string and argument-list, format recursively.
%
% Use "format_chars" to format into a character list instead of writing
% the result to an output file.
%
%-
% fmt:format(STRING?, ARGS?)
% fmt:format(FILE?, STRING?, ARGS?)
% fmt:format(FILE?, STRING?, ARGS?, DONE^)
%       Writes the values in the list ARGS in a manner described by
%       the format string STRING to FILE or to standard output.
%       STRING may be a string or a list of character codes and may
%       contain special characters describing how to output the next
%       item in ARGS. Assigns [] to DONE when finished.
%
% fmt:format_chars(STRING?, ARGS?, OUT^)
% fmt:format_chars(STRING?, ARGS?, OUT^, TAIL?)
%       Format to list of character codes and assign to OUT,
%       optionally with a list tail, which defaults to the empty_list.
%       STRING may be a string or a list of characters.
%
% fmt:format_chunked(FILE?, CHUNKS?)
% fmt:format_chunked(FILE?, CHUNKS?, DONE^)
%       Format elements of the "chunk" list CHUNKS and write the output
%       to file. CHUNKS is a list of output-specifications, as produced
%       by "fmt:parse_format/3".
%
% fmt:format_chars_chunked(CHUNKS?, OUT^, TAIL?)
%       Format elements of the "chunk" list CHUNKS and create the character
%       list OUT, terminated by TAIL.
%       CHUNKS is a list of output-specifications, as produce by
%       "fmt:parse_format/3".
%
% fmt:parse_format(LIST?, ARGUMENTS^, CHUNKS^)
%       Parses the format-specifications in the character list LIST with
%       arguments taken from the list ARGUMENTS and produces a "chunk" list
%       in CHUNKS, terminated by TAIL. If LIST contains invalid formatting
%       specifications, or if arguments are missing, the operation aborts
%       with an error.

-module(fmt).

format_var_tag(TAG, DONE) :-
    put_global(fmt_var_tag, TAG, DONE).

format(STR, ARGS) :- format(1, STR, ARGS, _).
format(FD, STR, ARGS) :- format(FD, STR, ARGS, _).

format(FD, STR, ARGS, DONE) :-
    format_chars(STR, ARGS, OUT),
    utf_encode(OUT, OUT2),
    write_file(FD, OUT2, DONE).

format_chunked(FD, Lst) :- format_chunked(FD, Lst, _).

format_chunked(FD, Lst, DONE) :-
    format_chars_chunked(Lst, Out, []),
    utf_encode(Out, Out2),
    write_file(FD, Out2, DONE).

format_chars(STR, ARGS, OUTS, TAIL) :-
    to_list(STR, FMT),
    parse_format(FMT, ARGS, Items),
    format_chars_chunked(Items, OUTS, TAIL).

format_chars(STR, ARGS, OUTS) :-
    format_chars(STR, ARGS, OUTS, []).

to_list(X, L) :- string(X) | string_to_list(X, L, []).
to_list(X, L) :- '$kl1string'(X) | array:kl1_string_to_list(X, L, []).
to_list(X, L) :- list(X) | L = X.
to_list(X, _) :-
    otherwise |
    error('format: expected string or list'(X)).

parse_format([], [], O) :- O = [].
parse_format([], ARGS, _) :-
    ARGS =\= [] |
    error('format: too many arguments'(ARGS)).
parse_format([0'~, C|R], ARGS, O) :-
    parse_format2(C, R, ARGS, O).
parse_format([C|R], ARGS, O) :-
    otherwise |
    O = [C|O2],
    parse_format(R, ARGS, O2).

parse_format2(0'n, R, ARGS, O) :-
    O = [10|O2], parse_format(R, ARGS, O2).
parse_format2(0'~, R, ARGS, O) :-
    O = [0'~|O2], parse_format(R, ARGS, O2).
parse_format2(0'c, R, [C|ARGS], O) :-
    O = [C|O2], parse_format(R, ARGS, O2).
parse_format2(0'?, R, [F, X|ARGS], O) :-
    O = [f(F, X)|O2], parse_format(R, ARGS, O2).
parse_format2(C, R, [X|ARGS], O) :-
    otherwise |
    list:member(C, "axsqdw", FC),
    parse_format3(FC, C, R, X, ARGS, O).

parse_format3(true, C, R, X, ARGS, O) :-
    list_to_string([C], CS),
    list_to_tuple([CS, X], T),
    O = [T|O2], parse_format(R, ARGS, O2).
parse_format3(false, C, _, X, ARGS, _) :-
    otherwise |
    error('format: bad formatting character'(C, [X|ARGS])).

format_chars_chunked([], O1, O) :- O1 = O.
format_chars_chunked([X|R], O1, O) :-
    output(X, O1, O2),
    format_chars_chunked(R, O2, O).

output(d(X), O1, O) :- number(X) | number_to_list(X, 10, O1, O).
output(x(X), O1, O) :- integer(X) | number_to_list(X, 16, O1, O).
output(a(X), O1, O) :- string(X) | string_to_list(X, O1, O).
output(s(X), O1, O) :- output_s(X, O1, O).
output(q(X), O1, O) :- expr(X, 0, q, S, []), format_chars_chunked(S, O1, O).
output(w(X), O1, O) :- expr(X, 0, w, S, []), format_chars_chunked(S, O1, O).
output(f(F, X), O1, O) :-
    to_list(F, FL),
    parse_format(FL, X, S),
    format_chars_chunked(S, O1, O).
output({F, _, _}, _, _) :-
    error('fmt: "~*..." formatting directive not supported'(F)).
output(X, O1, O) :- integer(X) | O1 = [X|O].
output(X, O1, O) :- string(X) | string_to_list(X, O1, O).
output(X, O1, O) :-
    module(X) |
    module_name(X, N),
    string_to_list(N, NS, [0'\', 0'>|O]),
    list:append("<module '", NS, O1).
output(X, _, _) :-
    otherwise |
    error('fmt: argument to format has no printable representation'(X)).

output_s([], O1, O) :- O1 = O.
output_s(X, O1, O) :- string(X), X =\= [] | string_to_list(X, O1, O).
output_s(X, O1, O) :-
    list(X) |
    verify_char_list(X, X, Ok),
    when(Ok, list:append(X, O, O1)).
output_s(X, O1, O) :-
    array(X) |
    array:type(X, T),
    output_a(T, X, O1, O).
output_s(X, _, _) :-
    otherwise |
    error('fmt: argument to "~s" does not have required type'(X)).

output_a(char, A, O1, O) :-
    length(A, Len),
    array:get(0, Len, A, Bytes),
    utf_decode(Bytes, Chars),
    list:append(Chars, O, O1).
output_a(kl1string, A, O1, O) :-
    array:kl1_string_to_list(A, Bytes, []),
    utf_decode(Bytes, Chars),
    list:append(Chars, O, O1).
output_a(int, A, O1, O) :-
    length(A, Len),
    array:get(0, Len, A, O1, O).
output_a(_, X, _, _) :-
    otherwise |
    error('fmt: argument to "~s" is not a char or int array'(X)).

verify_char_list([], _, Ok) :- Ok := [].
verify_char_list([C|L], X, Ok) :-
    integer(C), C > 0 | verify_char_list(L, X, Ok).
verify_char_list(_, X, _) :-
    otherwise |
    error('fmt: not a string or character list'(X)).

expr('', _, q, O, O2) :- O = ['\'\''|O2].
expr('$VAR'(VT, N, Name), P, M, O, O2) :-
    integer(VT), integer(N) |
    get_global(fmt_var_tag, Tag, 0),
    fmt_var(Tag, VT, N, Name, P, M, O, O2).
expr(X, P, M, O, O2) :- otherwise | expr2(X, P, M, O, O2).

fmt_var(false, _, N, _, _, _, O, O2) :-
    number_to_list(N, L, O2),
    O = [0'_|L].
fmt_var(Tag, VT, _, Name, _, _, O, O2) :-
    integer(Tag), Tag =\= VT, string(Name) |
    O = [Name|O2].
fmt_var(Tag, VT, N, 0, _, _, O, O2) :-
    integer(Tag), Tag =\= VT |
    number_to_list(N, L, O2),
    O = [0'_|L].
fmt_var(_, VT, N, Name, P, M, O, O2) :-
    otherwise |
    expr2('$VAR'(VT, N, Name), P, M, O, O2).

expr2(X, _, q, O, O2) :-
    list(X) |
    expr_string_or_list(X, Str, O2, F),
    (F == true ->
         O = [0'"|Str]
    ;
         O = [0'[|O3],
         expr_list(X, q, O3, O4),
         O4 = [0']|O2]
    ).
expr2(X, _, w, O, O2) :-
    list(X) |
    O = [0'[|O3],
    expr_list(X, w, O3, O4),
    O4 = [0']|O2].
expr2(X, P, M, O, O2) :-
    tuple(X) |
    tuple_to_list(X, T, []),
    expr0(T, P, M, O, O2).
expr2(X, _, M, O, O2) :-
    vector(X) |
    vector:elements(X, L, []),
    O = [0'#, 0'{|O3],
    expr_list(L, M, O3, O4),
    O4 = [0'}|O2].
expr2(X, _, _, O, O2) :-
    '$kl1string'(X) |
    O = [0'#, 0'$|O1],
    array:binary_to_hex(X, O1, O2).

expr2(X, _, M, O, O2) :-
    array(X) |
    array:type(X, T),
    (T == char ->
        expr_charbuf(X, M, O, O2)
    ;
        O = [X|O2]
    ).
expr2(X, _, _, O, O2) :- number(X) | O = [d(X)|O2].
expr2(X, _, M, O, O2) :- string(X) | expr2b(X, M, O, O2).
expr2(X, _, _, O, O2) :- otherwise | O = [X|O2].

expr_charbuf(X, q, O, O2) :-
    foreign_call(fl_array_printable(X, F)),
    (F == true ->
        O = [0'#, 0'"|O1],
        length(X, Len),
        array:get(0, Len, X, Data),
        expr_string_or_list(Data, O1, O2, _)
    ;
        expr_charbuf(X, w, O, O2)
    ).
expr_charbuf(X, w, O, O2) :-
    O = [0'#|O1],
    array:binary_to_hex(X, O1, O2).

expr_string_or_list([], Str, T, F) :-
    Str = [0'"|T],
    F = true.
expr_string_or_list([9|L], Str, T, F) :-
    Str = [0'\\, 0't|Str2],
    expr_string_or_list(L, Str2, T, F).
expr_string_or_list([10|L], Str, T, F) :-
    Str = [0'\\, 0'n|Str2],
    expr_string_or_list(L, Str2, T, F).
expr_string_or_list([0'"|L], Str, T, F) :-
    Str = [0'\\, 0'"|Str2],
    expr_string_or_list(L, Str2, T, F).
expr_string_or_list([0'\\|L], Str, T, F) :-
    Str = [0'\\, 0'\\|Str2],
    expr_string_or_list(L, Str2, T, F).
expr_string_or_list([C|L], Str, T, F) :-
    integer(C), C >= 32, C < 127, C =\= 0'", C =\= 0'\\ |
    Str = [C|Str2],
    expr_string_or_list(L, Str2, T, F).
expr_string_or_list(_, _, _, F) :-
    otherwise | F = false.

expr2b(X, M, O, O2) :-
    op(X/2, _, A),
    (A == none  ->  op(X/1, _, B); B = none),
    (B == none  ->
        expr2c(X, M, O, O2)
    ;
        exprpara(X, M, O, O2)
    ).

exprpara(X, M, O, O2) :-
    O = [0'(|O3],
    expr2c(X, M, O3, O4),
    O4 = [0')|O2].

expr2c(X, w, O, O2) :- O = [X|O2].
expr2c(X, q, O, O2) :-
    string_to_list(X, L, []),
    qstring(L, X, O, O2).

expr0([H|A], P, M, O, O2) :-
    string(H), A =\= [] |
    length(A, N),
    op(H/N, PP, ASS),
    expr_op(H, A, P, PP, ASS, M, O, O2).
expr0(T, _, M, O, O2) :-
    otherwise | expr_tuple(T, M, O, O2).

expr_op(OP, A, _, 11, _, M, O, O2) :-
    expr_tuple([OP|A], M, O, O2).
expr_op(OP, Args, P, PP, As, M, O, O2) :-
    otherwise | expr_op2(OP, Args, P, PP, As, M, O, O2).

expr_op2(OP, [X], P, PP, fy, M, O, O2) :-
    PP >= P |
    O = [OP, 32|O3],
    expr(X, PP, M, O3, O2).
expr_op2(OP, [X], P, PP, fx, M, O, O2) :-
    PP >= P |
    P2 is PP + 1,
    O = [OP, 32|O3],
    expr(X, P2, M, O3, O2).
expr_op2(OP, [X, Y], P, PP, xfx, M, O, O2) :-
    PP >= P |
    P2 is PP + 1,
    expr(X, P2, M, O, O4),
    O4 = [32, OP, 32|O5],
    expr(Y, P2, M, O5, O2).
expr_op2(OP, [X, Y], P, PP, xfy, M, O, O2) :-
    PP >= P |
    P2 is PP + 1,
    expr(X, P2, M, O, O3),
    O3 = [32, OP, 32|O4],
    expr(Y, PP, M, O4, O2).
expr_op2(OP, [X, Y], P, PP, yfx, M, O, O2) :-
    PP >= P |
    P2 is PP + 1,
    expr(X, PP, M, O, O4),
    O4 = [32, OP, 32|O5],
    expr(Y, P2, M, O5, O2).
expr_op2(OP, ARGS, _, PP, ASS, M, O, O2) :-
    otherwise |
    O = [0'(|O3],
    expr_op(OP, ARGS, PP, PP, ASS, M, O3, O4),
    O4 = [0')|O2].

expr_tuple([H|A], M, O, O2) :-
    string(H), H =\= [], A =\= [] |
    expr2b(H, M, O, [0'(|O3]),
    expr_list(A, M, O3, O4),
    O4 = [0')|O2].
expr_tuple(T, M, O, O2) :-
    otherwise |
    O = [0'{|O3],
    expr_list(T, M, O3, O4),
    O4 = [0'}|O2].

expr_list([], _, O, O2) :- O = O2.
expr_list([X], M, O, O2) :- expr(X, 5, M, O, O2).
expr_list([X, Y|R], M, O, O2) :-
    expr(X, 5, M, O, O3),
    O3 = [0',, 32|O4],
    expr_list([Y|R], M, O4, O2).
expr_list([X|R], M, O, O2) :-
    otherwise |
    expr(X, 5, M, O, O3),
    O3 = [0'||O4],
    expr(R, 2, M, O4, O2).

op((':-')/2, P, A) :- P = 0, A = xfx.
op(('|')/2, P, A) :- P = 1, A = xfx.
op((';')/2, P, A) :- P = 2, A = xfx.
op(('->')/2, P, A) :- P = 3, A = xfx.
op((',')/2, P, A) :- P = 4, A = xfy.
op(('=')/2, P, A) :- P = 5, A = xfx.
op((':=')/2, P, A) :- P = 5, A = xfx.
op(('<=')/2, P, A) :- P = 5, A = xfx.
op(('=>')/2, P, A) :- P = 5, A = xfx.
op(('<==')/2, P, A) :- P = 5, A = xfx.
op(('+=')/2, P, A) :- P = 5, A = xfx.
op(('-=')/2, P, A) :- P = 5, A = xfx.
op(('*=')/2, P, A) :- P = 5, A = xfx.
op(('/=')/2, P, A) :- P = 5, A = xfx.
op((is)/2, P, A) :- P = 5, A = xfx.
op(('<')/2, P, A) :- P = 6, A = xfx.
op(('>')/2, P, A) :- P = 6, A = xfx.
op(('>=')/2, P, A) :- P = 6, A = xfx.
op(('=<')/2, P, A) :- P = 6, A = xfx.
op(('==')/2, P, A) :- P = 6, A = xfx.
op(('=\\=')/2, P, A) :- P = 6, A = xfx.
op(('=:=')/2, P, A) :- P = 6, A = xfx.
op(('\\=:=')/2, P, A) :- P = 6, A = xfx.
op(('@>')/2, P, A) :- P = 6, A = xfx.
op(('@<')/2, P, A) :- P = 6, A = xfx.
op(('@>=')/2, P, A) :- P = 6, A = xfx.
op(('@=<')/2, P, A) :- P = 6, A = xfx.
op(('@')/2, P, A) :- P = 6, A = xfx.
op((':')/2, P, A) :- P = 7, A = xfx.
op(('+')/2, P, A) :- P = 8, A = yfx.
op(('-')/2, P, A) :- P = 8, A = yfx.
op(('/\\')/2, P, A) :- P = 8, A = yfx.
op(('\\/')/2, P, A) :- P = 8, A = yfx.
op(('><')/2, P, A) :- P = 8, A = yfx.
op(('*')/2, P, A) :- P = 9, A = yfx.
op(('/')/2, P, A) :- P = 9, A = yfx.
op(('<<')/2, P, A) :- P = 9, A = yfx.
op(('>>')/2, P, A) :- P = 9, A = yfx.
op(('\\\\')/2, P, A) :- P = 9, A = yfx.
op(('+')/1, P, A) :- P = 10, A = fy.
op(('-')/1, P, A) :- P = 10, A = fy.
op(('\\')/1, P, A) :- P = 10, A = fy.
op(('!')/1, P, A) :- P = 10, A = fx.
op(_, P, A) :- otherwise | P = 11, A = none.

qstring("", _, O, O2) :- O = ['\'\''|O2].
qstring("[]", S, O, O2) :- O = [S|O2].
qstring([0'_|R], S, O, O2) :-
    qstring2([0'_|R], yes, S, O, O2).
qstring([C|R], S, O, O2) :-
    otherwise |
    ucs:lower(C, F),
    qstring_u(F, C, R, S, O, O2).

qstring_u(false, C, R, S, O, O2) :-
    qstring2([C|R], yes, S, O, O2).
qstring_u(true, C, R, S, O, O2) :-
    otherwise |
    X = [C|R],
    qstring1(X, Q),
    qstring2(X, Q, S, O, O2).

qstring1([], Q) :- Q = no.
qstring1([0'_|R], Q) :- qstring1(R, Q).
qstring1([C|R], Q) :-
    otherwise |
    ucs:alpha(C, F),
    qstring1_u(F, C, R, Q).

qstring1_u(true, _, R, Q) :- qstring1(R, Q).
qstring1_u(false, C, R, Q) :-
    C >= 0'0, C =< 0'9 | qstring1(R, Q).
qstring1_u(false, _, _, Q) :-
    otherwise | Q = yes.

qstring2(L, yes, _, O, O2) :-
    O = [0'\'|O3],
    qstring3(L, O3, O2).
qstring2(_, no, S, O, O2) :- O = [S|O2].

qstring3([], O, O2) :- O = [0'\'|O2].
qstring3([0'\\|R], O, O2) :-
    O = ['\\\\'|O3],
    qstring3(R, O3, O2).
qstring3([0'\'|R], O, O2) :-
    O = ['\\\''|O3],
    qstring3(R, O3, O2).
qstring3([10|R], O, O2) :-
    O = ['\\n'|O3],
    qstring3(R, O3, O2).
qstring3([13|R], O, O2) :-
    O = ['\\r'|O3],
    qstring3(R, O3, O2).
qstring3([9|R], O, O2) :-
    O = ['\\t'|O3],
    qstring3(R, O3, O2).
qstring3([C|R], O, O2) :-
    otherwise |
    (C < 32 ->
        hex_escape(C, H, O3),
        O = [0'\\, 0'x|H]
    ;
        O = [C|O3]
    ),
    qstring3(R, O3, O2).

hex_escape(C, H, T) :- C < 16 | H = [0'0|H2], number_to_list(C, 16, H2, T).
hex_escape(C, H, T) :- otherwise | number_to_list(C, 16, H, T).

%= "list" module: List operations
%
% This module provides various types of operations on lists, like
% extraction ("take", "drop", "cut", "slice", "split"), appending ("append",
% "join"), searching ("member", "search", "scan") and deleting elements
% ("trim", "delete").
%
%-
% list:take(NUM?, LIST?, RESULT^, TAIL?)
%       Copy the first NUM elements of LIST into RESULT, terminated by TAIL.
%
% list:drop(NUM?, LIST?, RESULT^)
%       Remove the first NUM elements from LIST and unify RESULT with the
%       remaining list.
%
% list:cut(NUM?, LIST?, TAIL?, RESULT^, REST^)
%       Copy the first NUM elements of LIST into RESULT, terminated by TAIL
%       and unify REST with the remaining list.
%
% list:make(NUM?, LIST^, TAIL?)
% list:make(NUM?, VALUE?, LIST^, TAIL?)
%       Create a list of NUM elements, initialized to VALUE and terminated by
%       TAIL. If VALUE is not given, initialize each element to a fresh
%       variable.
%
% list:split(LIST?, SEP?, RESULT^)
% list:split(LIST?, SEP?, RESULT^, TAIL?)
%       Splits the list into sublists, separated by SEP.
%
% list:scan(LIST?, X?, RLIST^, TAIL^)
% list:scan(LIST?, X?, RLIST^, RTAIL^, TAIL^)
%       Collect elements of LIST in RLIST until the end of LIST or element
%       occurs X and assign remaining elements to TAIL. If given, the end
%       of RLIST is terminated with RTAIL or the empty list otherwise.
%
% list:iota(N?, LIST^, TAIL?)
% list:iota(N?, START?, LIST^, TAIL?)
% list:iota(N?, START?, STEP?, LIST^, TAIL?)
%       Creates a sequence of N integers START, START + STEP, ... .
%       START and STEP default to 1. The result list is terminated with TAIL
%       and unified with LIST.
%
% list:slice(INDEX?, NUM?, LIST?, RESULT^, TAIL?)
%       Returns a list of NUM elements, starting at index INDEX in LIST,
%       terminating the extracted list with TAIL. Indexing starts at 1.
%
% list:search(NEEDLE?, HAYSTACK?, POSITION^)
% list:search(NEEDLE?, HAYSTACK?, PREFIX^, TAIL?, REST^)
%       Searches for the list NEEDLE in HAYSTACK and either assigns the
%       index of the sublist to POSITION (or 0 if not found) or unifies
%       PREFIX with the list of elements in HAYSTACK preceeding NEEDLE
%       (terminated by TAIL) and REST with all remaining elements.
%
%+ list:trim trim_left trim_right
% list:trim(SET?, LIST?, RESULT^)
% list:trim_left(SET?, LIST?, RESULT^)
% list:trim_right(SET?, LIST?, RESULT^)
%       Remove elements from LIST found in SET from start (left), end
%       (right) or both end and unify the result list with RESULT.
%
% list:delete(ITEM?, LIST?, RESULT^)
%       Remove all elements equal to ITEM from LIST and unify the resulting
%       list with RESULT.
%
% list:reverse(LIST?, RESULT^)
% list:reverse(LIST?, RESULT^, TAIL?)
%       Reverse list and unify with RESULT, optionally terminated by TAIL.
%
% list:append(LISTS?, LIST^)
% list:append(LIST1?, LIST2?, LIST^)
%       Concatenates all lists in LISTS (or both LIST1 and LIST2) and
%       unifies the result with LIST.
%
% list:member(VALUE?, LIST?, BOOL^)
%       Unifies BOOL with the string "true" or "false", depending on
%       whether LIST contains a toplevel element that matches VALUE.
%
% list:last(LIST?, RESULT^)
%       Unifies RESULT with the last element of LIST (or the empty list
%       if LIST is empty).
%
% list:butlast(LIST?, RESULT?)
%       Unifies RESULT with the elements of LIST, except the last item.
%       If LIST is the empty list, then so is RESULT.
%
% list:prefix(PREFIX?, LIST?, RESULT^)
%       Unifies RESULT with the string "true" or "false", depending
%       on whether LIST begins with the list PREFIX or not.
%
% list:suffix(SUFFIX?, LIST?, RESULT^)
%       Unifies RESULT with the string "true" or "false", depending
%       on whether LIST ends with the list SUFFIX or not.
%
% list:join(LIST?, SEP?, RESULT^)
% list:join(LIST?, SEP?, RESULT^, TAIL?)
%       Unifies RESULT with all elements of the list of lists LIST,
%       separated by SEP. The final result is terminated with TAIL,
%	   or the empty list if TAIL is not given.
%
% list:characters(THING?, LIST^)
% list:characters(THING?, LIST^, TAIL?)
%    Converts THING, which should be a string or number, into a list of
%    characters and unifies the result with LIST, optionally terminated
%    by TAIL, which defaults to the empty list. If THING is already a
%    list, it is not changed.
%
% list:assoc(PROP?, LIST?, VALUE^)
% list:assoc(PROP?, LIST?, VALUE^, DEFAULT?)
%   Searches for a tuple or list whose first element is PROP in LIST and
%   unifies VALUE with the corresponding list element, if found, or with
%   DEFAULT, otherwise. If DEFAULT is not given, "false" is used.
%
% list:zip(LIST1?, LIST2?, RESULT^)
%   Unifies RESULT with a list of 2-element tuples containing the
%   elements of LIST1 and LIST2.
%
% list:nth(INDEX?, LIST?, RESULT^)
%   Unify RESULT with the INDEXth item of LIST, starting from index 1.
%   Signals an error if the list is too short.
%
% list:replace(OLD?, NEW?, LIST?, RESULT^)
%	Replace the sequence in the list OLD with the sequence in NEW in
%	LIST and unify the result with RESULT. OLD and NEW may also be
%	atomic and represent the respective single-element list.
%
% list:index(X?, LIST?, INDEX^)
%	Assign the first position of X in LIST to INDEX, or assign the string "false"
%	if X can not be found in LIST.
%
% list:count(X?, LIST?, COUNT^)
%    Count occurrences of X in LIST and assign the result to COUNT.

-module(list).

cut(0, L, T, R, RL) :- R = T, RL = L.
cut(N, [], T, R, RL) :- N > 0 | R = T, RL = [].
cut(N, [X|L], T, R, RL) :-
    otherwise, N > 0 |
    N2 is N - 1, R = [X|R2], cut(N2, L, T, R2, RL).

take(0, _, R, T) :- R = T.
take(N, [], R, T) :- N > 0 | R = T.
take(Cnt, [X|L], R, T) :-
    otherwise, Cnt > 0 |
    Cnt2 is Cnt - 1, R = [X|R2], take(Cnt2, L, R2, T).

drop(0, L, R) :- R = L.
drop(N, [], R) :- N > 0 | R = [].
drop(N, [_|L], R) :- otherwise, N > 0 | N2 is N - 1, drop(N2, L, R).

slice(P, N, L, R, T) :-
    P2 is P - 1,
    list:drop(P2, L, L1),
    list:take(N, L1, R, T).

nth(1, [X|_], R) :- R = X.
nth(N, [_|L], R) :-
    otherwise| N2 is N - 1, nth(N2, L, R).

make(0, R, T) :- R = T.
make(N, R, T) :-
    otherwise, N > 0 | R = [_|R2], N2 is N - 1, make(N2, R2, T).

make(0, _, R, T) :- R = T.
make(N, X, R, T) :-
    otherwise, N > 0 | R = [X|R2], N2 is N - 1, make(N2, X, R2, T).

split(L, S, R) :- split(L, S, R, []).

split([], _, R, T) :- R = T.
split([S|L], S, R, T) :-
    R = [[]|T2],
    split(L, S, T2, T).
split([X|L], S, R, T) :-
    X =\= S |
    R = [[X|IT]|T2],
    split2(L, S, IT, T2, T).

split2([], _, IT, R, T) :- IT = [], R = T.
split2([S|L], S, IT, R, T) :-
    IT = [],
    R = [IT2|T2],
    split2(L, S, IT2, T2, T).
split2([X|L], S, IT, R, T) :-
    X =\= S |
    IT = [X|IT2],
    split2(L, S, IT2, R, T).

scan(L, D, V, R) :- scan(L, D, V, [], R).

scan([], _, V, T, R) :- V = T, R = [].
scan([D|R], D, V, T, R2) :- V = T, R2 = R.
scan([C|R], D, V, T, R2) :-
    C =\= D |
    V = [C|V2],
    scan(R, D, V2, T, R2).

iota(N, L, T) :- iota(N, 1, 1, L, T).
iota(N, Start, L, T) :- iota(N, Start, 1, L, T).

iota(N, I, Step, L, T) :-
    N > 0 |
    L = [I|L2],
    I2 is I + Step,
    N2 is N - 1,
    iota(N2, I2, Step, L2, T).
iota(_, _, _, L, T) :- otherwise | L = T.

search(_, [], Prefix, Tail, Rest) :- Prefix = Tail, Rest = [].
search(Str, List, Prefix, Tail, Rest) :-
    otherwise |
    search2(Str, List, Prefix, Tail, Rest, Str, P2, P2).

search2([], List, Prefix, Tail, Rest, _, _, _) :-
    Prefix = Tail,
    Rest = List.
search2([X|S], [X|L], Prefix, Tail, Rest, Str, P2, PT) :-
    PT = [X|PT2],
    search2(S, L, Prefix, Tail, Rest, Str, P2, PT2).
search2(_, [], Prefix, Tail, Rest, _, P2, PT) :-
    Prefix = P2,
    PT = Tail,
    Rest = [].
search2([X|_], [Y|L], Prefix, Tail, Rest, Str, P2, PT) :-
    X =\= Y |
    Prefix = P2,
    PT = [Y|PT2],
    search(Str, L, PT2, Tail, Rest).

search(Str, List, P) :- search3(Str, List, 1, 1, Str, P).

search3([], _, _, F, _, P) :- P = F.
search3([X|S], [X|L], P1, F, Str, P) :-
    P2 is P1 + 1,
    search3(S, L, P2, F, Str, P).
search3(S, [], _, _, _, P) :- list(S) | P = 0.
search3([X|_], [Y|L], P1, _, Str, P) :-
    X =\= Y |
    P2 is P1 + 1,
    search3(Str, L, P2, P2, Str, P).

trim(S, L, R) :-
    trim_left(S, L, R1),
    trim_right(S, R1, R).

trim_left(_, [], R) :- R = [].
trim_left(Set, [C|L], R) :-
    member(C, Set, F),
    trim_left2(F, C, L, Set, R).

trim_left2(true, _, L, Set, R) :- trim_left(Set, L, R).
trim_left2(_, C, L, _, R) :- otherwise | R = [C|L].

trim_right(_, [], R) :- R = [].
trim_right(Set, [C], R) :-
    member(C, Set, F),
    trim_right2(F, C, R).
trim_right(Set, [C|L], R) :-
    otherwise |
    trim_right(Set, L, R1),
    trim_right3(R1, C, Set, R).

trim_right2(true, _, R) :- R = [].
trim_right2(_, C, R) :- otherwise | R = [C].

trim_right3([], C, Set, R) :- trim_right(Set, [C], R).
trim_right3(L, C, _, R) :- otherwise | R = [C|L].

delete(_, [], R) :- R = [].
delete(X, [X|L], R) :- delete(X, L, R).
delete(X, [Y|L], R) :-
    otherwise | R = [Y|R2], delete(X, L, R2).

reverse(List, Reversed) :- reverse(List, Reversed, []).

reverse([], R, Reversed) :- R = Reversed.
reverse([Head|Tail], Reversed, Sofar) :-
    reverse(Tail, Reversed, [Head|Sofar]).

append([], TAIL, L) :- L = TAIL.
append([X|R], TAIL, L) :-
    L = [X|TAIL2],
    append(R, TAIL, TAIL2).

append([], L) :- L = [].
append([L|R], RL) :- append(L, L2, RL), append(R, L2).

member(_, [], R) :- R = false.
member(X, [X|_], R) :- R = true.
member(X, [_|L], R) :- otherwise | member(X, L, R).

last([], L) :- L = [].
last([X], L) :- L = X.
last([_|Y], L) :- list(Y) | last(Y, L).

butlast([_], L) :- L = [].
butlast([], L) :- L = [].
butlast([X, Y|L], R) :-
	R = [X|R2],
	butlast([Y|L], R2).

prefix([], _, F) :- F = true.
prefix([X|L], [X|L2], F) :- prefix(L, L2, F).
prefix(_, _, F) :- otherwise | F = false.

suffix(S, L, F) :- suffix2(S, S, L, L, F).

suffix2([], _, [], _, F) :- F = true.
suffix2([X|L1], S, [X|L2], L, F) :- suffix2(L1, S, L2, L, F).
suffix2([X|_], S, [Y|_], [_|L], F) :- X =\= Y | suffix2(S, S, L, L, F).
suffix2(_, _, [], _, F) :- otherwise | F = false.

join(L, S, R) :- join(L, S, R, []).

join([], _, R, T) :- R = T.
join([[]], _, R, T) :- R = T.
join([[]|L], S, R, T) :-
    list(L) |
    R = [S|R2],
    join(L, S, R2, T).
join([[X|L]|Ls], S, R, T) :-
    otherwise |
    R = [X|R2],
    join([L|Ls], S, R2, T).

characters([], L) :- L = [].
characters(X, L) :- list(X) | L = X.
characters(X, L) :- otherwise | characters(X, L, []).

characters([], L, T) :- L = T.
characters(X, L, T) :- string(X), X =\= [] | string_to_list(X, L, T).
characters(X, L, T) :- number(X) | number_to_list(X, L, T).
characters(X, L, T) :- list(X) | append(X, T, L).

assoc(P, L, X) :- assoc(P, L, X, false).

assoc(_, [], X, D) :- X = D.
assoc(P, [T|L], X, D) :-
    tuple(T) |
    get_arg(1, T, Y),
    (Y == P -> X = T; assoc(P, L, X, D)).
assoc(P, [[P|S]|_], X, _) :- X = [P|S].
assoc(P, [_|L], X, D) :- otherwise | assoc(P, L, X, D).

zip([], _, R) :- R = [].
zip(_, [], R) :- R = [].
zip([X|L1], [Y|L2], R) :-
    R = [{X, Y}|R2],
    zip(L1, L2, R2).

replace(X, New, L, L2) :-
	atomic(X), X =\= [] |
	replace([X], New, L, L2).
replace(Old, X, L, L2) :-
	atomic(X), X =\= [] |
	replace(Old, [X], L, L2).
replace(Old, New, L, L2) :-
	otherwise | replace1(Old, New, L, L2).

replace1(_, _, [], L2) :- L2 = [].
replace1(Old, New, L, L2) :-
	otherwise |
	replace2(Old, New, L, L, Lc, L2, L2, L3),
	replace1(Old, New, Lc, L3).

replace2([], New, _, L, Lc, _, Lr, Lx) :-
	Lc = L,
	append(New, Lx, Lr).
replace2([X|Old], New, L1, [X|L], Lc, Lr1, Lr, Lx) :-
	replace2(Old, New, L1, L, Lc, Lr1, Lr, Lx).
replace2(_, _, [X|L1], _, Lc, Lr1, _, Lx) :-
	otherwise |
	Lr1 = [X|Lx],
	Lc = L1.

index(X, L, I) :- index(X, 1, L, I).

index(_, _, [], I) :- I = false.
index(X, P, [X|_], I) :- I = P.
index(X, P, [_|L], I) :-
	otherwise |
	P2 is P + 1,
	index(X, P2, L, I).

count(X, L, N) :- count(X, L, 0, N).

count(_, [], N1, N) :- N := N1.
count(X, [X|L], N1, N) :-
	N2 is N1 + 1,
	count(X, L, N2, N).
count(X, [_|L], N1, N) :-
	otherwise | count(X, L, N1, N).


%= "vector" module: sparse functional arrays
%
% Vectors are sparse immutable arrays holding arbitrary values. Modification is
% non-destructive and relatively efficient and returns a new vector that
% shares as much information with the old one as possible. Vector indices
% begin at 0. Internally, vectors are implemented using a balanced tree
% (see also the "map" library module).
%
%-
% vector:make(LENGTH?, INIT?, VECTOR^)
%	Creates a new vector of LENGTH elements intitialized to INIT and unifies
%	VECTOR with the newly created vector. Note that all elements share the
%	same initialization value.
%
% vector:element(VECTOR?, INDEX?, VALUE^)
%	Unifies VALUE with the element of VECTOR at the given INDEX. If INDEX
%	is not an integer or exceeds the bounds of the vector, an error is signalled.
%
% vector:elements(VECTOR?, LIST^)
% vector:elements(VECTOR?, LIST^, TAIL?)
%	Unifies LIST with the elements of VECTOR, optionally terminated by TAIL.
%
% vector:set_element(VECTOR?, INDEX?, VALUE?, NEWVECTOR^)
% vector:set_element(VECTOR?, INDEX?, OLD^, VALUE?, NEWVECTOR^)
%	Unifies NEWVECTOR with a new vector holding the elements of VECTOR
%	with VALUE replacing the element at INDEX. If INDEX
%	is not an integer or exceeds the bounds of the vector, an error is signalled.
%	If OLD is given, it is replaced with the original value.
%
% vector:new(VECTOR^, INIT?)
%	Unifies VECTOR with a a new vector. If INIT is an integer, it specifies
%	the size of the vector, with each element initialized to zero.
%	If INIT is a list, the new vector holds the elements of the list in their
%	natural order.
%
% vector:split(VECTOR?, AT?, LOW^, HIGH^)
%	Splits VECTOR at index AT, unifying LOW and HIGH with new vectors
%	containing the elements below AT and above or equal AT.
%
% vector:join(LOW?, HIGH?, VECTOR^)
%	Unifies VECTOR with the a new vector created by concatenating the
%	vectors LOW and HIGH.

-module(vector).

make(0, X, V) :- foreign_call(fl_make_vector(0, [], X, V)).
make(N, X, V) :-
	integer(N), N >= 0 |
	foreign_call(fl_make_vector(N, [], X, V)).
make(N, _, _) :-
	otherwise | error('vector:make: invalid size'(N)).

element(!V, I, X) :-
	integer(I), I >= 0 |
	foreign_call(fl_vector_data(V, M, _, D)),
	map:lookup(I, M, X, D).
element(_, I, _) :-
	otherwise | error('vector:element: invalid vector index'(I)).

set_element(V, I, X, V2) :- set_element(V, I, _, X, V2).

set_element(!V, I, O, X, V2) :-
	integer(I), I >= 0 |
	foreign_call(fl_vector_data(V, M, N, D)),
	N2 is max(N, I + 1),
	% ensure tree is complete so guard doesn't need to force;
	map:lookup(I, M, O, D),
	map:insert(I, X, M, M2) &
	foreign_call(fl_make_vector(N2, M2, D, V2)).
set_element(_, I, _, _, _) :-
	otherwise | error('vector:put: invalid vector index'(I)).

new(V, X) :- integer(X) | make(X, 0, V).
new(V, X) :-
	new_vector2(X, 0, N, [], M) &
	foreign_call(fl_make_vector(N, M, 0, V)).

new_vector2([])-_-_.
new_vector2([X|L])-N-M :-
	map:insert(N, X)-M,
	N += 1,
	new_vector2(L)-N-M.

elements(V, L) :- elements(V, L, []).

elements(!V, L, T) :-
	foreign_call(fl_vector_data(V, M, N, D)),
	vector_elements2(M, 0, N, D, L, T).

vector_elements2(_, N, N, _, L, T) :- L = T.
vector_elements2(M, I, N, D, L, T) :-
	otherwise |
	map:lookup(I, M, X, D),
	L = [X|L2],
	I2 is I + 1,
	vector_elements2(M, I2, N, D, L2, T).

get_info(!V, M, N, D) :- foreign_call(fl_vector_data(V, M, N, D)).

from_info(M, !N, !D, V) :-
	deref(M, Ok),
	when(Ok, foreign_call(fl_make_vector(N, M, D, V))).

split(!V, P, L, U) :-
	foreign_call(fl_vector_data(V, M, N, D)),
	split2(M, P, [], LM, [], UM) &	% ensure maps are complete
	S is N - P,
	split3(P, S, D, LM, UM, L, U).

split2([], _)-_-_.
split2(t(_, K, V, L, R), P)-LM-UM :-
	K < P |
	map:insert(K, V)-LM,
	split2(L, P)-LM-UM,
	split2(R, P)-LM-UM.
split2(t(_, K, V, L, R), P)-LM-UM :-
	otherwise |
	map:insert(K, V)-UM,
	split2(L, P)-LM-UM,
	split2(R, P)-LM-UM.

split3(S1, S2, D, LM, UM, L, U) :-
	foreign_call(fl_make_vector(S1, LM, D, L)),
	foreign_call(fl_make_vector(S2, UM, D, U)).

join(!L, !H, V) :-
	foreign_call(fl_vector_data(L, LM, LN, _)),
	foreign_call(fl_vector_data(H, HM, HN, D)),
	S is LN + HN,
	join2(LM, 0, [], M1),
	join2(HM, LN, M1, M2) &	% so vector is complete
	join2(S, M2, D, V).

join2(!S, M, D, V) :-
	foreign_call(fl_make_vector(S, M, D, V)).

join2([], _)-_.
join2(t(_, K, V, L, R), N)-M :-
	K2 is K + N,
	map:insert(K2, V)-M,
	join2(L, N)-M,
	join2(R, N)-M.

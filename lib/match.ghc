%= "match" module: Pattern matching
%
% This module allows matching sequences with patterns. Patterns may be
% strings or lists and are matched according to the following rules:
% Each element of the pattern is matched with zero or more elements of
% the subject list
%
%   0'*     matches zero or more elements
%   0'?     matches any element
%   0'[, ..., 0']       matches any element between the brackets
%   0'[, 0'^, ..., 0']  matches an element that different from the sequence
%                       given between the brackets
%       Bracket-sequences may contain ranges of the form
%       <element1>, 0'-, <element2>
%
%   [X, ...]  matches any X (like brackets above)
%   {X}     matches the arbitrary value X
%   call(X) matches if "apply(X, [SUBJECT?, R^])" assigns anything but the
%             string "false" to R.
%
% Note that patterns and subjects need not necessarily be character
% lists.
%
% "all" matches a full subject sequence, "begins" only a prefix. "find"
% searches a sequence for a match, and "extract" will produce the
% matched part after searching a sequence.
%
%-
% match:all(PATTERN?, SUBJECT?, RESULT^)
%   Unifies RESULT with "true" or "false", depending on whether PATTERN
%   matches the complete sequence SUBJECT.
%
% match:begins(PATTERN?, SUBJECT?, REST^)
%   Unifies REST with the portion of SUBJECT that follows the part matching
%   PATTERN, or "false", if SUBJECT does not begin with a matching sequence.
%
% match:find(PATTERN?, SUBJECT?, REST^)
% match:find(PATTERN?, SUBJECT?, PREFIX^, REST^)
% match:find(PATTERN?, SUBJECT?, PREFIX^, PREFIXTAIL?, REST^)
%   Searches SUBJECT for a sequence matching PATTERN and unifies PREFIX
%   with the part before the match (terminated by PREFIXTAIL, which defaults to
%   the empty list) and REST with the remaining part. If no match can be
%   found, REST is unified with the string "false".
%
% match:extract(PATTERN?, SUBJECT?, MATCH^)
% match:extract(PATTERN?, SUBJECT?, MATCH^, TAIL?)
% match:extract(PATTERN?, SUBJECT?, MATCH^, TAIL?, REST^)
% match:extract(PATTERN?, SUBJECT?, PREFIX^, PREFIXTAIL?, MATCH^, TAIL?, REST^)
%   Like "find", but unifies MATCH with the found sequence, optionally
%   terminated by TAIL (which defaults to the empty list). PREFIX and
%   PREFIXTAIL designate the part before the match (if found).
%
% match:fields(PATTERN?, SUBJECT?, LIST^)
% match:fields(PATTERN?, SUBJECT?, LIST^, TAIL?)
%    Unifies LIST with a list of sub-lists in SUBJECT separated by PATTERN,
%    optionally terminated by TAIL.

-module(match).

all(Pat, Sub, R) :-
    list:characters(Pat, P),
    list:characters(Sub, S),
    match1(P, S, M, [], R1),
    (M == false -> R = false;
      (R1 == [] -> R = true; R = false)).

begins(Pat, Sub, Rest) :-
    list:characters(Pat, P),
    list:characters(Sub, S),
    match1(P, S, M, [], Rest1),
    (M == false -> Rest = false; Rest = Rest1).

find(Pat, Sub, Rest) :-
    find(Pat, Sub, _, [], Rest).

find(Pat, Sub, Head, Rest) :-
    find(Pat, Sub, Head, [], Rest).

find(Pat, Sub, Head, Tail, Rest) :-
    list:characters(Pat, P),
    list:characters(Sub, S),
    find1(P, S, Head2, Tail, M, [], Rest1),
    (M == false -> Rest = false; Head = Head2, Rest = Rest1).

extract(Pat, Sub, Match) :-
    extract(Pat, Sub, _, [], Match, [], _).
extract(Pat, Sub, Match, Tail) :-
    extract(Pat, Sub, _, [], Match, Tail, _).
extract(Pat, Sub, Match, Tail, Rest) :-
    extract(Pat, Sub, _, [], Match, Tail, Rest).
extract(Pat, Sub, Prefix, PTail, Match, Tail, Rest) :-
    list:characters(Pat, P),
    list:characters(Sub, S),
    find1(P, S, Prefix, PTail, Match, Tail, Rest).

fields(Pat, Sub, R) :- fields(Pat, Sub, R, []).

fields(Pat, Sub, R, T) :-
    list:characters(Pat, P),
    list:characters(Sub, S),
    fields2(P, S, R, T).

fields2(_, [], R, T) :- R = T.
fields2(P, [X|S], R, T) :-
    match1(P, [X|S], M, [], Rest),
    (M == false ->
    	R = [[X|X2]|R2],
    	fields3(P, S, X2, R2, T)
   ;
    	fields2(P, Rest, R, T)
    ).

fields3(_, [], X, R, T) :- X = [], R = T.
fields3(P, [Y|S], X, R, T) :-
    match1(P, [Y|S], M, [], Rest),
    (M =\= false ->
        X = [],
        fields2(P, Rest, R, T)
    ;
        X = [Y|X2],
        fields3(P, S, X2, R, T)
    ).

find1(_, [], _, _, M, _, _) :- M = false.
find1(P, [X|S], H, T, M, MT, R) :-
    match1(P, [X|S], M1, MT, R1),
    (M1 == false  ->
        H = [X|H2],
        find1(P, S, H2, T, M, MT, R)
    ;
        H = T,
        M = M1,
        R = R1
    ).

match1([], S, M, T, R) :- R = S, M = T.
match1([0'[, C|P], S, M, T, R) :-
    C =\= 0'^ |
    scan_set([C|P], P, Set, P2),
    match1_set(Set, P2, S, M, T, R).
match1([0'[, 0'^|P], S, M, T, R) :-
    scan_set(P, P, Set, P2),
    match1_nset(Set, P2, S, M, T, R).
match1([0'*], S, M, T, R) :-
    list:append(S, T, M),
    R = [].
match1([0'*|P], S, M, T, R) :-
    P =\= [] | match1_any(P, S, M, T, R).
match1([0'?|P], [_|S], M, T, R) :- match1(P, S, M, T, R).
match1([X|P], S, M, T, R) :-
    otherwise |
    match2(X, P, S, M, T, R).

match2([], P, S, M, T, R) :- match1(P, S, M, T, R).
match2(call(X), P, [Y|S], M, T, R) :-
    app:papply(X, [Y, F]),
    (F == false ->
        M = false
    ;
        match1(P, S, M, T, R)
    ).
match2({X}, P, [X|S], M, T, R) :- match1(P, S, M, T, R).
match2(X, P, S, M, T, R) :-
    list(X) |
    to_set(X, Set),
    match1_set(Set, P, S, M, T, R).
match2(X, P, S, M, T, R) :-
    otherwise | match3(X, P, S, M, T, R).

match3(X, P, [X|S], M, T, R) :- match1(P, S, M, T, R).
match3(_, _, _, M, _, _) :- otherwise | M = false.

to_set(Lst, Set) :- app:maplist(wrap, Lst, Set).

wrap(X, R) :- R = {X}.

scan_set([], P, _, _) :- error('match: invalid pattern'(P)).
scan_set([0']|P], _, Set, P2) :- Set = [], P2 = P.
scan_set([From, 0'-, To|P], P0, Set, P2) :-
    Set = [{From, To}|Set2],
    scan_set(P, P0, Set2, P2).
scan_set([X|P], P0, Set, P2) :-
    otherwise |
    Set = [{X}|Set2],
    scan_set(P, P0, Set2, P2).

match1_set([], _, _, M, _, _) :- M = false.
match1_set([{X}|_], P, [X|Sub], M, T, R) :-
    match1(P, Sub, M, T, R).
match1_set([{From, To}|_], P, [X|Sub], M, T, R) :-
    X @>= From, X @=< To | match1(P, Sub, M, T, R).
match1_set([_|Set], P, Sub, M, T, R) :-
    otherwise | match1_set(Set, P, Sub, M, T, R).

match1_nset([], _, [], M, _, _) :- M = false.
match1_nset([], P, [_|S], M, T, R) :- match1(P, S, M, T, R).
match1_nset([{X}|_], _, [X|_], M, _, _) :- M = false.
match1_nset([{From, To}|_], _, [X|_], M, _, _) :-
    X @>= From, X @=< To | M = false.
match1_nset([_|Set], P, Sub, M, T, R) :-
    otherwise | match1_nset(Set, P, Sub, M, T, R).

match1_any(P, S, M, T, R) :-
    match1(P, S, M1, T, R1),
    (M1 == false  ->
        match1_next(P, S, M, T, R)
    ;
        M = M1,
        R = R1
    ).

match1_next(_, [], M, _, _) :- M = false.
match1_next(P, [_|S], M, T, R) :- match1_any(P, S, M, T, R).

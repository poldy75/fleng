%= JSON parsing
%
% A parser and printer for JSON objects. JSON data is mapped to FLENG terms
% in the following manner:
%
%            null, true, false   -> string
%            object              -> '{}'([<charlist>:<value>, ...])
%                        [PCN] {"{}", [{":", <charlist>, <value>}, ...]}
%            array               -> array([...])  [PCN] {"array", [...]}
%            string              -> character code list
%            number              -> number
%
% Use "json:read" to read JSON expressions and "json:to_string" and "json:print" to
% convert parsed JSON expressions to character lists or write them to a file,
% respectively. "json:query" provides a simple querying mechanism for JSON
% expressions.
%-
% json:read(IN?, OUT^)
% json:read(X^, IN?, OUT^)
%    Unifies X with a JSON term scanned from the character stream IN, with
%    the remaining input in OUT or reads successive terms from IN and writes
%    them to the stream OUT.
%    Errors will insert "error(MSG)" tuples into the output. The parser
%    attempts to be permissive, especially with the placement of ","
%    characters.
%
% json:print(TERM?)
% json:print(FILE?, TERM?)
% json:print(FILE?, TERM?, DONE^)
%    Writes TERM in JSON representation to FILE (or standard output),
%    optionally assigning the empty list to DONE when the operation is
%    complete.
%
% json:to_string(TERM?, LIST^)
% json:to_string(TERM?, LIST^, TAIL?)
%    Unifies LIST with a string containing the JSON representation
%    of TERM, optionally with tail TAIL.
%	For convenience, this operation detects numeric arrays and
%	converts them into JSON arrays. Strings that are not one of "true",
%	"false" and "null" are written as quoted strings. "error" tuples
%	creating during parsing will trigger an error when an attempt is
%	made to convert it to a string.
%
% json:query(QUERY?, OBJECT?, RESULT^)
% json:query(QUERY?, OBJECT?, RESULT^, TAIL?)
%    Extracts elements from the parsed JSON object OBJECT and unifies
%    RESULT with the list of results, optionally terminated by TAIL.
%_
%        Queries may be any of the following:
%_
%           index(I)        returns a list of the Ith element of the
%                           object, which must be an array. Indexing
%                           starts at 1.
%           range(I, J)     returns a list of the Ith to Jth elements
%                           (inclusive) of the object, which must be
%                           an array. Indexing starts at 1.
%           key(K)          returns a list of the value of element
%                           "K:<value>" of the object, which must
%                           be a tuple of K:V elements. If no such
%                           key exists, returns the empty list.
%           id              returns a list containing the object.
%           (QUERY1, QUERY2) returns the results of both queries,
%                            concatenated together.
%                            [PCN] {",", QUERY1, QUERY2}
%           QUERY1+QUERY2   returns the second query applied to all
%                           results of the first query.
%                           [PCN] {"+", QUERY1, QUERY2}
%           null
%           true
%           false           return a list holding the object, if
%                           it is equal to the given string or
%                           returns the empty list.
%           <string>        any other string is queried as a list of its characters
%           <charlist>      return list containing <charlist> if
%                           equal or return the empty list.

-module(json).

read([], Out) :- Out = [].
read(In, Out) :-
    otherwise |
    scan:whitespace(In, In2),
    read2(In2, Out).

read2([], Out) :- Out = [].
read2(In, Out) :-
    otherwise |
    read(X, In, In2),
    Out = [X|Out2],
    read(In2, Out2).

read(X)-S :-
    scan:whitespace-S,
    read1(X)-S.

read1(X, [], S) :- X = eof, S = [].
read1(X, [0'{|S1], S) :- read_o(X, S1, S).
read1(X, [0'[|S1], S) :- read_a(X, S1, S).
read1(X, [0'"|S1], S) :- read_s(X, S1, S).
read1(X, [0',|S1], S) :- read1(X, S1, S).
read1(X, [0'-|S1], S) :-
    scan:number(X1, [0'-|S1], S),
    list_to_number(X1, X).
read1(X, [C|S1], S) :-
    C >= 0'0, C =< 0'9 |
    scan:number(X1, [C|S1], S),
    list_to_number(X1, X).
read1(X, [C|S1], S) :-
    C >= 0'a, C =< 0'z |
    scan:identifier(X1, [C|S1], S),
    list_to_string(X1, X2),
    check_ident(X2, X).
read1(X, [C|S1], S) :-
    otherwise |
    X = error('unexpected input'(C)),
    S = S1.

check_ident(true, X) :- X = true.
check_ident(false, X) :- X = false.
check_ident(null, X) :- X = null.
check_ident(ID, X) :-
	otherwise | X = error('invalid identifier'(ID)).

read_s(X)-S :- scan:delimited_with_escape(0'", X)-S.

read_o(X)-S :-
    scan:whitespace-S,
    read_o1(L)-S,
    X = '{}'(L).

read_o1(L, [0'}|S1], S) :- L = [], S = S1.
read_o1(L, [0'"|S1], S) :-
    read_s(K, S1, S2),
    scan:whitespace(S2, S3),
    expect(0':, F, S3, S4),
    read_o2(F, L, K, S4, S).
read_o1(L)-S :-
    otherwise |
    read(X)-S,
    scan:whitespace-S,
    expect(0':, F)-S,
    read_o2(F, L, X)-S.

read_o2(true, L, K)-S :-
    read(V)-S,
    scan:whitespace-S,
    L = [K:V|L2],
    read_o3(L2)-S.
read_o2(false, L, K)-_ :-
    L = [K:error('missing colon')].

read_o3(L, [0',|S1], S) :- read_o1(L, S1, S).
read_o3(L, [0'}|S1], S) :- L = [], S = S1.
read_o3(L, S1, S) :-
    otherwise |
    L = [error('expected comma or closing brace')],
    S = S1.

read_a(X)-S :-
    scan:whitespace-S,
    read_a1(X1)-S,
    X = array(X1).

read_a1(X, [0',|S1], S) :- read_a1(X, S1, S).
read_a1(X, [0']|S1], S) :- X = [], S = S1.
read_a1(X)-S :-
    otherwise |
    read(V)-S,
    X = [V|X2],
    scan:whitespace-S,
    read_a2(X2)-S.

read_a2(L, [0',|S1], S) :- read_a1(L, S1, S).
read_a2(L, [0']|S1], S) :- L = [], S = S1.
read_a2(L, S1, S) :-
    otherwise |
    L = [error('expected comma or closing bracket')],
    S = S1.

expect(C, F, [C|S1], S) :- S = S1, F = true.
expect(_, F)-_ :- otherwise | F = false.

% printer

print(X) :- print(1, X, _).

print(F, X) :- print(F, X, _).

print(F, X, Ok) :-
	to_string(X, Str, []),
	write_file(F, Str, Ok).

to_string(X, Str) :- to_string(X, Str, []).

to_string(X, Str, T) :- to_string2(X, Str, T, 0).

to_string2(S, Str, T, _) :-
	string(S) | to_string_str(S, Str, T).
to_string2('{}'([]), Str, T, _) :- Str = [0'{, 0'}|T].
to_string2('{}'([X]), Str, T, I) :-
	I2 is I + 2,
	Str = [0'{|Str2],
	to_string_element(X, Str2, T2, I2),
	T2 = [0'}|T].
to_string2('{}'([X|L]), Str, T, I) :-
	list(L) |
	I2 is I + 2,
	Str = [0'{|Str2],
	to_string3([X|L], Str2, T2, I2),
	T2 = [0'}|T].
to_string2(array([]), Str, T, I) :- to_string2([], Str, T, I).
to_string2(array([X]), Str, T, I) :-
	I2 is I + 2,
	Str = [0'[|Str2],
	to_string_element(X, Str2, T2, I2),
	T2 = [0']|T].
to_string2(array([X|L]), Str, T, I) :-
	list(L) |
	I2 is I + 2,
	Str = [0'[|Str2],
	to_string3([X|L], Str2, T2, I2),
	T2 = [0']|T].
to_string2(error(Msg), _, _, _) :-
	error('json: can not convert invalid term'(Msg)).
to_string2(A, Str, T, I) :-
	array(A) |
	length(A, N),
	array:get(0, N, A, Lst),
	to_string2(array([Lst]), Str, T, I).
to_string2(N, Str, T, _) :-
	number(N) | number_to_list(N, Str, T).
to_string2(X, Str, T, _) :-
	otherwise |
	quote_and_escape(X, Str, T).

to_string3([], Str, T, I) :-
	I2 is I - 2,
	list:make(I2, 32, Str2, T),
	Str = [10|Str2].
to_string3([X], Str, T, I) :-
	list:make(I, 32, Str2, T2),
	Str = [10|Str2],
	to_string_element(X, T2, [10|T3], I),
	I2 is I - 2,
	list:make(I2, 32, T3, T).
to_string3([X|L], Str, T, I) :-
	otherwise |
	list:make(I, 32, Str2, T2),
	Str = [10|Str2],
	to_string_element(X, T2, [0',|T3], I),
	to_string3(L, T3, T, I).

to_string_element(X:Y, Str, T, I) :-
	to_string2(X, Str, [32, 0':, 32|T2], I),
	to_string2(Y, T2, T, I).
to_string_element(X, Str, T, I) :-
	otherwise | to_string2(X, Str, T, I).

to_string_str(true, S, T) :- S = [0't, 0'r, 0'u, 0'e|T].
to_string_str(false, S, T) :- S = [0'f, 0'a, 0'l, 0's, 0'e|T].
to_string_str(null, S, T) :- S = [0'n, 0'u, 0'l, 0'l|T].
to_string_str(Str, S, T) :-
	otherwise |
	string_to_list(Str, S2),
	quote_and_escape(S2, S, T).

quote_and_escape(S, Str, T) :-
	Str = [0'\"|R],
	quote_and_escape2(S, R, T).

quote_and_escape2([], R, T) :- R = [0'\"|T].
quote_and_escape2([C|S], R, T) :-
	backslash(C, C2),
	(C2 == false ->
		R = [C|R2]
	;
		R = [0'\\, C2|R2]
	),
	quote_and_escape2(S, R2, T).

-mode(backslash(?, ^)).
backslash(9, 0't).
backslash(10, 0'n).
backslash(13, 0'r).
backslash(0'\", 0'\").
backslash(0'\\, 0'\\).
backslash(8, 0'b).
backslash(12, 0'f).
backslash(_, false) :- otherwise | true.

% querying

query(Q, X, R) :- query(Q, X, R, []).

query(id, X, R, T) :- R = [X|T].
query(null, null, R, _) :- R = [null].
query(true, true, R, _) :- R = [true].
query(false, false, R, _) :- R = [false].
query(S, X, R, T) :-
    string(S), S =\= true, S=\= false, S =\= null |
    query_str(S, X, R, T).
query(key(K), '{}'(L), R, T) :-
    query_key(K, L, R, T).
query(index(I), array(L), R, T) :-
    N is I - 1,
    list:slice(N, 1, L, R, T).
query(range(I, J), array(L), R, T) :-
    N is J - I + 1,
    list:slice(I, N, L, R, T, R).
query((A, B), X, R, T) :-
    query(A, X, R, T1),
    query(B, X, T1, T).
query(A+B, X, R, T) :-
    query(A, X, Y, []),
    query_over(B, Y, R, T).
query(Q, Q, R, T) :-
    R = [Q|T].
query(_, _, R, T) :-
    otherwise | R = T.

query_str(S, S, R, T) :- R = [S|T].
query_str(S, X, R, T) :-
    otherwise |
    string_to_list(S, L),
    (L == X -> R = [X|T]; R = T).

query_key(K, L, R, T) :-
    string(K) |
    string_to_list(K, S, []),
    query_key(S, L, R, T).
query_key(_, [], R, T) :- R = T.
query_key(K, [K:X|_], R, T) :-
    list(K) | R = [X|T].
query_key(K, [_|L], R, T) :-
    otherwise | query_key(K, L, R, T).

query_over(_, [], R, T) :- R = T.
query_over(Q, [X|L], R, T) :-
    query(Q, X, R, T1),
    query_over(Q, L, T1, T).

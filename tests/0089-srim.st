% send/receive with incomplete messages
%
% from: "Parallel Logic Programming Techniques" (S. Taylor)

-initialization(main).
-machine(ring).

main :-
    sender(200, S), 
    receiver(S, 0, R)@fwd,
    chk(R).

chk(200).

sender(N, S) :-
    N > 0 |
    N1 is N - 1,
    sender(N1, S2),
    get_reply(R),
    S := [msg(R)|S2].
sender(0, S) :- S := [].

receiver([msg(R)|S], N, Ok) :-
    N1 is N + 1,
    receiver(S, N1, Ok),
    R := ok.
receiver([], N, Ok) :- 
    done(N, Ok)@bwd.

get_reply(ok).

done(N, O) :- data(N) | O := N.

% FLENG compiler - FFI wrapper generation

-module(ffi).

generate_stubs([], _, FW1, FW, SN1, SN, T1, T) :-
    SN = SN1, FW = FW1, T = T1.
generate_stubs([Spec|Specs], EP, FW1, FW, SN1, SN, Code, Tail) :-
    string(Spec) |
    verbatim(Spec, FW1, FW2),
    generate_stubs(Specs, EP, FW2, FW, SN1, SN, Code, Tail).
generate_stubs([verbatim(Text)|Specs], EP, FW1, FW, SN1, SN, Code, Tail) :-
    verbatim(Text, FW1, FW2),
    generate_stubs(Specs, EP, FW2, FW, SN1, SN, Code, Tail).
generate_stubs([struct(Struct)|Specs], EP, FW1, FW, SN1, SN, Code, Tail) :-
    generate_struct(Struct, EP, FW1, FW2, SN1, SN2, Code, Tail1),
    generate_stubs(Specs, EP, FW2, FW, SN2, SN, Tail1, Tail).
generate_stubs([const(Const)|Specs], EP, FW1, FW, SN1, SN, Code, Tail) :-
    generate_const(Const, EP, FW1, FW2, SN1, SN2, Code, Tail1),
    generate_stubs(Specs, EP, FW2, FW, SN2, SN, Tail1, Tail).
generate_stubs([Name = Spec|Specs], EP, FW1, FW, SN1, SN, Code, Tail) :-
    string(Name) |
    generate_stub(Name, Spec, FW1, FW2, SN1, SN2, Code, Tail1),
    generate_stubs(Specs, EP, FW2, FW, SN2, SN, Tail1, Tail).
generate_stubs([Spec|Specs], EP, FW1, FW, SN1, SN, Code, Tail) :-
    tuple(Spec) |
    term:functor_parts(Spec, Name, _, _),
    generate_stub(Name, Spec, FW1, FW2, SN1, SN2, Code, Tail1),
    generate_stubs(Specs, EP, FW2, FW, SN2, SN, Tail1, Tail).
generate_stubs([Spec|_], EP, _, _, _, _, _, _) :-
    otherwise |
    send(EP, error('invalid foreign specification: ~q', [Spec])).


% utiltities

verbatim(Spec, FW1, FW) :-
    map:replace('$verbatim', [Spec|Old], Old, FW1, FW).


% Stub function parsing and generation

generate_stub(Name, Spec, FW1, FW, SN1, SN, Code, Tail) :-
    term:functor_parts(Spec, Fname, _, Args),
    add_confirmation_arg(Args, Args2),
    map:insert(Fname, Args2, FW1, FW),
    parse_foreign_args(Args2, 0, Vars, CallArgs, Params, Derefs, no),
    foreign_stub_name(Fname, Stub),
    gen_foreign_stub(Name, Vars, CallArgs, Params, Derefs, Stub, SN1, SN,
        Code, Tail).

add_confirmation_arg([], Args) :- Args = [-void].
add_confirmation_arg([-T|L], Args) :- Args = [-T|L].
add_confirmation_arg([X|L], Args) :-
	otherwise |
	Args = [X|Args2],
	add_confirmation_arg(L, Args2).

foreign_stub_name(Name, SName) :-
    u:format_to_atom('fl_f_~a', [Name], SName).

parse_foreign_args([], V, Vars, CallArgs, Params, Derefs, no) :-
    Vars = [V],
    CallArgs = ['$VAR'(0, V, 0)],
    Params = ['$VAR'(0, V, 0)],
    Derefs = [].
parse_foreign_args([], _, Vars, CallArgs, Params, Derefs, yes) :-
    Vars = [], CallArgs = [], Params = [], Derefs = [].
parse_foreign_args([-void], V, Vars, CallArgs, Params, Derefs, no) :-
    Vars = [V],
    Var = '$VAR'(0, V, 0),
    CallArgs = [Var],
    Params = [Var],
    Derefs = [].
parse_foreign_args([-void], _, Vars, CallArgs, Params, Derefs, yes) :-
    Vars = [], CallArgs = [], Params = [], Derefs = [].
parse_foreign_args([-T], V, Vars, CallArgs, Params, Derefs, no) :-
    T =\= void |
    Vars = [V],
    Var = '$VAR'(0, V, 0),
    CallArgs = [Var],
    Params = [Var],
    Derefs = [].
parse_foreign_args([-T|Args], V, Vars, CallArgs, Params, Derefs, _) :-
    T =\= void, list(Args) |
    Vars = [V|Vars2],
    Var = '$VAR'(0, V, 0),
    CallArgs = [Var|CallArgs2],
    Params = [Var|Params2],
    V2 is V + 1,
    parse_foreign_args(Args, V2, Vars2, CallArgs2, Params2, Derefs, yes).
parse_foreign_args([T|Args], V, Vars, CallArgs, Params, Derefs, R) :-
    otherwise |
    classify_type(T, FF, DF),
    Vars = [V|Vars2],
    CallArgs = [Var|CallArgs2],
    (FF == true -> Params = ['!'(Var)|Params2]; Params = [Var|Params2]),
    (DF == true -> Derefs = [Var|Derefs2]; Derefs = Derefs2),
    Var = '$VAR'(0, V, 0),
    V2 is V + 1,
    parse_foreign_args(Args, V2, Vars2, CallArgs2, Params2, Derefs2, R).

gen_foreign_stub(Name, Vars, CallArgs, Params, Derefs, Stub, SN1, SN, Code, Tail) :-
    Derefs =\= [] |
    length(Vars, CV),
    fmt:format_chars('$d$~a', [Name], Name2),
    list_to_string(Name2, Name2s),
    list_to_tuple([Name2s, '$VAR'(0, CV, 0)|CallArgs], Call2),
    list_to_tuple([Name2s, '!'('$VAR'(0, CV, 0))|Params], Head2),
    canonical_call_args(CallArgs, CA2),
    list_to_tuple([Stub|CA2], Call),
    list_to_tuple([Name|Params], Head),
    length(CallArgs, N),
    N2 is N + 1,
    SN = [{Name2/N2, Name/N}|SN1],
    Code = [t((Head :- call(lib:deref(Derefs, '$VAR'(0, CV, 0))), Call2),
                [], []),
            t((Head2 :- foreign_call(Call)), [], [])|Tail].
gen_foreign_stub(Name, _, CallArgs, Params, [], Stub, SN1, SN, Code, Tail) :-
    otherwise |
    canonical_call_args(CallArgs, CA2),
    list_to_tuple([Stub|CA2], Call),
    list_to_tuple([Name|Params], Head),
    SN = SN1,
    Code = [t((Head :- foreign_call(Call)), [], [])|Tail].

-mode(classify_type(?, ^, ^)).
classify_type(string, true, true).
classify_type(list, true, true).
classify_type(tuple, true, true).
classify_type(integer, true, false).
classify_type(char, true, false).
classify_type(long, true, false).
classify_type(short, true, false).
classify_type(pointer, true, false).
classify_type(pointer(_), true, false).
classify_type(_, false, false) :- otherwise | true.

canonical_call_args(Args, CA) :- canonical_call_args(Args, 0, CA).

canonical_call_args([], _, CA) :- CA = [].
canonical_call_args([X], 3, CA) :- CA = [X].
canonical_call_args([X|Args], 3, CA) :-
    list(Args) | CA = [[X|Args]].
canonical_call_args([X|Args], N, CA) :-
    otherwise |
    CA = [X|CA2],
    N2 is N + 1,
    canonical_call_args(Args, N2, CA2).


% Struct wrapper parsing

generate_struct(Name = Spec, EP, FW1, FW, SN1, SN, Code, Tail) :-
    string(Name) |
    generate_struct2(Name, Spec, EP, FW1, FW, SN1, SN, Code, Tail).
generate_struct(Spec, EP, FW1, FW, SN1, SN, Code, Tail) :-
    tuple(Spec) |
    term:functor_parts(Spec, Name, _, _),
    generate_struct2(Name, Spec, EP, FW1, FW, SN1, SN, Code, Tail).
generate_struct(Spec, EP, _, _, _, _, _, _) :-
    otherwise |
    send(EP, error('invalid foreign struct specification: ~w', [Spec])).

generate_struct2(Name, Spec, EP, FW1, FW, SN1, SN, C, T) :-
    term:functor_parts(Spec, Sname, _, Args),
    generate_struct_accessors(Args, Name, Sname, EP, FW1, FW, SN1, SN, C, T).

generate_struct_accessors([], _, _, _, FW1, FW, SN1, SN, C1, C) :-
    FW = FW1, C = C1, SN = SN1.
generate_struct_accessors([-Slot:Type|Args], Name, Sname, EP, FW1, FW, SN1,
    SN, C, T) :-
    string(Slot) |
    generate_struct_getter(Name, Sname, Slot, Type, FW1, FW2, SN1, SN2,
        C, T1),
    generate_struct_accessors(Args, Name, Sname, EP, FW2, FW, SN2, SN, T1, T).
generate_struct_accessors([Slot:Type|Args], Name, Sname, EP, FW1, FW, SN1, SN,
    C, T) :-
    string(Slot) |
    generate_struct_getter(Name, Sname, Slot, Type, FW1, FW2, SN1, SN2,
        C, T1),
    generate_struct_setter(Name, Sname, Slot, Type, FW2, FW3, SN2, SN3,
        T1, T2),
    generate_struct_accessors(Args, Name, Sname, EP, FW3, FW, SN3, SN, T2, T).
generate_struct_accessors([Sspec|_], _, Sname, EP, _, _, _, _, _, _) :-
    otherwise |
    send(EP, error('invalid slot specification for foreign struct ~a: ~w',
        [Sname, Sspec])).

generate_struct_getter(Name, Sname, Slot, Type, FW1, FW, SN1, SN, C,
    Tail) :-
    u:format_to_atom('s_g_~a_~a', [Name, Slot], GetN),
    u:format_to_atom('#define ~a(_x) (_x->~a)', [GetN, Slot], Get),
    verbatim(Get, FW1, FW2),
    u:format_to_atom('~a_~a', [Name, Slot], Getter),
    list_to_tuple([GetN, pointer(Sname), -Type], Spec),
    generate_stub(Getter, Spec, FW2, FW, SN1, SN, C, Tail).

generate_struct_setter(Name, Sname, Slot, Type, FW1, FW, SN1, SN, C,
    Tail) :-
    u:format_to_atom('s_s_~a_~a', [Name, Slot], SetN),
    u:format_to_atom('#define ~a(_x, _y) (_x->~a = _y)', [SetN, Slot], Set),
    verbatim(Set, FW1, FW2),
    u:format_to_atom('~a_~a', [Name, Slot], Setter),
    list_to_tuple([SetN, pointer(Sname), Type], Spec),
    generate_stub(Setter, Spec, FW2, FW, SN1, SN, C, Tail).


% Constant stubs

generate_const(Name = Const:Type, _, FW1, FW, SN1, SN, C, T) :-
    generate_const2(Name, Const, Type, FW1, FW, SN1, SN, C, T).
generate_const(Name:Type, _, FW1, FW, SN1, SN, C, T) :-
    generate_const2(Name, Name, Type, FW1, FW, SN1, SN, C, T).
generate_const(X, _, _, EP, _, _, _, _, _, _) :-
    send(EP, error('invalid foreign const specification: ~w', [X])).

generate_const2(Name, Const, Type, FW1, FW, SN1, SN, C, Tail) :-
    u:format_to_atom('c_g_~a', [Name], GetC),
    native_type(Type, NT),
    u:format_to_atom('static ~a ~a() {return ~a;}', [NT, GetC, Const], Get),
    verbatim(Get, FW1, FW2),
    list_to_tuple([GetC, -Type], Spec),
    generate_stub(Name, Spec, FW2, FW, SN1, SN, C, Tail).


% C wrapper code generation

generate_wrappers([]).
generate_wrappers(FW) :-
    otherwise |
    global(foreign_language, FL),
    global(foreign_name, FN),
    fmt:format_chars('~s.~s', [FN, FL], FWN),
    generate_wrappers2(FWN, FW).

generate_wrappers2(Fname, FW) :-
    open_file(Fname, write, Out),
    fmt:format(Out, '/* GENERATED BY FLENG */\n', [], Ok),
    get_global(pcn_generated, F, false),
    (F == false ->
    	when(Ok, fmt:format(Out, '#include "fleng-util.h"\n', [], Ok2))
    ;
    	Ok2 = Ok
    ),
    map:map_to_list(FW, FL),
    write_wrappers(Ok2, FL, Out).

write_wrappers([], [], Out) :-
    fmt:format(Out, '/* END OF FILE */\n', [], _).
write_wrappers([], [Name-Args|FL], Out) :-
    write_wrapper(Name, Args, Out, Ok),
    write_wrappers(Ok, FL, Out).

write_wrapper('$verbatim', Text, Out, Ok) :-
    list:reverse(Text, Rev),
    write_verbatim([], Rev, Out, Ok).
write_wrapper(Name, Args, Out, Ok) :-
    otherwise |
    wrapper_arity(Args, A1, Confirm),
    A is min(A1, 4),
    extern_c(ExtC),
    fmt:format(Out, '~svoid fl_f_~a_~d(FL_TCB *tcb', [ExtC, Name, A], Ok1),
    write_wrapper_params(Ok1, Args, 0, no, Out, Ok2),
    when(Ok2, fmt:format(Out, ') {\n', [], Ok3)),
    write_wrapper_init(Ok3, Args, Out, Ok4),
    write_wrapper_conv(Ok4, Args, 0, no, A1, Out, Ok5),
    write_wrapper_call(Ok5, Name, Args, Out, Ok6),
    write_wrapper_confirm(Ok6, Confirm, A1, Out, Ok7),
    when(Ok7, fmt:format(Out, '}\n', [], Ok)).

write_verbatim([], [], _, Ok) :- Ok = [].
write_verbatim([], [Ln|L], Out, Ok) :-
    fmt:format(Out, '~s\n', [Ln], Ok1),
    write_verbatim(Ok1, L, Out, Ok).

write_wrapper_init([], Args, Out, Ok) :-
    set:intersection([charlist, string], Args, U),
    (U =\= []  ->
        fmt:format(Out, '  int sl;\n  char *sp = init_cbuf(tcb);\n', [], Ok)
    ;
        Ok = []
    ).

wrapper_arity(Args, A, C) :- wrapper_arity(Args, no, 0, A, C).

%XXX probably too complicated, since we add a confirmation arg in any case:
-mode(wrapper_arity(?, ?, ?, ^, ^)).
wrapper_arity([], yes, A, A, no).
wrapper_arity([], no, A1, A, yes) :- A is A1 + 1.
wrapper_arity([-void], no, A1, A, yes) :- A is A1 + 1.
wrapper_arity([-void], yes, A, A, no).
wrapper_arity([-_|Args], _, A1, A, C) :-
    A2 is A1 + 1,
    wrapper_arity(Args, yes, A2, A, C).
wrapper_arity([_|Args], R, A1, A, C) :-
    otherwise |
    A2 is A1 + 1,
    wrapper_arity(Args, R, A2, A, C).

write_wrapper_confirm([], yes, A, Out, Ok) :-
    N is A - 1,
    fmt:format(Out, '  fl_assign(tcb, fl_nil, p~d);\n', [N], Ok).
write_wrapper_confirm([], no, _, _, Ok) :- otherwise | Ok = [].

write_wrapper_params([], [], N, no, Out, Ok) :-
    N < 4 |
    fmt:format(Out, ', FL_VAL p~d', [N], Ok).
write_wrapper_params([], [], _, yes, _, Ok) :- Ok = [].
write_wrapper_params([], [_|_], 3, _, Out, Ok) :-
    fmt:format(Out, ', FL_VAL p3', [], Ok).
write_wrapper_params([], [-_|Args], N, _, Out, Ok) :-
    N < 3 |
    N2 is N + 1,
    fmt:format(Out, ', FL_VAL p~d', [N], Ok1),
    write_wrapper_params(Ok1, Args, N2, yes, Out, Ok).
write_wrapper_params([], [_|Args], N, R, Out, Ok) :-
    otherwise |
    N2 is N + 1,
    fmt:format(Out, ', FL_VAL p~d', [N], Ok1),
    write_wrapper_params(Ok1, Args, N2, R, Out, Ok).

write_wrapper_conv([], [], _, _, _, _, Ok) :- Ok = [].
write_wrapper_conv([], [-void], _, yes, _, _, Ok) :- Ok = [].
write_wrapper_conv([], [-void], N, no, A, Out, Ok) :-
    wrapper_accessor(N, A, _, Out, Ok).
write_wrapper_conv([], [string|Args], N, RA, A, Out, Ok) :-
    wrapper_accessor(N, A, Var, Out, Ok1),
    when(Ok1, fmt:format(Out, '  CHECK_STRING(~a);\n  char *a~d = fl_stringify_next(tcb, &sp, ~a, &sl);\n  sp += sl + 1;\n',
        [Var, N, Var], Ok2)),
    N2 is N + 1,
    write_wrapper_conv(Ok2, Args, N2, RA, A, Out, Ok).
write_wrapper_conv([], [charlist|Args], N, RA, A, Out, Ok) :-
    wrapper_accessor(N, A, Var, Out, Ok1),
    when(Ok1, fmt:format(Out, '  char *a~d = fl_stringify_next(tcb, &sp, ~a, &sl);\n  sp += sl + 1;\n',
        [N, Var], Ok2)),
    N2 is N + 1,
    write_wrapper_conv(Ok2, Args, N2, RA, A, Out, Ok).
write_wrapper_conv([], [real|Args], N, RA, A, Out, Ok) :-
    wrapper_accessor(N, A, Var, Out, Ok1),
    when(Ok1, fmt:format(Out, '  CHECK_NUMBER(~a);\n  FLOAT(~a, a~d);\n',
        [Var, Var, N], Ok2)),
    N2 is N + 1,
    write_wrapper_conv(Ok2, Args, N2, RA, A, Out, Ok).
write_wrapper_conv([], [pointer|Args], N, RA, A, Out, Ok) :-
    wrapper_accessor(N, A, Var, Out, Ok1),
    when(Ok1, fmt:format(Out, '  CHECK_INT(~a);\n  void *a~d = (void *)POINTER(~a);\n',
        [Var, N, Var], Ok2)),
    N2 is N + 1,
    write_wrapper_conv(Ok2, Args, N2, RA, A, Out, Ok).
write_wrapper_conv([], [pointer(T)|Args], N, RA, A, Out, Ok) :-
    wrapper_accessor(N, A, Var, Out, Ok1),
    when(Ok1, fmt:format(Out, '  CHECK_INT(~a);\n  ~a *a~d = (~a *)POINTER(~a);\n',
        [Var, T, N, T, Var], Ok2)),
    N2 is N + 1,
    write_wrapper_conv(Ok2, Args, N2, RA, A, Out, Ok).
write_wrapper_conv([], [tuple|Args], N, RA, A, Out, Ok) :-
    wrapper_accessor(N, A, Var, Out, Ok1),
    when(Ok1, fmt:format(Out, '  CHECK_TUPLE(~a);\n  FL_VAL a~d = ~a;\n',
        [Var, N, Var], Ok2)),
    N2 is N + 1,
    write_wrapper_conv(Ok2, Args, N2, RA, A, Out, Ok).
write_wrapper_conv([], [-T], N, _, A, Out, Ok) :-
    T =\= void |
    wrapper_accessor(N, A, _, Out, Ok).
write_wrapper_conv([], [-T|Args], N, _, A, Out, Ok) :-
    T =\= void, list(Args) |
    list:member(T, [string, charlist], F),
    wrapper_accessor(N, A, _, Out, Ok0),
    (data(Ok0), F == true  ->
        fmt:format(Out, '  char *a~d;\n', [N], Ok1)
    ;
        native_type(T, NT),
        fmt:format(Out, '  ~a a~d;\n', [NT, N], Ok1)
    ),
    N2 is N + 1,
    write_wrapper_conv(Ok1, Args, N2, yes, A, Out, Ok).
write_wrapper_conv([], [T|Args], N, RA, A, Out, Ok) :-
    otherwise |
    list:member(T, [integer, long, char, short], F),
    wrapper_accessor(N, A, Var, Out, _) &
    (F == true  ->
        native_type(T, NT),
        fmt:format(Out, '  CHECK_INT(~a);\n  ~a a~d = (~a)INT(~a);\n',
            [Var, NT, N, NT, Var], Ok1)
    ;
        fmt:format(Out, '  FL_VAL a~d = ~a;\n', [N, Var], Ok1)
    ),
    N2 is N + 1,
    write_wrapper_conv(Ok1, Args, N2, RA, A, Out, Ok).

wrapper_accessor(N, _, Var, _, Ok) :-
    N < 3 |
    u:format_to_atom('p~d', [N], Var),
    Ok = [].
wrapper_accessor(3, A, Var, Out, Ok) :-
    A > 3 |
    fmt:format(Out, '  FL_VAL pr = CDR(p3);\n  p3 = CAR(p3);\n', [], Ok),
    Var = p3.
wrapper_accessor(N, A, Var, Out, Ok) :-
    N >= 3, A > 3 |
    fmt:format(Out, '  LIST(pr, p~d, r~d);\n  pr = r~d;\n', [N, N, N], Ok),
    u:format_to_atom('p~d', [N], Var).
wrapper_accessor(_, _, Var, _, Ok) :- otherwise | Var = p3, Ok = [].

write_wrapper_call([], Name, Args, Out, Ok) :-
    has_result(Args, Type, F),
    write_wrapper_call2(F, Name, Args, Type, Out, Ok).

write_wrapper_call2(true, Name, Args, Type, Out, Ok) :-
    length(Args, N),
    N2 is N - 1,
    fmt:format(Out, '  ~a a~d = ~a(', [Type, N2, Name], Ok1),
    write_wrapper_args(Ok1, true, Args, 0, Out, Ok2),
    when(Ok2, fmt:format(Out, ');\n', [], Ok3)),
    write_wrapper_results(Ok3, Args, 0, Out, Ok).
write_wrapper_call2(false, Name, Args, _, Out, Ok) :-
    fmt:format(Out, '  ~a(', [Name], Ok1),
    write_wrapper_args(Ok1, false, Args, 0, Out, Ok2),
    when(Ok2, fmt:format(Out, ');\n', [], Ok3)),
    write_wrapper_results(Ok3, Args, 0, Out, Ok).

has_result([], _, F) :- F = false.
has_result([-void], _, F) :- F = false.
has_result([-integer], R, F) :- R = long, F = true.
has_result([-T], NT, F) :-
	T =\= integer, T =\= void |
	native_type(T, NT), F = true.
has_result([_|Args], T, F) :- otherwise | has_result(Args, T, F).

-mode(native_type(?, ^)).
native_type(integer, int).
native_type(long, long).
native_type(short, short).
native_type(string, 'char *').
native_type(pointer, 'void *').
native_type(real, double).
native_type(char, char).
native_type(pointer(T), NT) :- u:format_to_atom('~a *', [T], NT).
native_type(_, 'FL_VAL') :- otherwise | true.

write_wrapper_args([], _, [], _, _, Ok) :- Ok = [].
write_wrapper_args([], true, [_], _, _, Ok) :- Ok = [].
write_wrapper_args([], false, [-void], _, _, Ok) :- Ok = [].
write_wrapper_args([], RF, [T|Args], N, Out, Ok) :-
	otherwise |
	(N > 0 -> fmt:format(Out, ', ', [], Ok1); Ok1 = []),
	write_wrapper_arg(Ok1, T, N, Out, Ok2),
	N2 is N + 1,
	write_wrapper_args(Ok2, RF, Args, N2, Out, Ok).

write_wrapper_arg([], -_, N, Out, Ok) :-
	fmt:format(Out, '&a~d', [N], Ok).
write_wrapper_arg([], _, N, Out, Ok) :-
	otherwise |
	fmt:format(Out, 'a~d', [N], Ok).

write_wrapper_results([], [], _, _, Ok) :- Ok = [].
write_wrapper_results([], [-void], _, _, Ok) :- Ok = [].
write_wrapper_results([], [-real|Args], N, Out, Ok) :-
    fmt:format(Out, '  fl_unify_result(tcb, fl_alloc_float(tcb, a~d), p~d);\n',
        [N, N], Ok1),
    N2 is N + 1,
    write_wrapper_results(Ok1, Args, N2, Out, Ok).
write_wrapper_results([], [-string|Args], N, Out, Ok) :-
    fmt:format(Out, '  fl_unify_result(tcb, fl_mkstring(a~d, strlen(a~d)), p~d);\n',
        [N, N, N], Ok1),
    N2 is N + 1,
    write_wrapper_results(Ok1, Args, N2, Out, Ok).
write_wrapper_results([], [-charlist|Args], N, Out, Ok) :-
    fmt:format(Out, '  fl_unify_result(tcb, mkcharlist(tcb, a~d, strlen(a~d), fl_nil), p~d);\n',
        [N, N, N], Ok1),
    N2 is N + 1,
    write_wrapper_results(Ok1, Args, N2, Out, Ok).
write_wrapper_results([], [-T|Args], N, Out, Ok) :-
    T =\= string, T =\= real, T =\= void, T =\= charlist |
    list:member(T, [char, integer, long, short], F),
    write_wrapper_results2(F, T, Args, N, Out, Ok).
write_wrapper_results([], [_|Args], N, Out, Ok) :-
    otherwise |
    N2 is N + 1,
    write_wrapper_results([], Args, N2, Out, Ok).

write_wrapper_results2(true, _, Args, N, Out, Ok) :-
    fmt:format(Out, '  fl_unify_result(tcb, MKINT(a~d), p~d);\n', [N, N], Ok1),
    N2 is N + 1,
    write_wrapper_results(Ok1, Args, N2, Out, Ok).
write_wrapper_results2(false, pointer, Args, N, Out, Ok) :-
    write_wrapper_results_ptr(Args, N, Out, Ok).
write_wrapper_results2(false, pointer(_), Args, N, Out, Ok) :-
    write_wrapper_results_ptr(Args, N, Out, Ok).
write_wrapper_results(false, _, Args, N, Out, Ok) :-
    otherwise |
    fmt:format(Out, '  fl_unify_result(tcb, a~d, p~d);\n', [N, N], Ok1),
    N2 is N + 1,
    write_wrapper_results(Ok1, Args, N2, Out, Ok).

write_wrapper_results_ptr(Args, N, Out, Ok) :-
    fmt:format(Out, '  fl_unify_result(tcb, MKINT((unsigned long)a~d), p~d);\n',
        [N, N], Ok1),
    N2 is N + 1,
    write_wrapper_results(Ok1, Args, N2, Out, Ok).

extern_c(ExtC) :-
	global(foreign_language, FL),
	(FL == cpp -> ExtC = 'extern "C" '; ExtC = '').

% FLENG compiler - AArch64 backend - generic part
%
%   A: x0
%   T: x19 (callee save)
%   C: x0
%   G: x20 (callee save)
%   N: x21 (callee save)
%   E0-E5: x22-x27, E<n>: stack, F points to E6, ...
%   F: x29
%   P0-P3: x1-x4 (tcb is passed in x0)
%   SP: x28, on entry set to x29
%   X0, X1: x8, x9

-include('compiler/t.ghc').

-mode(sym_prefix(^)).

d_setup-_.

d_finalize-Out :- emit({floats})-Out.

emit_external(_)-_.

comment_prefix(P) :- P = ('//').

d_global(Name)-Out :-
    sym_prefix(P),
    emit('  .global ~a~a\n', [P, Name])-Out.

d_section(Name)-Out :- emit('  ~a\n', [Name])-Out.
d_comment(Str)-Out :- emit('// ~s\n', [Str])-Out.
d_wreserve(N)-Out :- emit('  .space ~d*8\n', [N])-Out.

d_function_entry-Out :-
    emit('  sub sp, sp, #32\n  str x30, [sp]\n  stp x8, x9, [sp, #8]\n')-Out.
d_function_exit-Out :-
    emit('  ldr x30, [sp]\n  ldp x8, x9, [sp, #8]\n  add sp, sp, #32\n')-Out.
d_endfunction-_.

c_prepare_foreign_call(_)-_.

c_finalize_foreign_call(N)-Out :-
    N > 8 |
    M is (N - 8 + (N /\ 1)) * 8,
    emit('  add sp, sp, #~d\n', [M])-Out.
c_finalize_foreign_call(_)-_ :- otherwise | true.

c_prepare_foreign_c_call(_)-_.

c_finalize_foreign_c_call(N)-Out :-
    c_finalize_foreign_call(N)-Out.

c_push_arg(R, I, N)-Out :-
    I =:= N - 1, N > 8 |
    K is (N - 8 + (N /\ 1)) * 8, % plus dummy to align stack, if needed
    emit('  sub sp, sp, #~d\n', [K])-Out,
    r_reg(R, RR),
    Off is (I - 8) * 8,
    emit('  str ~a, [sp, #~d]\n', [RR, Off])-Out.
c_push_arg(R, I, _)-Out :-
    I =< 7 |
    r_reg(R, RR),
    emit('  mov x~d, ~a\n', [I, RR])-Out.
c_push_arg(R, I, _)-Out :-
    otherwise |
    r_reg(R, RR),
    Off is (I - 8) * 8,
    emit('  str ~a, [sp, #~d]\n', [RR, Off])-Out.

d_align(code)-Out :- emit('  .balign 8\n')-Out.
d_align(even)-Out :- emit('  .balign 2\n')-Out.
d_align(halfword)-Out :- emit('  .balign 4\n')-Out.
d_align(word)-Out :- emit('  .balign 8\n')-Out.
d_align(float)-Out :- emit('  .balign 8\n')-Out.

d_equ(S, X)-Out :- emit('  .equ ~a, ~d\n', [S, X])-Out.
d_equ(S, Index, X)-Out :- emit('  .equ ~a~d, ~d\n', [S, Index, X])-Out.

d_bdata([B])-Out :- emit('  .byte ~w\n', [B])-Out.
d_bdata([B|L])-Out :-
    otherwise |
    emit('  .byte ~w', [B])-Out,
    d_data1(L)-Out.

d_wdata([B])-Out :- emit('  .quad ~w\n', [B])-Out.
d_wdata([B|L])-Out :-
    otherwise |
    emit('  .quad ~w', [B])-Out,
    d_data1(L)-Out.

d_fdata([N])-Out :- emit('  .double ~d\n', [N])-Out.
d_fdata([N|L])-Out :-
    otherwise |
    emit('  .double ~d', [N])-Out,
    d_data1(L)-Out.

d_data1([])-Out :- emit('\n')-Out.
d_data1([B|L])-Out :-
    emit(', ~w', [B])-Out,
    d_data1(L)-Out.

d_label(Str)-Out :- emit('~a:\n', [Str])-Out.
d_label(Prefix, Index)-Out :- emit('~a~d:\n', [Prefix, Index])-Out.

dg_label(Str)-Out :-
    sym_prefix(P),
    emit('~a~a:\n', [P, Str])-Out.

a_call(Name)-Out :-
    sym_prefix(P),
    emit('  bl ~a~a\n', [P, Name])-Out.

-mode(r_reg(?, ^)).
r_reg(a, x0).
r_reg(c, x0).
r_reg(t, x19).
r_reg(g, x20).
r_reg(n, x21).
r_reg(fp, x29).
r_reg(sp, x28).
r_reg(e(N), R) :- e_reg(N, R).
r_reg(p(N), R) :- p_reg(N, R).
r_reg(x(N), R) :- x_reg(N, R).

-mode(p_reg(?, ^)).
p_reg(0, x1).
p_reg(1, x2).
p_reg(2, x3).
p_reg(3, x4).

-mode(x_reg(?, ^)).
x_reg(0, x8).
x_reg(1, x9).

-mode(e_reg(?, ^)).
e_reg(0, x22).
e_reg(1, x23).
e_reg(2, x24).
e_reg(3, x25).
e_reg(4, x26).
e_reg(5, x27).

r_move_r(R, R)-_.
r_move_r(NR, R)-Out :-
    otherwise |
    r_reg(R, RR),
    emit('  ldr ~a, ~a\n', [RR, NR])-Out.

r_move(Src, Src)-_.
r_move(Src, e(N))-Out :-
    N > 5 |
    N2 is N - 6,
    r_store(Src, fp, N2)-Out.
r_move(e(N), Dest)-Out :-
    N > 5 |
    N2 is N - 6,
    r_load(fp, N2, Dest)-Out.
r_move(Src, Dest)-Out :-
    otherwise |
    r_reg(Src, SR),
    r_reg(Dest, DR),
    emit('  mov ~a, ~a\n', [DR, SR])-Out.

r_load(RI, I, RD)-Out :-
    r_reg(RI, RIR),
    r_reg(RD, RDR),
    I2 is I * 8,
    emit('  ldr ~a, [~a, #~d]\n', [RDR, RIR, I2])-Out.

a_load(A, I, RD)-Out :-
    r_reg(RD, RDR),
    I2 is I * 8,
    relocation(A, Hi, Lo),
    emit('  adrp ~a, ~s\n  add ~a, ~a, ~s\n  ldr ~a, [~a, #~d]\n',
        [RDR, Hi, RDR, RDR, Lo, RDR, RDR, I2])-Out.

r_store(RS, RI, I)-Out :-
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    I2 is I * 8,
    emit('  str ~a, [~a, #~d]\n', [RSR, RIR, I2])-Out.

la_load(L, RD)-Out :-
    r_reg(RD, RDR),
    relocation(L, Hi, Lo),
    emit('  adrp ~a, ~s\n  add ~a, ~a, ~s\n', [RDR, Hi, RDR, RDR, Lo])-Out.
la_load(L, Index, RD)-Out :-
    r_reg(RD, RDR),
    relocation(L, Index, Hi, Lo),
    emit('  adrp ~a, ~s\n  add ~a, ~a, ~s\n', [RDR, Hi, RDR, RDR, Lo])-Out.

la_loadm(L, Index, RD)-Out :-
    r_reg(RD, RDR),
    relocation(L, Index, Hi, Lo),
    emit('  adrp ~a, ~s\n  add ~a, ~a, ~s\n orr ~a, ~a, #1\n',
        [RDR, Hi, RDR, RDR, Lo, RDR, RDR])-Out.

lag_load(L, RD)-Out :-
    r_reg(RD, RDR),
    sym_prefix(P),
    fmt:format_chars('~a~a', [P, L], L1),
    list_to_string(L1, L1s),
    relocation(L1s, Hi, Lo),
    emit('  adrp ~a, ~s\n  add ~a, ~a, ~s\n', [RDR, Hi, RDR, RDR, Lo])-Out.

i_load(I, RD)-Out :-
    I >= 0xffff |
    r_reg(RD, RDR),
    I1 is I /\ 0xffff,
    I2 is I >> 16,
    emit('  mov ~a, #~d\n', [RDR, I1])-Out,
    i_load_next(I2, 16, RDR)-Out.
i_load(I, RD)-Out :-
    otherwise |
    r_reg(RD, RDR),
    emit('  mov ~a, #~d\n', [RDR, I])-Out.

i_load_next(0, _, _)-_.
i_load_next(-1, _, _)-_.
i_load_next(N, S, RD)-Out :-
    otherwise |
    I1 is N /\ 0xffff,
    I2 is N >> 16,
    emit('  movk ~a, #~d, lsl ~d\n', [RD, I1, S])-Out,
    S2 is S + 16,
    i_load_next(I2, S2, RD)-Out.

f_load(F)-Out :-
    Out <= float(F, L),
    relocation(f, L, Hi, Lo),
    emit('  adrp x8, ~s\n  add x8, x8, ~s\n  ldr d0, [x8]\n', [Hi, Lo])-Out.

r_push(R)-Out :-
    r_reg(R, RR),
    emit('  str ~a, [x28, #-8]!\n', [RR])-Out.

r_pop(R)-Out :-
    r_reg(R, RR),
    emit('  ldr ~a, [x28], #8\n', [RR])-Out.

g_return-Out :- emit('  ret\n')-Out.

ra_inc(R)-Out :-
    r_reg(R, RR),
    emit('  ldr w8, [~a]\n  add w8, w8, #1\n  str w8, [~a]\n', [RR, RR])-Out.

la_inc(L)-Out :-
    relocation(L, Hi, Lo),
    emit('  adrp x8, ~s\n  add x8, x8, ~s\n  ldr w9, [x8]\n  add w9, w9, #1\n  str w9, [x8]\n',
        [Hi, Lo])-Out.

sp_align-_.

ra_add(RS, N, RD)-Out :-
    r_reg(RS, RSR),
    r_reg(RD, RDR),
    N2 is N * 8,
    emit('  add ~a, ~a, #~d\n', [RDR, RSR, N2])-Out.

r_r_op(OP, RA, RB, RD)-Out :-
    r_reg(RA, RAR),
    r_reg(RB, RBR),
    r_reg(RD, RDR),
    emit('  ~a ~a, ~a, ~a\n', [OP, RDR, RAR, RBR])-Out.

r_and(RA, RB, RD)-Out :- r_r_op(and, RA, RB, RD)-Out.

r_or(RA, RB, RD)-Out :- r_r_op(orr, RA, RB, RD)-Out.

r_xor(RA, RB, RD)-Out :-
    % special case
    r_reg(RA, RAR),
    r_reg(RB, RBR),
    r_reg(RD, RDR),
    emit('  eor ~a, ~a, ~a\n  add ~a, ~a, #1\n', [RAR, RAR, RBR, RDR, RAR])-Out.

r_fx_add(RA, RB, RD)-Out :-
    r_reg(RA, RAR),
    r_reg(RB, RBR),
    r_reg(RD, RDR),
    emit('  sub ~a, ~a, #1\n  add ~a, ~a, ~a\n', [RDR, RAR, RDR, RDR, RBR])-Out.

r_fx_sub(RA, RB, RD)-Out :-
    r_reg(RA, RAR),
    r_reg(RB, RBR),
    r_reg(RD, RDR),
    emit('  sub ~a, ~a, ~a\n  add ~a, ~a, #1\n', [RAR, RAR, RBR, RDR, RAR])-Out.

i_mask(R, M)-Out :-
    r_reg(R, RR),
    emit('  and ~a, ~a, #~w\n', [RR, RR, M])-Out.

i_or(M, R)-Out :-
    r_reg(R, RR),
    emit('  orr ~a, ~a, #~w\n', [RR, RR, M])-Out.

j_if_carry(L)-Out :- emit('  bcs ~a\n', [L])-Out.
j_if_carry(L, Index)-Out :- emit('  bcs ~a~d\n', [L, Index])-Out.

j_compare_if_equal(RS, RI, I, L, Index)-Out :-
    r_load(RI, I, a)-Out,
    r_reg(RS, RSR),
    r_reg(a, RI2),
    emit('  cmp ~a, ~a\n  beq ~a~d\n', [RI2, RSR, L, Index])-Out.

j_r_compare_if_not_equal(RS, RI, L, Index)-Out :-
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    emit('  cmp ~a, ~a\n  bne ~a~d\n', [RSR, RIR, L, Index])-Out.

j_a_compare_if_not_equal(RS, A, L, Index)-Out :-
    r_reg(RS, RSR),
    sym_prefix(P),
    relocation(A, Hi, Lo),
    emit('  adrp x8, ~a~s\n  add x8, x8, ~a~s\n  ldr x8, [x8]\n  cmp ~a, x8\n  bne ~a~d\n',
        [P, Hi, P, Lo, RSR, L, Index])-Out.

j_compare_if_not_equal(RS, RI, I, L, Index)-Out :-
    r_load(RI, I, a)-Out,
    r_reg(RS, RSR),
    r_reg(a, RI2),
    emit('  cmp ~a, ~a\n  bne ~a~d\n', [RI2, RSR, L, Index])-Out.

j_if_test(R, I, L, Index)-Out :-
    r_reg(R, RR),
    emit('  tst ~a, #~d\n  beq ~a~d\n', [RR, I, L, Index])-Out.
j_if_test(R, I, L)-Out :-
    r_reg(R, RR),
    emit('  tst ~a, #~d\n  beq ~a\n', [RR, I, L])-Out.

j_if_not_test(R, I, L, Index)-Out :-
    r_reg(R, RR),
    emit('  tst ~a, #~d\n  bne ~a~d\n', [RR, I, L, Index])-Out.
j_if_not_test(R, I, L)-Out :-
    r_reg(R, RR),
    emit('  tst ~a, #~d\n  bne ~a\n', [RR, I, L])-Out.

j_always(L)-Out :- emit('  b ~a\n', [L])-Out.
j_always(P, Index)-Out :- emit('  b ~a~d\n', [P, Index])-Out.

sp_reset-Out :-
    r_reg(t, R),
    emit('  ldr x0, [~a, #22*8]\n  mov sp, x0\n', [R])-Out.   % tcb->sp0

set_or(R1, R2, RD)-Out :-
    r_reg(R1, R1R),
    r_reg(R2, R2R),
    r_reg(RD, RDR),
    sym_prefix(P),
    relocation(fl_true, Hi, Lo),
    emit('  adrp x8, ~a~s\n  add x8, x8, ~a~s\n  ldr x8, [x8]\n  mov x9, ~a\n  cmp x8, x9\n  csel ~a, ~a, x9, ne\n',
        [P, Hi, P, Lo, R1R, RDR, R2R])-Out.

set_and(R1, R2, RD)-Out :-
    r_reg(R1, R1R),
    r_reg(R2, R2R),
    r_reg(RD, RDR),
    sym_prefix(P),
    relocation(fl_true, Hi, Lo),
    emit('  adrp x8, ~a~s\n  add x8, x8, ~a~s\n  ldr x8, [x8]\n  mov x9, ~a\n  cmp x8, x9\n  csel ~a, ~a, x9, eq\n',
        [P, Hi, P, Lo, R1R, RDR, R2R])-Out.

set_equal(R1, R2, RD)-Out :-
    r_reg(R1, R1R),
    r_reg(R2, R2R),
    r_reg(RD, RDR),
    sym_prefix(P),
    relocation(fl_true, Hi1, Lo1),
    relocation(fl_false, Hi2, Lo2),
    emit('  adrp x8, ~a~s\n  add x8, x8, ~a~s\n  ldr x8, [x8]\n  adrp x9, ~a~s\n  add x9, x9, ~a~s\n  ldr x9, [x9]\n  cmp ~a, ~a\n  csel ~a, x9, x8, ne\n',
        [P, Hi1, P, Lo1, P, Hi2, P, Lo2, R1R, R2R, RDR])-Out.

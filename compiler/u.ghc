% FLENG compiler - utilities

-module(u).

% generic error handler

error_handler(Name, P) :-
    open_port(P, S),
    handle_errors(S, Name).

handle_errors([], _).
handle_errors([error(Ln, Fmt, Args, Code)|S], Name) :-
    integer(Ln) |
    fmt:format(2, 'Error: (~s:~d) ~?\n', [Name, Ln, Fmt, Args], _) &
    handle_errors(Code, S, Name).
handle_errors([error(Name:Ln, Fmt, Args, Code)|S], _) :-
    fmt:format(2, 'Error: (~s:~d) ~?\n', [Name, Ln, Fmt, Args], _) &
    handle_errors(Code, S, Name).
handle_errors([error(Ln, Fmt, Args)|S], Name) :-
    integer(Ln) |
    fmt:format(2, 'Error: (~s:~d) ~?\n', [Name, Ln, Fmt, Args], _) &
    handle_errors(1, S, Name).
handle_errors([error(N, Fmt, Args)|S], Name) :-
    string(N) |
    fmt:format(2, 'Error: (~s) ~?\n', [N, Fmt, Args], _) &
    handle_errors(1, S, Name).
handle_errors([error(Name:0, Fmt, Args)|S], _) :-
    fmt:format(2, 'Error: (~s) ~?\n', [Name, Fmt, Args], _) &
    handle_errors(1, S, Name).
handle_errors([error(Name:Ln, Fmt, Args)|S], _) :-
    Ln =\= 0 |
    fmt:format(2, 'Error: (~s:~d) ~?\n', [Name, Ln, Fmt, Args], _) &
    handle_errors(1, S, Name).
handle_errors([error(Fmt, Args)|S], Name) :-
    fmt:format(2, 'Error: (~s) ~?\n', [Name, Fmt, Args], _) &
    handle_errors(1, S, Name).
handle_errors([warning(Ln, Fmt, Args)|S], Name) :-
    integer(Ln) |
    fmt:format(2, 'Warning: (~s:~d) ~?\n', [Name, Ln, Fmt, Args], _) &
    handle_errors(S, Name).
handle_errors([warning(N, Fmt, Args)|S], Name) :-
    string(N) |
    fmt:format(2, 'Warning: (~s) ~?\n', [N, Fmt, Args], _) &
    handle_errors(S, Name).
handle_errors([warning(Name:0, Fmt, Args)|S], _) :-
    fmt:format(2, 'Warning: (~s) ~?\n', [Name, Fmt, Args], _) &
    handle_errors(S, Name).
handle_errors([warning(Name:Ln, Fmt, Args)|S], _) :-
    Ln =\= 0 |
    fmt:format(2, 'Warning: (~s:~d) ~?\n', [Name, Ln, Fmt, Args], _) &
    handle_errors(S, Name).
handle_errors([warning(Fmt, Args)|S], Name) :-
    fmt:format(2, 'Warning: (~s) ~?\n', [Name, Fmt, Args], _) &
    handle_errors(S, Name).
handle_errors([note(Fmt, Args)|S], Name) :-
    fmt:format(1, '~?\n', [Fmt, Args], _) &
    handle_errors(S, Name).

handle_errors(0, S, Name) :- handle_errors(S, Name).
handle_errors(N, _, _) :- otherwise | halt(N).


% dump list of expressions

dump([]).
dump([X|L]) :-
    fmt:format('~q.\n', [X]) &
    dump(L).


% format and convert to string

format_to_atom(FStr, Args, Out) :-
    fmt:format_chars(FStr, Args, Out1),
    list_to_string(Out1, Out).


% open file or return std fileno

open_output_file('-', F) :- F = 1.
open_output_file(Name, F) :-
    otherwise | open_file(Name, w, F).

open_input_file('-', F) :- F = 0.
open_input_file(Name, F) :-
    otherwise | open_file(Name, r, F).


% find entry in map with key A/B with same A but arbitrary B

find_similar(A, t(_, A/B, _, _, _), F) :- F = B.
find_similar(A, t(_, X/_, _, L, _), F) :-
    A @< X | find_similar(A, L, F).
find_similar(A, t(_, X/_, _, _, R), F) :-
    A @> X | find_similar(A, R, F).
find_similar(_, _, F) :- otherwise | F = false.

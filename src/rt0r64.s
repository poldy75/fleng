# FLENG - asm part of RISCV64 runtime

    .section .text

    .global fl_check_atom
    # a0=val
fl_check_atom:
    andi t3, a0, 3
    li t4, 2
    bne t3, t4, l1
    ret
l1:
    mv a1, a0
    mv a0, s1
    li a2, 1          # FL_NOT_AN_ATOM
    j fl_rt_error

    .global fl_check_cell
    # a0=val
fl_check_cell:
    andi t3, a0, 3
    bne t3, zero, l2
    ret
l2:
    mv a1, a0
    mv a0, s1
    li a2, 2          # FL_NOT_A_CELL
    j fl_rt_error

    .global fl_check_integer
    # a0=val
fl_check_integer:
    andi t3, a0, 1         # FL_INT_BIT
    beq t3, zero, l3
    ret
l3:
    mv a1, a0
    mv a0, s1
    li a2, 3          # FL_NOT_AN_INT
    j fl_rt_error

    .global fl_check_list
    # a0=val
fl_check_list:
    andi t3, a0, 3
    bne t3, zero, l4
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x01000000 # FL_LIST_TAG
    bne t3, t4, l4
    ret
l4:
    mv a1, a0
    mv a0, s1
    li a2, 4          # FL_NOT_A_LIST
    j fl_rt_error

    .global fl_check_module
    # a0=val
fl_check_module:
    andi t3, a0, 3
    bne t3, zero, l4b
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x0a000000 # FL_MODULE_TAG
    bne t3, t4, l4b
    ret
l4b:
    mv a1, a0
    mv a0, s1
    li a2, 23          # FL_NOT_A_MODULE
    j fl_rt_error

    .global fl_check_tuple
    # a0=val
fl_check_tuple:
    andi t3, a0, 3
    bne t3, zero, l5
    lw t3, (a0)
    li t4, 0x80000000  # FL_TUPLE_BIT
    and t3, t3, t4
    beq t3, zero, l5
    ret
l5:
    mv a1, a0
    mv a0, s1
    li a2, 5          # FL_NOT_A_TUPLE
    j fl_rt_error

    .global fl_check_var
    # a0=val
fl_check_var:
    andi t3, a0, 3
    bne t3, zero, l6
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x04000000 # FL_VAR_TAG
    beq t3, t4, l6
    li t4, 0x07000000 # FL_REF_TAG
    beq t3, t4, l6
    mv a1, a0
    mv a0, s1
    li a2, 6          # FL_NOT_A_VAR
    j fl_rt_error
l6:
    ret

    .global fl_check_number
    # a0=val
fl_check_number:
    andi t3, a0, 1
    beq t3, zero, l7
    ret
l7:
    andi t3, a0, 2
    bne t3, zero, l8
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x02000000 # FL_FLOAT_TAG
    bne t3, t4, l8
    ret
l8:
    mv a1, a0
    mv a0, s1
    li a2, 11          # FL_NOT_A_NUMBER
    j fl_rt_error

    # a1=val -> fa0
float_arg_1:
    andi t3, a1, 1
    beq t3, zero, l9
    srai a1, a1, 1
    fcvt.d.l fa0, a1
    ret
l9:
    fld fa0, 8(a1)
    ret

    # a2=val -> fa1
float_arg_2:
    andi t3, a2, 1
    beq t3, zero, l10
    srai a2, a2, 1
    fcvt.d.l fa1, a2
    ret
l10:
    fld fa1, 8(a2)
    ret

    .global fl_add
    # a1=num, a2=num -> a0
fl_add:
    andi t3, a1, 1
    beq t3, zero, l11
    andi t3, a2, 1
    beq t3, zero, l11
    addi a0, a1, -1
    add a0, a0, a2
    ret
l11:
    mv t5, ra       # assumes t5 is not changed
    call float_arg_1
    call float_arg_2
    fadd.d fa0, fa0, fa1
    mv ra, t5
    j fl_mkfloat

    .global fl_sub
    # a1=num, a2=num -> a0
fl_sub:
    andi t3, a1, 1
    beq t3, zero, l12
    andi t3, a2, 1
    beq t3, zero, l12
    sub a1, a1, a2
    addi a0, a1, 1
    ret
l12:
    mv t5, ra       # assumes t5 is not changed
    call float_arg_1
    call float_arg_2
    fsub.d fa0, fa0, fa1
    mv ra, t5
    j fl_mkfloat

    .global fl_mul
    # a1=num, a2=num -> a0
fl_mul:
    andi t3, a1, 1
    beq t3, zero, l13
    andi t3, a2, 1
    beq t3, zero, l13
    srai a1, a1, 1
    srai a2, a2, 1
    mul a0, a1, a2
    slli a0, a0, 1
    ori a0, a0, 1
    ret
l13:
    mv t5, ra           # s.a.
    call float_arg_1
    call float_arg_2
    fmul.d fa0, fa0, fa1
    mv ra, t5
    j fl_mkfloat

    .global fl_div
    # a1=num, a2=num -> a0
fl_div:
    li t3, 1
    bne t3, a2, l14
    mv a0, s1
    li a2, 12         # FL_DIV_BY_ZERO
    j fl_rt_error
l14:
    andi t3, a1, 1
    beq t3, zero, l15
    andi t3, a2, 1
    beq t3, zero, l15
    srai a1, a1, 1
    srai a2, a2, 1
    div a0, a1, a2
    slli a0, a0, 1
    ori a0, a0, 1
    ret
l15:
    mv t5, ra       # s.a.
    call float_arg_1
    call float_arg_2
    fdiv.d fa0, fa0, fa1
    mv ra, t5
    j fl_mkfloat

    .global fl_mod
    # a1=int,a2=int -> a0
fl_mod:
    li t3, 1
    bne t3, a2, l14b
    mv a0, s1
    li a2, 12         # FL_DIV_BY_ZERO
    j fl_rt_error
l14b:
    srai a1, a1, 1
    srai a2, a2, 1
    rem a0, a1, a2
    slli a0, a0, 1
    ori a0, a0, 1
    ret

    .global fl_pow
    # a1=num, a2=num -> a0
fl_pow:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    call float_arg_2
    call pow
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_sqrt
    # a1=num -> a0
fl_sqrt:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    fsqrt.d fa0, fa0
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_sin
    # a1=num -> a0
fl_sin:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    call sin
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_cos
    # a1=num -> a0
fl_cos:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    call cos
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_tan
    # a1=num -> a0
fl_tan:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    call tan
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_asin
    # a1=num -> a0
fl_asin:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    call asin
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_acos
    # a1=num -> a0
fl_acos:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    call acos
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_atan
    # a1=num -> a0
fl_atan:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    call atan
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_loge
    # a1=num -> a0
fl_loge:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    call log
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_exp
    # a1=num -> a0
fl_exp:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    call exp
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_truncate
    # a1=num -> a0
fl_truncate:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    fcvt.l.d a0, fa0, rtz
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_round
    # a1=num -> a0
fl_round:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    fcvt.l.d a0, fa0, rmm
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_floor
    # a1=num -> a0
fl_floor:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    fcvt.l.d a0, fa0, rdn
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_ceiling
    # a1=num -> a0
fl_ceiling:
    addi s11, s11, -16
    sd ra, (s11)
    call float_arg_1
    fcvt.l.d a0, fa0, rup
    ld ra, (s11)
    addi s11, s11, 16
    j fl_mkfloat

    .global fl_shl
    # a1=num, a2=num -> a0
fl_shl:
    srli a2, a2, 1
    andi a1, a1, ~1
    sll a0, a1, a2
    ori a0, a0, 1
    ret

    .global fl_shr
    # a1=num, a2=num -> a0
fl_shr:
    srli a2, a2, 1
    sra a0, a1, a2
    ori a0, a0, 1
    ret

    .global fl_int
    # a1=num -> a0
fl_int:
    andi t3, a1, 1
    beq t3, zero, l16
    mv a0, a1
    ret
l16:
    fld fa0, 8(a1)
    fcvt.l.d a0, fa0, rtz
    slli a0, a0, 1
    ori a0, a0, 1
    ret

    .global fl_float
    # a1=num -> a0
fl_float:
    srai a1, a1, 1
    fcvt.d.l fa0, a1
    j fl_mkfloat

true:
    la a0, fl_true
    ld a0, (a0)
    ret

false:
    la a0, fl_false
    ld a0, (a0)
    ret

    .global fl_sametype
    # a1=val, a2=val -> a0
fl_sametype:
    andi a0, a1, 3
    andi t3, a2, 3
    bne a0, t3, false
    andi t3, a0, 3
    bne t3, zero, true
    lw a0, (a1)
    lw t3, (a2)
    li t4, 0x80000000    # FL_TUPLE_BIT
    and t5, a0, t4
    beq t5, zero, l19
    and t5, t3, t4
    bne t5, zero, false
    j true
l19:
    and t5, t3, t4
    bne t5, zero, false
    li t4, 0xff000000     # FL_TAG_MASK
    and a0, a0, t4
    and t3, t3, t4
    bne a0, t3, false
    j true

    .global fl_eq
    # a1=val, a2=val -> a0
fl_eq:
    beq a1, a2, true
    j false

    .global fl_equal
    # a1=val, a2=val -> a0
    # must preserve P0-P3
fl_equal:
    beq a1, a2, true
    andi t3, a1, 1
    beq t3, zero, l22
    andi t3, a2, 1
    beq t3, zero, l23
    j false
l22:
    andi t3, a2, 2
    bne t3, zero, false
    lw t3, (a1)
    li t4, 0xff000000     # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x02000000     # FL_FLOAT_TAG
    bne t3, t4, false
l23:
    andi t3, a2, 2
    bne t3, zero, false
    andi t3, a2, 1
    bne t3, zero, l25
    lw t3, (a2)
    li t4, 0xff000000     # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x02000000     # FL_FLOAT_TAG
    bne t3, t4, false
l25:
    mv t5, ra           # assume t5 is not changed
    call float_arg_1
    call float_arg_2
    mv ra, t5
    feq.d t3, fa0, fa1
    bne t3, zero, true
    j false

    .global fl_pf_equal
    # principal functor equality
    # a1=val, a2=val -> a0
    # must preserve P0-P3
fl_pf_equal:
    beq a1, a2, true
    andi x5, a1, 3
    bne x5, zero, false
    andi x5, a2, 3
    bne x5, zero, false
    lw t3, (a1)
    li x5, 0xff000000		# FL_TAG_MASK
    and t3, t3, x5
    lw t4, (a2)
    and t4, t4, x5
    bne t3, t4, false
    li x5, 0x80000000		# FL_TUPLE_BIT
    and t3, t3, x5
    beq t3, zero, true
    ld t3, 8(a1)
    ld t4, 8(a2)
    bne t3, t4, false
    j true

    .global fl_less
    # a1=val, a2=val -> a0
    # must preserve P0-P3
fl_less:
    andi t3, a1, 1
    beq t3, zero, l26
    andi t3, a2, 1
    beq t3, zero, l26
    bge a1, a2, false
    j true
l26:
    mv t5, ra       # s.a.
    call float_arg_1
    call float_arg_2
    mv ra, t5
    flt.d t3, fa0, fa1
    bne t3, zero, true
    j false

first:
    mv a0, a1
    ret

second:
    mv a0, a2
    ret

    .global fl_max
    # a1=val, a2=val -> a0
    # must preserve P0-P3
fl_max:
    andi t3, a1, 1
    beq t3, zero, l26a
    andi t3, a2, 1
    beq t3, zero, l26a
    bge a1, a2, first
    j second
l26a:
    mv t5, ra       # t5 must not be changed
    addi s11, s11, -16
    sd a1, (s11)
    sd a2, 8(s11)
    call float_arg_1
    call float_arg_2
    mv ra, t5
    flt.d t3, fa0, fa1
    ld a1, (s11)
    ld a2, 8(s11)
    addi s11, s11, 16
    bne t3, zero, second
    j first

    .global fl_min
    # a1=val, a2=val -> a0
    # must preserve P0-P3
fl_min:
    andi t3, a1, 1
    beq t3, zero, l26i
    andi t3, a2, 1
    beq t3, zero, l26i
    bge a1, a2, second
    j first
l26i:
    mv t5, ra       # s.a.
    addi s11, s11, -16
    sd a1, (s11)
    sd a2, 8(s11)
    call float_arg_1
    call float_arg_2
    mv ra, t5
    flt.d t3, fa0, fa1
    ld a1, (s11)
    ld a2, 8(s11)
    addi s11, s11, 16
    bne t3, zero, first
    j second

    .global fl_sign
    # a1=val -> a0
fl_sign:
    andi t3, a1, 1
    bne t3, zero, lsign1
    mv t5, ra   # s.a.
    call float_arg_1
    mv ra, t5
    la t0, lzero
    fld fa1, (t0)
    feq.d t3, fa0, fa1
    bne t3, zero, lsign3
    flt.d t3, fa0, fa1
    bne t3, zero, lsign2
lsign4:
    li a0, 3
    ret
lsign2:
    li a0, -1
    ret
lsign3:
    li a0, 1
    ret
lsign1:
    li t3, 1
    blt a0, t3, lsign2
    bne a0, t3, lsign4
    mv a0, a1
    ret
    .data
lzero: .double 0
    .text

    .global fl_abs
    # a1=val -> a0
fl_abs:
    andi t3, a1, 1
    bne t3, zero, labs1
    mv t5, ra   # s.a.
    call float_arg_1
    mv ra, t5
    fabs.d fa0, fa0
    j fl_mkfloat
labs1:
    blt a0, zero, labs2
    mv a0, a1
    ret
labs2:
    neg a0, a1
    addi a0, a0, 2
    ret

    .global fl_rnd
    # a1=val -> a0
fl_rnd:
    mv a0, a1
    j fl_random_int

    .global fl_fip
    # a1=val -> a0
fl_fip:
    andi t3, a1, 1
    beq t3, zero, lfip1
    mv a0, a1
    ret
lfip1:
    ld a0, 8(a1)
    srli a0, a0, 52
    andi a0, a0, 0x7ff
    addi a0, a0, -0x3ff
    li t3, 52
    blt a0, t3, lfip2
    mv a0, a1      # nan
    ret
lfip2:
    bge a0, zero, lfip4
    li a0, 1
    ret
lfip4:
    li t3, -1
    srli t3, t3, 12
    srl t3, t3, a0
    ld a0, 8(a1)
    and t3, a0, t3
    bne t3, zero, lfip3
    mv a0, a1
    ret
lfip3:
    not t3, t3
    and a0, t3, a0
    fmv.d.x fa0, a0
    j fl_mkfloat

    .global fl_ffp
    # x1=val -> a0
fl_ffp:
    andi t3, a1, 1
    beq t3, zero, lffp1
    li a0, 1
    ret
lffp1:
    ld a0, 8(a1)
    srli a0, a0, 52
    andi a0, a0, 0x7ff
    addi a0, a0, -0x3ff
    li t3, 52
    blt a0, t3, lffp2
    li t3, 0x400
    bne a0, t3, lffp3
    ld a0, 8(a1)
    slli a0, a0, 12
    beq a0, zero, lffp3
    mv a0, a1      # nan
    ret
lffp3:
    li a0, 1
    ret
lffp2:
    bge a0, zero, lffp4
    mv a0, a1
    ret
lffp4:
    li t3, -1
    srli t3, t3, 12
    srl t3, t3, a0
    ld a0, 8(a1)
    and t3, t3, a0
    beq t3, zero, lffp3
    not t3, t3
    and a0, a0, t3
    fmv.d.x fa1, a0
    fld fa0, 8(a1)
    fsub.d fa0, fa0, fa1
    j fl_mkfloat

    .global fl_iless
    # a1=val, a2=val -> a0
fl_iless:
    blt a1, a2, true
    j false

    .global fl_fless
    # a1=val, a2=val -> a0
fl_fless:
    mv t5, ra       # s.a.
    addi s11, s11, -16
    sd a1, (s11)
    sd a2, 8(s11)
    call float_arg_1
    call float_arg_2
    mv ra, t5
    flt.d t3, fa0, fa1
    ld a1, (s11)
    ld a2, 8(s11)
    addi s11, s11, 16
    bne t3, zero, true
    j false

    .global fl_below
    # a1=val, a2=val -> a0
fl_below:
    addi s11, s11, -40
    sd ra, 8(s11)
    sd a1, 16(s11)
    sd a2, 24(s11)
    sd a3, 32(s11)
    mv a0, s1
    mv a3, s11
    call fl_ordering
    ld ra, 8(s11)
    ld a1, 16(s11)
    ld a2, 24(s11)
    ld a3, 32(s11)
    ld t3, (s11)
    addi s11, s11, 40
    beq t3, zero, lbel1
    la a0, fl_nil
    ld a0, (a0)
    ret
lbel1:
    blt a0, zero, true
    j false

    .global fl_below_or_equal
    # a1=val, a2=val -> a0
fl_below_or_equal:
    addi s11, s11, -40
    sd ra, 8(s11)
    sd a1, 16(s11)
    sd a2, 24(s11)
    sd a3, 32(s11)
    mv a0, s1
    mv a3, s11
    call fl_ordering
    ld ra, 8(s11)
    ld a1, 16(s11)
    ld a2, 24(s11)
    ld a3, 32(s11)
    ld t3, (s11)
    addi s11, s11, 40
    beq t3, zero, lbeleq1
    la a0, fl_nil
    ld a0, (a0)
    ret
lbeleq1:
    bgt a0, zero, false
    j true

    .global fl_above
    # a1=val, a2=val -> a0
fl_above:
    addi s11, s11, -40
    sd ra, 8(s11)
    sd a1, 16(s11)
    sd a2, 25(s11)
    sd a3, 32(s11)
    mv a0, s1
    mv a3, s11
    call fl_ordering
    ld ra, 8(s11)
    ld a1, 16(s11)
    ld a2, 24(s11)
    ld a3, 32(s11)
    ld t3, (s11)
    addi s11, s11, 40
    beq t3, zero, labo1
    la a0, fl_nil
    ld a0, (a0)
    ret
labo1:
    li t4, 1
    bne t4, a0, false
    j true

    .global fl_above_or_equal
    # a1=val, a2=val -> a0
fl_above_or_equal:
    addi s11, s11, -40
    sd ra, 8(s11)
    sd a1, 16(s11)
    sd a2, 25(s11)
    sd a3, 32(s11)
    mv a0, s1
    mv a3, s11
    call fl_ordering
    ld ra, 8(s11)
    ld a1, 16(s11)
    ld a2, 24(s11)
    ld a3, 32(s11)
    ld t3, (s11)
    addi s11, s11, 40
    beq t3, zero, laboeq1
    la a0, fl_nil
    ld a0, (a0)
    ret
laboeq1:
    blt a0, zero, false
    j true

    .global fl_deref
    # a0=val -> a0
    # must preserve P0-P3
fl_deref:
    andi t3, a0, 3
    beq t3, zero, l27
l28:
    ret
l27:
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x04000000 # FL_VAR_TAG
    bne t3, t4, l28
    ld t3, 8(a0)
    beq a0, t3, l28
    mv a0, t3
    j fl_deref

    .global fl_unbox
    # a0=val -> a0
fl_unbox:
    andi t3, a0, 3
    beq t3, zero, lu27
lu28:
    ret
lu27:
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x04000000 # FL_VAR_TAG
    bne t3, t4, lu29
    ld t3, 8(a0)
    beq a0, t3, lu28
    mv a0, t3
    j fl_unbox
lu29:
    mv a1, a0
    mv a0, s1
    j fl_unbox_cell

    .global fl_box
    # a1=val -> A
fl_box:
    andi t3, a1, 1
    bne t3, zero, box_l1
    ret
box_l1:
    mv a2, ra          # save return address
    la a0, fl_nil
    ld a0, (a0)
    mv t3, a0
    call cons
    li t4, 0x0f000000     # FL_BOX_LONG_TAG
    sw t4, (a0)
    srai a1, a1, 1
    sd a1, 8(a0)
    mv ra, a2
    ret

    .global fl_unref
    # t3=val
    # must preserve A + P0-P3
fl_unref:
    andi t5, t3, 3
    beq t5, zero, l29
    ret
l29:
    li t5, 0x00ffffff      # FL_COUNT_MASK
    lw t6, (t3)
    and t6, t6, t5
    bne t6, t5, l30
    ret
l30:
    lw t6, (t3)
    addi t6, t6, -1
    and t5, t5, t6
    beq t5, zero, l30a
    sw t6, (t3)
    ret
l30a:
    addi s11, s11, -48
    sd a0, (s11)
    sd a1, 8(s11)
    sd a2, 16(s11)
    sd a3, 24(s11)
    sd ra, 32(s11)
    mv a0, s1
    mv a1, t3
    call fl_release
    ld a0, (s11)
    ld a1, 8(s11)
    ld a2, 16(s11)
    ld a3, 24(s11)
    ld ra, 32(s11)
    addi s11, s11, 48
    ret

setcarry:
    li t5, 1
    ret

clrcarry:
    mv t5, zero
    ret

    .global fl_force
    # a0=val -> a0, t5 is 1 when suspending
fl_force:
    andi t3, a0, 3
    bne t3, zero, clrcarry
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x07000000 # FL_REF_TAG
    beq t3, t4, l33
    li t4, 0x04000000 # FL_VAR_TAG
    bne t3, t4, clrcarry
    ld t3, 8(a0)
    beq a0, t3, l33
    mv a0, t3
    j fl_force
l33:
    ld t3, 8(s1)   # tcb->sstackp
    sd a0, (t3)
    ld a0, (s2)      # goal->addr
    sd a0, 8(t3)
    addi t3, t3, 16
    sd t3, 8(s1)   # tcb->sstackp
    j setcarry

    .global fl_match
    # a1=val1, a2=val2 -> rax
    # assumes args are forced (allows initial quick eq check)
fl_match:
    beq a1, a2, true
    mv a0, s1
    addi s11, s11, -32
    sd a1, (s11)
    sd a2, 8(s11)
    sd ra, 16(s11)
    call fl_match_rec
    ld a1, (s11)
    ld a2, 8(s11)
    ld ra, 16(s11)
    addi s11, s11, 32
    ret

    .global fl_islist_t
    # a0=val, set t5 if not a list
fl_islist_t:
    andi t3, a0, 3
    bne t3, zero, setcarry
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x01000000 # FL_LIST_TAG
    beq t3, t4, clrcarry
    j setcarry

    .global fl_istuple_t
    # a0=val, set t5 if not a tuple
fl_istuple_t:
    andi t3, a0, 3
    bne t3, zero, setcarry
    lw t3, (a0)
    li t4, 0x80000000 # FL_TUPLE_BIT
    and t3, t3, t4
    bne t3, zero, clrcarry
    j setcarry

    # a1=addr
trynext:
    sd a1, (s2)      # goal->addr
    j setcarry

    .global fl_isval
    # a0=val, a2=atom, a1=addr
fl_isval:
    bne a0, a2, trynext
    j clrcarry

    .global fl_isfloat
    # a0=val, d0=float, a1=addr
fl_isfloat:
    andi t3, a0, 3
    bne t3, zero, trynext
    fld fa1, 8(a0)
    feq.d t3, fa0, fa1
    beq t3, zero, trynext
    j clrcarry

    .global fl_islist
    # a0=val, a1=addr -> a0(car)
    # push cdr on stack
fl_islist:
    mv t6, ra       # assumes t6 is not changed
    call fl_islist_t
    mv ra, t6
    bne t5, zero, trynext
    ld t3, 16(a0)
    addi s11, s11, -8
    sd t3, (s11)
    ld a0, 8(a0)
    j clrcarry

    .global fl_isvector
    # a0=val, a1=addr, a2=len -> a0(car)
    # push vector on stack
fl_isvector:
    andi t3, a0, 3
    bne t3, zero, trynext
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x03000000 # FL_VECTOR_TAG
    bne t3, t4, trynext
    ld t3, 16(a0)
    ld t4, 8(t3)
    srai t4, t4, 1
    bne t4, a2, trynext
    addi s11, s11, -8
    sd a0, (s11)
    j clrcarry

    .global fl_velement
    # a1=index(fixnum), stack=vector -> a0=item
fl_velement:
    mv t6, ra			# assume t6 is not changed
    ld a0, (s11)
    ld a0, 8(a0)			# get map
    call fl_deref
    la x5, fl_nil
    ld x5, (x5)
lie1:
    bne a0, x5, lie4
    ld a0, (s11)
    ld a0, 16(a0)
    ld a0, 16(a0)		# default
    mv ra, t6
    ret
lie4:
    ld a0, 16(a0)		# get part of "t" tuple holding k, v
    ld a0, 16(a0)
    ld t3, 8(a0)			# key
    blt a1, t3, lie2
    bgt a1, t3, lie3
    ld a0, 16(a0)
    ld a0, 8(a0)
    mv ra, t6
    ret
lie2:
    ld a0, 16(a0)		# left part
    ld a0, 16(a0)
    ld a0, 8(a0)
    call fl_deref
    j lie1
lie3:
    ld a0, 16(a0)		# right part
    j lie2

    .global fl_isarrayval
    # a0=val, a1=addr, a2=literal_array -> carry (fail)
fl_isarrayval:
    andi t3, a0, 3
    bne t3, zero, trynext
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x08000000 # FL_ARRAY_TAG
    bne t3, t4, trynext
    ld t3, 8(a0)		# CAR(A) = [ptr len/type]
    ld t4, 8(a2)		# CAR(P1) = [ptr len/type]
    ld x5, 16(t3)
    li x7, ~8		# clear FL_ARRAY_IMMUTABLE
    and x5, x5, x7
    ld x6, 16(t4)
    and x6, x6, x7
    bne x5, x6, trynext
    srli x5, x5, 4
    ld t3, 8(t3)
    ld t4, 8(t4)
liav1:
    beq x5, zero, clrcarry
    lb x6, (t3)
    lb x7, (t4)
    bne x6, x7, trynext
    addi t3, t3, 1
    addi t4, t4, 1
    addi x5, x5, -1
    j liav1

    .global fl_istuple
    # a0=val, a1=addr, a2=arity -> a0(head)
    # push arglist on stack
fl_istuple:
    andi t3, a0, 3
    bne t3, zero, trynext
    lw t3, (a0)
    li t4, 0x80000000 # FL_TUPLE_BIT
    and t5, t3, t4
    beq t5, zero, trynext
    srli t3, t3, 24
    andi t3, t3, 0x7f
    bne t3, a2, trynext
    ld a3, 16(a0)
    addi s11, s11, -8
    sd a3, (s11)
    ld a0, 8(a0)
    j clrcarry

    # t3=car, a0=cdr -> a0
    # must preserve parameter registers
cons:
    ld t4, 6 * 8(s1)   # tcb->freelist
    bne t4, zero, l34
    addi s11, s11, -48
    sd a1, (s11)
    sd a2, 8(s11)
    sd a3, 16(s11)
    sd ra, 24(s11)
    sd t3, 32(s11)
    sd a0, 40(s11)
    mv a0, s1
    call fl_new_chunk
    ld a0, 40(s11)
    ld t3, 32(s11)
    ld ra, 24(s11)
    ld a3, 16(s11)
    ld a2, 8(s11)
    ld a1, (s11)
    addi s11, s11, 48
    ld t4, 6 * 8(s1)   # tcb->freelist
l34:
    mv t6, ra       # assumes t6 is not changed
    ld t5, 16(t4)
    sd t5, 6 * 8(s1)  # tcb->freelist
    call fl_addref
    sd a0, 16(t4)
    mv a0, t3
    call fl_addref
    sd a0, 8(t4)
    ld t5, 11 * 8(s1)  # tcb->used
    addi t5, t5, 1
    sd t5, 11 * 8(s1)    # tcb->used
    mv a0, t4
    mv ra, t6
    ret

    .global fl_mkfloat
    # fa0=float64 -> a0
    # must preserve parameter registers
fl_mkfloat:
    addi s11, s11, -8
    sd ra, (s11)
    li t3, 1
    li a0, 1
    call cons
    fsd fa0, 8(a0)
    li t3, 0x02000000      # FL_FLOAT_TAG
    sw t3, (a0)
    ld ra, (s11)
    addi s11, s11, 8
    ret

    .global fl_mklist
    # a0=cdr, <stack>=car -> a0
fl_mklist:
    ld t3, (s11)
    sd ra, (s11)
    call cons
    li t3, 0x01000000      # FL_LIST_TAG
    sw t3, (a0)
    ld ra, (s11)
    addi s11, s11, 8
    ret

    .global fl_mktuple
    # a0=args, t3=arity, t4=atom -> a0
fl_mktuple:
    addi s11, s11, -16
    sd ra, (s11)
    sd t3, 8(s11)
    mv t3, t4
    call cons
    ld ra, (s11)
    ld t3, 8(s11)
    addi s11, s11, 16
    # fall through ...

    .global fl_mktuple0
    # a0=args, t3=arity -> a0
fl_mktuple0:
    slli t3, t3, 24
    li t4, 0x80000000 # FL_TUPLE_BIT
    or t3, t3, t4
    sw t3, (a0)
    ret

    .global fl_tovector
    # a0=lst -> a0
fl_tovector:
    li t4, 0x03000000	# FL_VECTOR_TAG
    sw t4, (a0)
    ret

    .global fl_mkvar
    # -> a0
fl_mkvar:
    addi s11, s11, -8
    sd ra, (s11)
    li t3, 1
    la a0, fl_nil
    ld a0, (a0)
    call cons
    sd a0, 8(a0)
    li t3, 0x04000000   # FL_VAR_TAG
    sw t3, (a0)
    ld ra, (s11)
    addi s11, s11, 8
    ret

    # s3=goal
enqueue:
    ld a0, 13 * 8(s1)     # tcb->qend
    sd s3, (a0)
    addi a0, a0, 8
    ld a3, 12 * 8(s1)     # tcb->qstart
    bne a0, a3, l40
    li a2, 7                  # FL_QUEUE_FULL
    mv a0, s1
    j fl_rt_error
l40:
    ld t3, 15 * 8(s1)     # tcb->max_goals
    slli t3, t3, 3
    ld t4, 14 * 8(s1)     # tcb->queue
    add t3, t3, t4
    bltu a0, t3, l41
    ld a0, 14 * 8(s1)     # tcb->queue
l41:
    sd a0, 13 * 8(s1)     # tcb->qend
    ld t3, 10 * 8(s1)     # tcb->active
    addi t3, t3, 1
    sd t3, 10 * 8(s1)    # tcb->active
    ret

    .global fl_opengoal
    # -> s3
fl_opengoal:
    ld s3, 16 * 8(s1)     # tcb->freegoals
    bne s3, zero, lg1
    li a2, 26                  # FL_TOO_MANY_GOALS
    mv a0, s1
    j fl_rt_error
lg1:
    ld t3, (s3)               # goal->addr
    sd t3, 16 * 8(s1)      # tcb->freegoals
    ld a0, 6 * 8(s2)         # goal->task
    mv t5, ra
    call fl_addref
    mv ra, t5
    sd a0, 6 * 8(s3)     # goal->task
    ld t3, 21 * 8(s1)    # tcb->goals
    addi t3, t3, 1
    sd t3, 21 * 8(s1)    # tcb->goals
    j enqueue

    .global fl_dropgoal
    # s1=tcb
fl_dropgoal:
    ld t3, (s1)               # tcb->goal
    ld t4, 16 * 8(s1)     # tcb->freegoals
    sd t4, (t3)               # goal->addr
    sd t3, 16 * 8(s1)      # tcb->freegoals
    ld t0, 21 * 8(s1)    # tcb->goals
    addi t0, t0, -1
    sd t0, 21 * 8(s1)    # tcb->goals
    sd zero, 5 * 8(t3)    # goal->info
    la t0, fl_nil
    ld t0, (t0)
    ld t4, 6 * 8(t3)     # goal->task
    sd t0, 6 * 8(t3)     # goal->task
    mv t3, t4
    j fl_unref

    .global fl_addref
    # a0=val
    # may not change any registers (but uses t0)
fl_addref:
    andi t0, a0, 3
    beq t0, zero, l42
    ret
l42:
    addi s11, s11, -16
    sd t3, (s11)
    sd t4, 8(s11)
    li t4, 0x00ffffff     # FL_COUNT_MASK
    lw t3, (a0)
    and t3, t3, t4
    beq t3, t4, l42a
    lw t3, (a0)
    addi t3, t3, 1
    sw t3, (a0)
l42a:
    ld t3, (s11)
    ld t4, 8(s11)
    addi s11, s11, 16
    ret

    .global fl_puttailarg
    # a0=list
fl_puttailarg:
    ld t3, 16(a0)
    andi t4, t3, 3
    beq t4, zero, l43
    ld a0, 8(a0)
l43:
    mv t5, ra           # assumes t5 is not changed
    call fl_addref
    mv ra, t5
    sd a0, 4 * 8(s3)   # goal->args[ 3 ]
    ret

    .global fl_reify
    # replace cdr of pair with deref'd element
    # a0=pair
fl_reify:
    addi s11, s11, -16
    sd ra, 8(s11)
    call fl_deref
    sd a0, (s11)
    ld a0, 16(a0)
    mv t5, a0           # assume t5 is not changed
    call fl_deref
    call fl_addref
    mv t3, t5
    call fl_unref
    ld t5, (s11)
    sd a0, 16(t5)
    ld ra, 8(s11)
    addi s11, s11, 16
    ret

    .global fl_resolve
    # a4=cache, a1=arity, a2=module, a3=name
fl_resolve:
    ld t3, (a4)
    beq t3, zero, l44
    sd t3, (s3)        # goal->addr
    ret
l44:
    mv a0, s1
    mv a5, s3
    j fl_resolve_pdef

    .global fl_replace
    # a0=val, a2=argaddr
fl_replace:
    ld t3, (a2)
    bne a0, t3, l45
    ret
l45:
    ld t3, (a2)
    mv t5, ra           # assumes t5 is not changed
    call fl_addref
    mv ra, t5
    sd a0, (a2)
    j fl_unref

    .global fl_tailgoal
    # a2=addr -> s3(N)
fl_tailgoal:
    ld a3, 16(s1) # tcb->tailcalls
    ld a1, 24(s1) # tcb->timeslice
    blt a3, a1, l_tg_1
    addi s11, s11, -16
    sd a2, (s11)
    sd ra, 8(s11)
    call fl_opengoal
    ld a2, (s11)
    ld ra, 8(s11)
    addi s11, s11, 16
    sd a2, (s3)      # goal->addr
    ret
l_tg_1:
    mv s3, s2
    sd a2, (s3)      # goal->addr
    ret

    .global fl_tail_jump
fl_tail_jump:
    ld a3, 16(s1) # tcb->tailcalls
    ld a1, 24(s1) # tcb->timeslice
    blt a3, a1, l46
    sd zero, 16(s1) # tcb->tailcalls
    ret
l46:
    addi a3, a3, 1
    sd a3, 16(s1) # tcb->tailcalls
    # fall through ...

    .global fl_invoke_jump
fl_invoke_jump:
    mv s11, fp
    mv s2, s3
    sd s3, (s1)      # tcb->goal
    ld a0, (s3)      # goal->addr
    jr a0

    .global fl_test_integer
    # a1=val -> a0 (bool)
fl_test_integer:
    andi t3, a1, 1
    bne t3, zero, true
    j false

    .global fl_test_number
    # a1=val -> a0 (bool)
fl_test_number:
    andi t3, a1, 1
    bne t3, zero, true
    li t3, 0x02000000      # FL_FLOAT_TAG
    j fl_test_tag

    .global fl_test_atom
    # a1=val -> a0 (bool)
fl_test_atom:
    mv a0, a1
    andi a0, a0, 3
    li t3, 2
    bne a0, t3, false
    j true

    .global fl_test_tag
    # a1=val, t3=tag -> a0 (bool)
fl_test_tag:
    andi t4, a1, 3
    bne t4, zero, false
    lw a0, (a1)
    li t4, 0xff000000 # FL_TAG_MASK
    and a0, a0, t4
    bne a0, t3, false
    j true

    .global fl_test_var
    # a1=val -> a0 (bool)
fl_test_var:
    andi t3, a1, 3
    bne t3, zero, false
    lw a0, (a1)
    li t3, 0xff000000 # FL_TAG_MASK
    and a0, a0, t3
    li t3, 0x07000000 # FL_REF_TAG
    beq a0, t3, true
    li t3, 0x04000000 # FL_VAR_TAG
    bne a0, t3, false
    ld a0, 8(a1)
    bne a0, a1, false
    j true

    .global fl_test_remote
    # a1=val -> a0 (bool)
fl_test_remote:
    andi t3, a1, 3
    bne t3, zero, false
    lw a0, (a1)
    li t3, 0xff000000 # FL_TAG_MASK
    and a0, a0, t3
    li t3, 0x07000000 # FL_REF_TAG
    beq a0, t3, true
    li t3, 0x05000000 # FL_PORT_TAG
    bne a0, t3, false
    ld a0, 8(a1)
    andi a0, a0, 1
    bne a0, zero, true
    j false

    .global fl_test_tuple
    # a1=val -> a0 (bool)
fl_test_tuple:
    andi t3, a1, 3
    bne t3, zero, false
    lw a0, (a1)
    li t3, 0x80000000 # FL_TUPLE_BIT
    and t3, a0, t3
    beq t3, zero, false
    j true

    .global fl_test_struct
    # a1=val, a2=name, a3=arity -> a0 (bool)
fl_test_struct:
    andi t3, a1, 3         # FL_BITS_MASK
    bne t3, zero, false
    lw a0, (a1)
    srli t3, a0, 24
    andi t3, t3, 0xff
    srli a3, a3, 1
    ori a3, a3, 0x80 		# FL_TUPLE_BIT
    bne a3, t3, false
    ld t3, 8(a1)
    bne t3, a2, false
    j true

    .global fl_test_array
    # a1=val -> a0 (bool)
fl_test_array:
    andi t3, a1, 3         # FL_BITS_MASK
    bne t3, zero, false
    lw a0, (a1)
    li t3, 0xff000000     # FL_TAG_MASK
    and a0, a0, t3
    li t3, 0x08000000     # FL_ARRAY_TAG
    bne a0, t3, false
    j true

    .global fl_test_kl1string
    # a1=val -> a0 (bool)
fl_test_kl1string:
    andi t3, a1, 3         # FL_BITS_MASK
    bne t3, zero, false
    lw a0, (a1)
    li t3, 0xff000000     # FL_TAG_MASK
    and a0, a0, t3
    li t3, 0x08000000     # FL_ARRAY_TAG
    bne a0, t3, false
    ld a0, 8(a1)
    ld a0, 16(a0)
    andi a0, a0, 0x7
    li t3, 6				# FL_KL1_STRING
    bne a0, t3, false
    j true

    .global fl_test_vector
    # a1=val -> a0 (bool)
fl_test_vector:
    andi t3, a1, 3         # FL_BITS_MASK
    bne t3, zero, false
    lw a0, (a1)
    li t3, 0xff000000     # FL_TAG_MASK
    and a0, a0, t3
    li t3, 0x03000000     # FL_VECTOR_TAG
    bne a0, t3, false
    j true

    .global fl_enter
    # a0=tcb
fl_enter:
    mv s1, a0
    ld s11, 5 * 8(s1)  # tcb->stackbase
    ld sp, 22*8(s1)     # tcb->sp0
    mv fp, s11
    ld s2, (s1)      # tcb->goal
    ld a0, (s2)      # goal->addr
    jr a0

    .global fl_atomic
    # a1=val -> a0 (bool)
fl_atomic:
    andi t3, a1, 3         # FL_BITS_MASK
    bne t3, zero, true
    lw a0, (a1)
    li t3, 0xff000000     # FL_TAG_MASK
    and a0, a0, t3
    li t3, 0x02000000     # FL_FLOAT_TAG
    bne a0, t3, false
    j true

    .global fl_atomic_nonfloat
    # a1=val -> a0 (bool)
fl_atomic_nonfloat:
    andi t3, a1, 3         # FL_BITS_MASK
    bne t3, zero, true
    j false

    .global fl_entry
    # a1=arity
fl_entry:
    la t3, fl_nil
    ld t3, (t3)
    ld a0, 6 * 8(s2)      # goal->task
    beq a0, t3, l50a
    ld a0, 8(a0)
    ld a0, 8(a0)
    ld a0, 8(a0)
    bne a0, t3, l50
l50a:
    ld a0, 20 * 8(s1)     # tcb->logging
    andi t3, a0, 1
    bne t3, zero, l50
    ret
l50:
    mv a0, s1
    j fl_log_entry

    .global fl_assign0
    # a1=val, a2=val -> a0 (bool)
    # must preserve P0-P3
fl_assign0:
    addi s11, s11, -32
    sd a1, (s11)
    sd a2, 8(s11)
    sd a3, 16(s11)
    sd ra, 24(s11)
    mv a0, s1
    call fl_assign
    ld ra, 24(s11)
    ld a3, 16(s11)
    ld a2, 8(s11)
    ld a1, (s11)
    addi s11, s11, 32
    ret

    .global fl_d_assign0
    # a1=val, a2=val -> a0 (bool)
    # must preserve P0-P3
fl_d_assign0:
    addi s11, s11, -32
    sd a1, (s11)
    sd a2, 8(s11)
    sd a3, 16(s11)
    sd ra, 24(s11)
    mv a0, s1
    call fl_d_assign
    ld ra, 24(s11)
    ld a3, 16(s11)
    ld a2, 8(s11)
    ld a1, (s11)
    addi s11, s11, 32
    ret

    .global fl_unify
    # a1=val, a2=val -> a0 (bool)
    # must preserve P0-P3
fl_unify:
    addi s11, s11, -32
    sd a1, (s11)
    sd a2, 8(s11)
    sd a3, 16(s11)
    sd ra, 24(s11)
    mv a0, s1
    call fl_unify_rec
    ld ra, 24(s11)
    ld a3, 16(s11)
    ld a2, 8(s11)
    ld a1, (s11)
    addi s11, s11, 32
    ret

    .global fl_d_unify
    # a1=val, a2=val -> a0 (bool)
    # must preserve P0-P3
fl_d_unify:
    addi s11, s11, -32
    sd a1, (s11)
    sd a2, 8(s11)
    sd a3, 16(s11)
    sd ra, 24(s11)
    mv a0, s1
    call fl_d_unify_rec
    ld ra, 24(s11)
    ld a3, 16(s11)
    ld a2, 8(s11)
    ld a1, (s11)
    addi s11, s11, 32
    ret

    .global fl_unify_safe
    # a1=val, a2=val -> a0 (bool)
    # must preserve P0-P3
fl_unify_safe:
    addi s11, s11, -32
    sd a1, (s11)
    sd a2, 8(s11)
    sd a3, 16(s11)
    sd ra, 24(s11)
    mv a0, s1
    call fl_unify_safe1
    ld ra, 24(s11)
    ld a3, 16(s11)
    ld a2, 8(s11)
    ld a1, (s11)
    addi s11, s11, 32
    ret

    .global fl_unify_checked
    # a1=val, a2=val
    # must preserve P0-P3
fl_unify_checked:
    addi s11, s11, -32
    sd a1, (s11)
    sd a2, 8(s11)
    sd a3, 16(s11)
    sd ra, 24(s11)
    mv a0, s1
    call fl_unify_rec_checked
    ld ra, 24(s11)
    ld a3, 16(s11)
    ld a2, 8(s11)
    ld a1, (s11)
    addi s11, s11, 32
    ret

    .global fl_d_unify_checked
    # a1=val, a2=val
    # must preserve P0-P3
fl_d_unify_checked:
    addi s11, s11, -32
    sd a1, (s11)
    sd a2, 8(s11)
    sd a3, 16(s11)
    sd ra, 24(s11)
    mv a0, s1
    call fl_d_unify_rec_checked
    ld ra, 24(s11)
    ld a3, 16(s11)
    ld a2, 8(s11)
    ld a1, (s11)
    addi s11, s11, 32
    ret

    .global fl_lookup
    # A, X0: table [count, int, label, ..., label]
fl_lookup:
    mv a1, t3
    ld t4, (t3)
    addi t3, t3, 8
llok1:
    ld t5, (t3)
    beq a0, t5, llok2
    addi t3, t3, 16
    addi t4, t4, -1
    bne t4, zero, llok1
    addi t3, t3, -8
llok2:
    ld t3, 8(t3)
    add x1, a1, t3
    ret

    .global fl_goto
    # A, <SP>: table [min, max*, flabel, label1, ...]
    # max: mark bit is 0
fl_goto:
    mv a1, t3
    ld t4, (t3)
    sub a0, a0, t4
    ld t4, 8(t3)
    bgtu a0, t4, lgoto1
    addi a0, a0, 6
    sll a0, a0, 2
    add a0, a0, a1
    ld a0, (a0)
    add x1, a0, a1
    ret
lgoto1:
    ld a0, 16(t3)
    add x1, a0, a1
    ret

    .global fl_prepare_c_arg
    # A: Arg
fl_prepare_c_arg:
    andi t3, a0, 2      # FL_ATOM_BIT
    beq t3, zero, lprep1
    addi a0, a0, 1
    ret
lprep1:
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x08000000 # FL_ARRAY_TAG
    bne t3, t4, lprep2
    ld a0, 8(a0)
    ld a0, 8(a0)
    ret
lprep2:
    addi a0, a0, 8
    ret

    .global fl_new_task
    # G, s1=tcb, A = status(var)
fl_new_task:
    mv a2, ra         # save return addr in P1
    mv a1, a0          # save status in P0
    la t3, fl_nil
    ld t3, (t3)
    ld a0, 6*8(s2)   # goal->task
    bne a0, t3, new_task_l1
    # cons new env/parent cell [[]|[]]]
    call cons
    li t4, 0x01000000   # FL_LIST_TAG
    sw t4, (a0)
    mv t3, a0
    j new_task_l2
new_task_l1:
    ld a0, 8(a0)
    ld t3, 8(a0)            # env
new_task_l2:
    ld a0, 6*8(s2)            # goal->task (parent)
    call cons
    mv t3, a0
    mv a0, a1
    call cons                       # task: [env/parent|st]
    li t4, 0x06000001         # FL_TASK_TAG|1
    sw t4, (a0)
    ld t3, 6*8(s2)            # goal->task (old)
    call fl_unref
    sd a0, 6*8(s2)            # goal->task
    mv ra, a2
    ret

    .global fl_box_long_ref
    # A = box -> A
fl_box_long_ref:
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x08000000 # FL_ARRAY_TAG
    bne t3, t4, lblr1
    ld a0, 8(a0)
    ld a0, 8(a0)
lblr1:
    ld a0, 8(a0)
fix:
    slli a0, a0, 1
    ori a0, a0, 1
    ret

    .global fl_box_int_ref
    # A = box -> A
fl_box_int_ref:
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x08000000 # FL_ARRAY_TAG
    bne t3, t4, lbir1
    ld a0, 8(a0)
    ld a0, 8(a0)
lbir1:
    lw a0, 8(a0)
    j fix

    .global fl_box_char_ref
    # A = box -> A
fl_box_char_ref:
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x08000000 # FL_ARRAY_TAG
    bne t3, t4, lbcr1
    ld a0, 8(a0)
    ld a0, 8(a0)
lbcr1:
    lbu a0, 8(a0)
    j fix

    .global fl_box_short_ref
    # A = box -> A
fl_box_short_ref:
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x08000000 # FL_ARRAY_TAG
    bne t3, t4, lbsr1
    ld a0, 8(a0)
    ld a0, 8(a0)
lbsr1:
    lwu a0, 8(a0)
    j fix

    .global fl_box_double_ref
    # T, A = box -> A
fl_box_double_ref:
    lw t3, (a0)
    li t4, 0xff000000 # FL_TAG_MASK
    and t3, t3, t4
    li t4, 0x08000000 # FL_ARRAY_TAG
    bne t3, t4, lbdr1
    ld a0, 8(a0)
    ld a0, 8(a0)
lbdr1:
    fld fa0, 8(a0)
    j fl_mkfloat

    .global fl_factset
    # a1=arity, a2=tree
    # must preserve P0-P3
fl_factset:
    addi s11, s11, -32
    sd a1, (s11)
    sd a2, 8(s11)
    sd a3, 16(s11)
    sd ra, 24(s11)
    mv a0, s1
    call fl_match_facts
    ld ra, 24(s11)
    ld a3, 16(s11)
    ld a2, 8(s11)
    ld a1, (s11)
    addi s11, s11, 32
    beq a0, zero, setcarry
    j clrcarry

    .global fl_candidate
    # T -> set carry if pre-selecting fair choice candidate
fl_candidate:
    ld a0, 24*8(s1)		# tcb->candidates
    ld t3, 25*8(s1)		# tcb->selected
    andi x5, t3, 1
    bne x5, zero, lcand1
    # case 1: the clause is a possible candidate (selected == 0)
    addi a0, a0, 1		# ++tcb->candidates
    sd a0, 24*8(s1)
    j setcarry			# skip
lcand1:
    bne a0, zero, lcand2
    # case 2: the clause is the selected candidate (selected == 1, candidates == 0)
    sd a0, 25*8(s1)		# tcb->selected = 0
    j clrcarry				# execute candidates
lcand2:
    # case 3: the clause not the selected candidate (selected == 1, candidates > 0)
    ld t4, 24*8(s1)		# --tcb->candidates
    addi t4, t4, -1
    sd t4, 24*8(s1)
    j setcarry

    .global fl_select
    # T, X0=cell address
    # clobbers P0, P1
fl_select:
    ld a2, 24*8(s1)			# tcb->candidates
    bne a2, zero, lsel1
    ret						# no candidates, normal suspend
lsel1:
    li t4, 1
    sd t4, 25*8(s1)			# tcb->selected = 1
    ld t4, (t3)
    bne t4, zero, lsel2
    addi t4, t4, 1
lsel2:
    srli x5, t4, 8
    add a1, a2, x5
    rem a0, a1, a2
    li x5, 1664525			# LCG
    mul t4, t4, x5
    li x5, 1013904223
    add t4, t4, x5
    sd t4, (t3)				# seed = (1664525 * seed) + 1013904223
    sd a0, 24*8(s1)			# tcb->candidates = tcb->candidates % (<cell> + 1)
    ld a0, (s2)				# goal->addr
    jr a0						# reenter goal

    .global fl_set_or
    # X0=val, X1=val -> X0
fl_set_or:
    la a0, fl_true
    ld a0, (a0)
    bne t3, a0, lsetor1
    ret
lsetor1:
    mv t3, t4
    ret

    .global fl_set_and
    # X0=val, X1=val -> X0
fl_set_and:
    la a0, fl_true
    ld a0, (a0)
    beq t3, a0, lsetor1
lsetand1:
    la t3, fl_false
    ld t3, (t3)
    ret

    .global fl_set_equal
    # X0=val, X1=val -> X0
fl_set_equal:
    bne t3, t4, lsetand1
    la t3, fl_true
    ld t3, (t3)
    ret

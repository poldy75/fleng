/* CRC32 calculation
    https://www.cl.cam.ac.uk/research/srg/projects/fairisle/bluebook/21/crc/node6.html
*/

#include <string.h>
#include <stdint.h>
#include "fleng-util.h"

#define QUOTIENT 	0x04c11db7
static unsigned int crctab[256];

void fl_crc_init_0(FL_TCB *tcb) {
    int i, j;
    unsigned int crc;
    for (i = 0; i < 256; i++)
    {
        crc = i << 24;
        for (j = 0; j < 8; j++)
        {
            if (crc & 0x80000000)
                crc = (crc << 1) ^ QUOTIENT;
            else
                crc = crc << 1;
        }
        crctab[i] = crc;
    }
}

static uint32_t start_crc(unsigned char *data, int *plen) {
    unsigned int        result;
    int                 i;
    unsigned char       octet;
    unsigned char d1[ 4 ];
    int len = *plen;
    memcpy(d1, "\x00\x00\x00\x00", 4);
    memcpy(d1, data, len >= 4 ? 4 : len);
    result = *data++ << 24;
    result |= *data++ << 16;
    result |= *data++ << 8;
    result |= *data++;
    result = ~result;
    *plen = len - 4;
    return result;
}

static uint32_t add_crc(unsigned char *data, uint32_t result, int len) {
    for (int i=0; i<len; i++)
    {
        result = (result << 8 | *data++) ^ crctab[result >> 24];
    }
    return ~result;
}

void fl_crc_input_3(FL_TCB *tcb, FL_VAL data, FL_VAL state, FL_VAL result) {
	char *in;
	int len;
	if(ISSTRING(data)) {
		in = STRING(data);
		len = STRING_LENGTH(data);
	} else if(ISCELL(data)) {
		switch(TAG(data)) {
		case FL_ARRAY_TAG:
			in = ARRAY_POINTER(data);
			len = array_size_in_bytes(data);
			break;
		case FL_LIST_TAG:
			in = stringify(tcb, data, &len);
			break;
		default:
			fl_rt_error(tcb, data, FL_NOT_AN_ARRAY);
		}
	} else fl_rt_error(tcb, data, FL_NOT_AN_ARRAY);
	uint32_t r, s;
	if(state == fl_nil) r = start_crc(in, &len);
	else {
		if(ISCELL(state) && TAG(state) == FL_FLOAT_TAG)
			s = (uint32_t)FLOATVAL(state);
		else {
			CHECK_INT(state);
			s = INT(state);
		}
		r = add_crc(in, s, len);
	}
	if(((r >> 31) ^ (r >> 30)) == 1)
		fl_assign(tcb, fl_alloc_float(tcb, (double)r), result);
	else fl_assign(tcb, MKINT(r), result);
}
